﻿CREATE PROCEDURE [dbo].[ResetGame] @GameID varchar(40)
AS
BEGIN
	SET NOCOUNT ON;
		if @GameID is null 
		begin
			SET @GameID = 'c07696c3-8b5e-40a7-9f89-fb4d4108827a'
		end
		
		UPDATE dbo.Question set TimeWentLive = NULL WHERE GameID = @GameID
		
		UPDATE dbo.Game set
			HasStarted = 0, 
			IsActive = 0, 
			IsCompleted = 0 
		WHERE ID = @GameID
		
		UPDATE dbo.User_Game set HasBeenEliminated = 0  WHERE GameID = @GameID
		
		DELETE FROM dbo.User_Question  WHERE GameID = @GameID
		
		DELETE FROM dbo.User_Game  WHERE GameID = @GameID



		/*
DECLARE @game_id varchar(50)
SET @game_id = '7B7C42B0-E011-4F95-91B5-BABB58869C7E' --'AFF69FDA-5185-4773-ADCE-45D201147D0F' --'AD39288F-37F6-4444-9EE5-E902CFBD27B4'
UPDATE dbo.Question set TimeWentLive = NULL WHERE GameID = @game_id
UPDATE dbo.Game set HasStarted = 0 WHERE ID = @game_id
UPDATE dbo.Game set IsActive = 0 WHERE ID = @game_id
UPDATE dbo.Game set IsCompleted = 0 WHERE ID = @game_id
UPDATE dbo.User_Game set HasBeenEliminated = 0  WHERE GameID = @game_id
DELETE FROM dbo.User_Question  WHERE GameID = @game_id
DELETE FROM dbo.User_Game  WHERE GameID = @game_id
		*/
		
END
