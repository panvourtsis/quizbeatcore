﻿CREATE PROCEDURE [dbo].[ReactivateUser] ( 
	@GameID varchar(40), 
	@UserID varchar(40), 
	@livesNeeded int,
	@lastUserQuestionID varchar(40),
	@activeQuestionID varchar(40),
	@dateCreated DATETIME,
	@newID varchar(40))
AS
BEGIN

BEGIN TRANSACTION

	UPDATE[User_Game] SET HasBeenEliminated = 0, HasUsedExtraLive = 1 WHERE UserID = @UserID and GameID = @GameID;

	UPDATE[User] SET ExtraLives = ExtraLives - @livesNeeded WHERE ID = @UserID;

	UPDATE[User_Question] SET IsCorrect = 1, HasUsedExtraLive = 1 WHERE QuestionID = @activeQuestionID;

	INSERT INTO dbo.LivesUsed( [ID], [Count], [DateCreated], [UserID], [QuestionID], [User_QuestionID]) VALUES(@newID, @livesNeeded, @dateCreated, @userid, @activeQuestionID, @lastUserQuestionID);

COMMIT


END