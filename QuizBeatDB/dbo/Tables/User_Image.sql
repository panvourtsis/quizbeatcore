﻿CREATE TABLE [dbo].[User_Image]
(
	[ID]		VARCHAR(40) NOT NULL, 
	[UserID]	VARCHAR(40) NOT NULL,	
	[Image]		image, 
	[FileName]	nvarchar(300), 
    CONSTRAINT [PK_User_Image] PRIMARY KEY ([ID]),
)
GO
CREATE INDEX [IX_User_Image_FileName] ON [dbo].[User_Image] ([FileName])
GO

CREATE INDEX [IX_User_Image_UserID] ON [dbo].[User_Image] ([UserID])
GO
