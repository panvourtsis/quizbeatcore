﻿CREATE TABLE [dbo].[LivesUsed] (
    [ID]              VARCHAR(40) NOT NULL,
    [Count]           INT           NULL,
    [QuestionID]      VARCHAR(40) NULL,
    [User_QuestionID] VARCHAR(40) NULL,
    [DateCreated]     DATETIME      NULL,
    [UserID]          VARCHAR(40) NULL,
	CONSTRAINT [PK__LivesUsed] PRIMARY KEY CLUSTERED ([ID] ASC)

);
GO
CREATE NONCLUSTERED INDEX [IX_LivesUsed_UserID]
    ON [dbo].[LivesUsed]([UserID] ASC);


