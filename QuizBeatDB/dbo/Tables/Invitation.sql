﻿CREATE TABLE [dbo].[Invitation] (
    [ID]              VARCHAR(40)	NOT NULL,
    [UserID]          VARCHAR(40)	NULL,
    [InvitedUserID]   VARCHAR(40)	NULL,
    [HasBeenRedeemed] BIT	CONSTRAINT [DF__Invitation__HasBeenRedeemed] DEFAULT ((0)) NULL,
    [Date]            DATETIME			NULL,
    CONSTRAINT [PK__Invitation] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK__Invitation__InvitedUserID] FOREIGN KEY ([InvitedUserID]) REFERENCES [dbo].[User] ([ID])
);
GO
CREATE INDEX [IX_Invitation_UserID] ON [dbo].[Invitation] ([UserID] ASC)
GO
CREATE INDEX [IX_Invitation_InvitedUserID_Hasbeenredeemed] ON [dbo].[Invitation] ([InvitedUserID] ASC, [HasBeenRedeemed] ASC)
GO