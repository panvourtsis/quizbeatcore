﻿CREATE TABLE [dbo].[SocketConnection] (
    [ID]       VARCHAR(40) NOT NULL,
    [UserID]   NVARCHAR (40)   NULL,
    [Date]     DATE             NULL,
    [SocketID] NVARCHAR (MAX)   NULL,
	CONSTRAINT [PK__SocketConnection] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO

CREATE INDEX [IX_SocketConnection_UserID] ON [dbo].[SocketConnection] ([UserID] ASC)
