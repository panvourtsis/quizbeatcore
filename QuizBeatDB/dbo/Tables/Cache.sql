﻿CREATE TABLE [dbo].[Cache] (
    [ID]			VARCHAR(40) NOT NULL,
    [Key]			VARCHAR(250) NOT NULL,
    [Value]			NVARCHAR (MAX)	 NULL,
    [Expires]		DATETIME NULL,
    [DateCreated]   DATETIME NOT NULL,
    [DateUpdated]   DATETIME NULL,
    CONSTRAINT [PK__Cache] PRIMARY KEY CLUSTERED ([ID] ASC),
);


GO

CREATE INDEX [IX_Cache_Key] ON [dbo].[Cache] ([Key])
GO

CREATE INDEX [IX_Cache_DateCreated_Key] ON [dbo].[Cache] ([DateCreated] DESC, [Key] ASC)
GO
