﻿CREATE TABLE [dbo].[Game] (
    [ID]                VARCHAR(40) NOT NULL,
    [StartDateTime]     DATETIME      NULL,
    [IsCompleted]       BIT           CONSTRAINT [DF__Game__IsComplete] DEFAULT ((0)) NULL,
    [PrizeID]           VARCHAR(40) NULL,
    [IsActive]          BIT           CONSTRAINT [DF__Game__IsActive] DEFAULT ((0)) NULL,
    [HasStarted]        BIT           NULL,
    [TimeStarted]       DATETIME      NULL,
    [NumberOfQuestions] INT           NULL,
    [PushAutoReminder]  BIT           NULL,
    [FastestWinner]     BIT           CONSTRAINT [DF__Game__FastestWin] DEFAULT ((0)) NULL,
    CONSTRAINT [PK__Game] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [Game_Prize] FOREIGN KEY ([PrizeID]) REFERENCES [dbo].[Prize] ([ID])
);


GO
CREATE NONCLUSTERED INDEX [IX_ISACTIVE_HASSTARTED_ISCOMPLETED]
    ON [dbo].[Game]([IsActive] ASC, [IsCompleted] ASC, [HasStarted] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ISNEXT]
    ON [dbo].[Game]([IsActive] ASC, [PrizeID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ISACTIVE]
    ON [dbo].[Game]([IsActive] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ISACTIVE_ISCOMPLETED]
    ON [dbo].[Game]([IsActive] ASC, [IsCompleted] ASC);


GO

CREATE INDEX [IX_Game_StartDateTime] ON [dbo].[Game] ([StartDateTime] ASC)
