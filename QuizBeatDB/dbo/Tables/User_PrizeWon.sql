﻿CREATE TABLE [dbo].[User_PrizeWon] (
    [ID]       VARCHAR(40)			NOT NULL,
    [UserID]   VARCHAR(40)			NOT NULL,  /*COLLATE SQL_Latin1_General_CP1_CI_AS*/
    [PrizeID]  VARCHAR(40)			NOT NULL,
    [Amount]   FLOAT (53)			CONSTRAINT [DF_User_PrizeWon_Amount] DEFAULT ((0)) NOT NULL,
    [DateTime] DATETIME				NULL,
    [GameID]   VARCHAR(40)			NOT NULL,
    CONSTRAINT [PK__User_PrizeWon]	PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [User_PrizeWon_Prize] FOREIGN KEY ([PrizeID]) REFERENCES [dbo].[Prize] ([ID])
);

GO
CREATE NONCLUSTERED INDEX [IX_User_PrizeWon_GameID]
    ON [dbo].[User_PrizeWon]([GameID] ASC);
GO
CREATE NONCLUSTERED INDEX [IX_User_PrizeWon_UserID]
    ON [dbo].[User_PrizeWon]([UserID] ASC);
GO
CREATE NONCLUSTERED INDEX [IX_User_PrizeWon_UserID_GameID]
    ON [dbo].[User_PrizeWon]([UserID] ASC, [GameID] ASC);
