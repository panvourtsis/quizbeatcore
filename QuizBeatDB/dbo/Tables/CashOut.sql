﻿CREATE TABLE [dbo].[CashOut] (
    [ID]         VARCHAR(40) NOT NULL,
    [UserID]     VARCHAR(40)  NULL,
    [Money]      FLOAT (53)    NULL,
    [Date]       DATETIME      NULL,
    [IsApproved] BIT           NULL,
    [IBAN]       VARCHAR (40)  NULL,
    [FullName]   NVARCHAR (140) NULL,
    [Type]       VARCHAR (20)  NULL,
    CONSTRAINT [PK__CashOut] PRIMARY KEY CLUSTERED ([ID] ASC),
);


GO

CREATE INDEX [IX_CashOut_UserID] ON [dbo].[CashOut] ([UserID])
GO

CREATE INDEX [IX_CashOut_IsApproved_UserID] ON [dbo].[CashOut] ([IsApproved] ASC, [UserID] ASC)
GO
