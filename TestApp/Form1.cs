﻿using Core.Common;
using Core.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestApp
{
    public partial class Form1 : Form
    {

        // f621833b-f9a4-442f-a312-786e7a89ec7d
        //--game: AD39288F-37F6-4444-9EE5-E902CFBD27B4
        //--auth: 4EE7453E-1737-447A-9C52-D1C4D34E1B35

        //string testingKey = "Testing;759049cd4d0e689b7fadca0d3fc362e6";

        List<Question> lsQuestions = new List<Question>();

        private Config cfg = null;

        public Form1()
        {
            InitializeComponent();

            cmbWebServer.SelectedText = "";
            cmbWebServer.SelectionLength = 0;
            cmbDbServer.SelectedText = "";
            cmbDbServer.SelectionLength = 0;
            ActiveControl = txtGameID;
            txtGameID.Focus();

            this.cmdConnect.Click += async (o, e) => await this.cmdConnect_Click(o, e);
            this.lnkResetGame.LinkClicked += async (o, e) => await this.cmdResetGame_Click(o, e);
            this.lnkTest.LinkClicked += async (o, e) => await this.lnkTest_Click(o, e);
            this.lnkEndGame.LinkClicked += async (o, e) => await this.lnkEndGame_Click(o, e);
            this.lnkSendAnswer.LinkClicked += async (o, e) => await this.lnkSenAnswer_Click(o, e);
            this.lnkAssignToGame.LinkClicked += async (o, e) => await this.lnkAssignToGame_Click(o, e);
            this.lnkCheckDB.LinkClicked += async (o, e) => await this.lnkCheckDB_Click(o, e);
            this.lnkInitCache.LinkClicked += async (o, e) => await this.lnkInitCache_Click(o, e);
            this.lnkPreheat.LinkClicked += async (o, e) => await this.lnkPreheat_Click(o, e);

            cmbDbServer.SelectedIndex = 0;
            cmbWebServer.SelectedIndex = 0;

            cfg = Config.Load();

            cmbDbServer.Text = cfg.DbServer;
            cmbWebServer.Text = cfg.WebServer;
            txtUsrAuthID.Text = cfg.AuthToken;
            txtGameID.Text = cfg.GameID;
            txtUserName.Text = cfg.UserName;
            //string key = testingKey;
        }


        protected override void OnFormClosing(FormClosingEventArgs e)
        {

            cfg.DbServer = cmbDbServer.Text;
            cfg.WebServer = cmbWebServer.Text;
            cfg.AuthToken = txtUsrAuthID.Text;
            cfg.GameID = txtGameID.Text;
            cfg.UserName = txtUserName.Text;
            cfg.Save();

        }
        private async Task cmdConnect_Click(object sender, EventArgs e)
        {
            string web = getServerURL();
            string db = getConnStr();

            if (db.IsNullOrEmpty())
            {
                Log("DB connection is missing!");
                return;
            }
            if (txtGameID.Text.IsNullOrEmpty())
            {
                Log("Game ID is missing!");
                return;
            }
            if (txtUserName.Text.IsNullOrEmpty())
            {
                Log("user name/email is missing!");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                using (DataSvc da = DataSvc.GetDataService(db))
                {
                    da.ConnectionString = db;

                    dynamic usr = await da.ExecuteDynamicObjectAsync("select * from [user] where username='" + txtUserName.Text.Trim() + "' or email='" + txtUserName.Text + "'");
                    dynamic game = da.ExecuteDynamicObject("select * from [game] where id='" + txtGameID.Text + "'");
                    IEnumerable<dynamic> lsq = da.ExecuteDynamicList("select * from question where gameid='" + txtGameID.Text + "' order by sorting", da.Args(), false);
                    if (usr != null)
                    {
                        txtUsrAuthID.Text = usr.AuthToken;
                        Log("User found...");
                    }
                    if (lsq != null)
                    {
                        cmbQuestions.Items.Clear();
                        lsQuestions.Clear();
                        foreach (var q in lsq)
                        {
                            Question qu = new Question() { ID = q.ID, GameID = q.GameID, Answer_1 = q.Answer_1, Answer_2 = q.Answer_2, Answer_3 = q.Answer_3, CorrectAnswer = q.CorrectAnswer, Sorting = q.Sorting, Text = q.Text, TimeWentLive = q.TimeWentLive };
                            lsQuestions.Add(qu);
                            cmbQuestions.Items.Add(qu.ID);
                        }
                        //cmbQuestions.DataSource = lsQuestions;
                        if (lsQuestions.Count > 0)
                        {
                            cmbQuestions.Text = lsQuestions[0].ID;
                        }
                        Log($"{cmbQuestions.Items.Count} questions found...");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            Cursor.Current = Cursors.Default;
        }

        private async Task lnkCheckDB_Click(object sender, EventArgs e)
        {
            string web = getServerURL();
            string db = getConnStr();

            if (db.IsNullOrEmpty())
            {
                Log("DB connection is missing!");
                return;
            }
            if (txtGameID.Text.IsNullOrEmpty())
            {
                Log("Game ID is missing!");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                using (DataSvc da = DataSvc.GetDataService(db))
                {
                    da.ConnectionString = db;

                    dynamic cnt = await da.ExecuteDynamicObjectAsync("select count(*) Cnt from [user_question] where gameid='" + txtGameID.Text.Trim() + "'");
                    if (cnt != null)
                    {
                        Log($"Check DB: <{cnt.Cnt}> questions inserted...");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            Cursor.Current = Cursors.Default;
        }


        private async Task lnkPreheat_Click(object sender, EventArgs e)
        {
            string web = getServerURL();
            string db = getConnStr();

            Cursor.Current = Cursors.WaitCursor;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                for (int i = 0; i < 10; i++)
                {
                    ThreadPool.QueueUserWorkItem(new WaitCallback(delegate (object state){ Get($"{web}/api/game/PreheatGame", new Dictionary<string, string>() { { "AuthToken", "220f78a7-7dcc-49a5-b581-e2038c5efc81" } }, $" [{i}]"); }), null);
                }
                for (int i = 2000; i < 2010; i++)
                {
                    ThreadPool.QueueUserWorkItem(new WaitCallback(delegate (object state) { Get($"{web}/api/game/PreheatGame", new Dictionary<string, string>() { { "AuthToken", "220f78a7-7dcc-49a5-b581-e2038c5efc81" } }, $" [{i}]"); }), null);
                }
                for (int i = 1000; i < 1020; i++)
                {
                    ThreadPool.QueueUserWorkItem(new WaitCallback(delegate (object state) { Get($"{web}/api/game/PreheatGame", new Dictionary<string, string>() { { "AuthToken", "220f78a7-7dcc-49a5-b581-e2038c5efc81" } }, $" [{i}]"); }), null);
                }
                for (int i = 500; i < 520; i++)
                {
                    ThreadPool.QueueUserWorkItem(new WaitCallback(delegate (object state) { Get($"{web}/api/game/PreheatGame", new Dictionary<string, string>() { { "AuthToken", "220f78a7-7dcc-49a5-b581-e2038c5efc81" } }, $" [{i}]"); }), null);
                }
                for (int i = 3000; i < 3020; i++)
                {
                    ThreadPool.QueueUserWorkItem(new WaitCallback(delegate (object state) { Get($"{web}/api/game/PreheatGame", new Dictionary<string, string>() { { "AuthToken", "220f78a7-7dcc-49a5-b581-e2038c5efc81" } }, $" [{i}]"); }), null);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            Cursor.Current = Cursors.Default;
        }


        private async Task lnkInitCache_Click(object sender, EventArgs e)
        {
            string web = getServerURL();
            string db = getConnStr();

            Cursor.Current = Cursors.WaitCursor;

            try
            {
                string rs = await GetAsync($"{web}/api/game/GameInit", new Dictionary<string, string>() { { "AuthToken", "220f78a7-7dcc-49a5-b581-e2038c5efc81" } });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            Cursor.Current = Cursors.Default;
        }


        private async Task cmdResetGame_Click(object sender, EventArgs e)
        {
            string web = getServerURL();
            string db = getConnStr();
            if (db.IsNullOrEmpty())
            {
                Log("DB connection is missing!");
                return;
            }
            if (txtGameID.Text.IsNullOrEmpty())
            {
                Log("Game ID is missing!");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                using (DataSvc da = DataSvc.GetDataService(db))
                {
                    int n = 0;
                    da.ConnectionString = db;
                    var args = da.Args("@GameID", txtGameID.Text);
                    n += await da.ExecuteNonQueryAsync(@"UPDATE dbo.Question set TimeWentLive = NULL WHERE GameID = @GameID", args);
                    n += await da.ExecuteNonQueryAsync(@"UPDATE dbo.Game set HasStarted = 0, IsActive = 0, IsCompleted = 0 WHERE ID = @GameID", args);
                    n += await da.ExecuteNonQueryAsync(@"DELETE FROM dbo.User_Question WHERE GameID = @GameID", args);
                    n += await da.ExecuteNonQueryAsync(@"DELETE FROM dbo.User_Game WHERE GameID = @GameID", args);
                    Log($"Game was reset ({n} updates)...");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            Cursor.Current = Cursors.Default;
        }


        private async Task lnkTest_Click(object sender, EventArgs e)
        {
            string web = getServerURL();
            string db = getConnStr();
            if (web.IsNullOrEmpty())
            {
                Log("Web server is missing!");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                string rs = await GetAsync($"{web}/api/values/test");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            Cursor.Current = Cursors.Default;
        }

        private async Task lnkEndGame_Click(object sender, EventArgs e)
        {
            string web = getServerURL();
            string db = getConnStr();
            if (web.IsNullOrEmpty())
            {
                Log("Web server is missing!");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                string rs = await GetAsync($"{web}/api/game/endsocketgame", new Dictionary<string, string>() { { "AuthToken", "220f78a7-7dcc-49a5-b581-e2038c5efc81" } });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            Cursor.Current = Cursors.Default;
        }

        private async Task lnkAssignToGame_Click(object sender, EventArgs e)
        {
            string web = getServerURL();
            string db = getConnStr();
            if (web.IsNullOrEmpty())
            {
                Log("Web server is missing!");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                string rs = await Post($"{web}/api/game/assigntoActiveGame",
                    "",
                    new Dictionary<string, string>() { { "Testing", "759049cd4d0e689b7fadca0d3fc362e6" }, { "Authorization", txtUsrAuthID.Text } });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            Cursor.Current = Cursors.Default;
        }


        private async Task lnkSenAnswer_Click(object sender, EventArgs e)
        {
            string web = getServerURL();
            string db = getConnStr();
            if (web.IsNullOrEmpty())
            {
                Log("Web server is missing!");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                string rs = await GetAsync($"{web}/api/game/sendanswer?questionId={cmbQuestions.Text}&answerGiven={txtAnswer.Value}",
                    new Dictionary<string, string>() { { "Testing", "759049cd4d0e689b7fadca0d3fc362e6" }, { "Content-Type", "application/json" }, { "Authorization", txtUsrAuthID.Text } });

                var obj = new { questionId = cmbQuestions.Text, answerGiven = txtAnswer.Value };
                rs += Environment.NewLine;
                rs += await Post($"{web}/api/game/sendanswer", Json.ToJson(obj),
                    new Dictionary<string, string>() { { "Testing", "759049cd4d0e689b7fadca0d3fc362e6" }, { "Authorization", txtUsrAuthID.Text } });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            Cursor.Current = Cursors.Default;
        }


        private string getConnStr()
        {
            string key = cmbDbServer.Text;
            switch (key)
            {
                case "QBLive":
                    return Properties.Settings.Default.QBLive;
                case "QBLocal":
                    return Properties.Settings.Default.QBLocal;
                case "QBStaging":
                    return Properties.Settings.Default.QBStaging;
            }
            return string.Empty;
        }

        private string getServerURL()
        {
            return cmbWebServer.Text;
        }

        private void Log(string text)
        {
            txtLog.InvokeControlAction<TextBox>(x => x.AppendText(text + Environment.NewLine));
        }




        private void lnkClearLog_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            txtLog.Text = "";
        }


        private async Task<string> GetAsync(string address = null, Dictionary<string, string> headers = null, string identifier = null)
        {
            string JsonResult = string.Empty;
            try
            {
                using (var client = new System.Net.WebClient())
                {
                    if (headers != null)
                    {
                        foreach (var k in headers)
                        {
                            client.Headers.Add(k.Key, k.Value);
                        }
                    }
                    JsonResult = await client.DownloadStringTaskAsync(new Uri(address));
                    Log($"{address}{identifier}:");
                    Log(Core.Helpers.Json.FormatJson(JsonResult));
                }
            }
            catch (System.Net.WebException wex)
            {
                if (wex.Response != null)
                {
                    using (var errorResponse = (System.Net.HttpWebResponse)wex.Response)
                    {
                        using (var reader = new System.IO.StreamReader(errorResponse.GetResponseStream()))
                        {
                            string error = reader.ReadToEnd();
                            if (error != null && error.Length > 300) error = error.Substring(0, 300) + "...";
                            JsonResult = error;
                            throw new Exception(wex.Message + (error.IsNullOrEmpty() ? "" : System.Environment.NewLine + "=> " + error));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return JsonResult;
        }

        private string Get(string address = null, Dictionary<string, string> headers = null, string identifier = null)
        {
            string JsonResult = string.Empty;
            try
            {
                using (var client = new System.Net.WebClient())
                {
                    if (headers != null)
                    {
                        foreach (var k in headers)
                        {
                            client.Headers.Add(k.Key, k.Value);
                        }
                    }
                    JsonResult = client.DownloadString(new Uri(address));
                    Log($"{address}{identifier}:");
                    Log(Core.Helpers.Json.FormatJson(JsonResult));
                }
            }
            catch (System.Net.WebException wex)
            {
                if (wex.Response != null)
                {
                    using (var errorResponse = (System.Net.HttpWebResponse)wex.Response)
                    {
                        using (var reader = new System.IO.StreamReader(errorResponse.GetResponseStream()))
                        {
                            string error = reader.ReadToEnd();
                            if (error != null && error.Length > 300) error = error.Substring(0, 300) + "...";
                            JsonResult = error;
                            throw new Exception(wex.Message + (error.IsNullOrEmpty() ? "" : System.Environment.NewLine + "=> " + error));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return JsonResult;
        }


        public enum enPostType
        {
            Form,
            Json
        }

        internal static async Task<string> Post(string URI, string Parameters, Dictionary<string, string> headers = null, enPostType postType = enPostType.Form, CookieContainer cnt = null)
        {
            string JsonResult = string.Empty;
            try
            {
                System.Net.WebResponse resp = null;
                System.Net.HttpWebRequest req = (HttpWebRequest)System.Net.WebRequest.Create(URI);
                req.CookieContainer = cnt;
                req.Timeout = 999999999;
                switch (postType)
                {
                    case enPostType.Form:
                        req.ContentType = "application/x-www-form-urlencoded";
                        break;
                    case enPostType.Json:
                        req.ContentType = "application/json";
                        break;
                }

                req.Method = "POST";
                if (headers != null)
                {
                    headers.ForEach(X => req.Headers.Add(X.Key, X.Value));
                }
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(Parameters);
                req.ContentLength = bytes.Length;
                System.IO.Stream os = await req.GetRequestStreamAsync();
                os.Write(bytes, 0, bytes.Length);
                os.Close();
                resp = req.GetResponse();
                if (resp == null) return null;
                System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
                JsonResult = sr.ReadToEnd().Trim();
            }
            catch (System.Net.WebException wex)
            {
                if (wex.Response != null)
                {
                    using (var errorResponse = (System.Net.HttpWebResponse)wex.Response)
                    {
                        using (var reader = new System.IO.StreamReader(errorResponse.GetResponseStream()))
                        {
                            string error = reader.ReadToEnd();
                            if (error != null && error.Length > 300) error = error.Substring(0, 300) + "...";
                            JsonResult = error;
                            throw new Exception(wex.Message + (error.IsNullOrEmpty() ? "" : System.Environment.NewLine + "=> " + error));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                JsonResult = ex.Message;
            }

            return JsonResult;
        }


    }


    public static class extensions
    {
        /// <summary>
        /// Usage:
        /// InvokeControlAction<Label>(lblTime, lbl => lbl.Text = String.Format("The current time is: {0}", DateTime.Now.ToString("h:mm:ss tt")));
        /// InvokeControlAction<Form>(this, frm => frm.Text = String.Format("The current time is: {0}", DateTime.Now.ToString("h:mm:ss tt")));
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="control"></param>
        /// <param name="action"></param>
        public static void InvokeControlAction<T>(this T control, Action<T> action) where T : Control
        {
            if (control.InvokeRequired)
            {
                control.Invoke(new Action<T, Action<T>>(InvokeControlAction), new object[] { control, action });
            }
            else
            { action(control); }
        }

    }

    public class Question
    {
        public string ID { get; set; }
        public string Text { get; set; }
        public int CorrectAnswer { get; set; }
        public string Answer_1 { get; set; }
        public string Answer_2 { get; set; }
        public string Answer_3 { get; set; }
        public string GameID { get; set; }
        public int Sorting { get; set; }
        public DateTime? TimeWentLive { get; set; }
        public override string ToString()
        {
            return $"{Sorting.ToString().PadLeft(2, '0')}-{Text} [{ID}]({CorrectAnswer})";
        }
    }



    public class Config
    {
        public string WebServer { get; set; }
        public string DbServer { get; set; }
        public string GameID { get; set; }
        public string UserName { get; set; }
        public string AuthToken { get; set; }

        public static Config Load()
        {
            if (File.Exists("app.cfg"))
            {
                Config c = File.ReadAllText("app.cfg").FromJson<Config>();
                return c;
            }
            else
            {
                return new Config();
            }
        }

        public void Save()
        {
            File.WriteAllText("app.cfg", Json.ToJson(this));
        }

    }


}
