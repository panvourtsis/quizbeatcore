﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;

namespace Core.Helpers
{
    #region < Imaging >
    public static class Imaging
    {
        const int maxPixels = 100;

        public static Size GetThumbnailSize(Image original, int pixelSize = maxPixels)
        {
            // Maximum size of any dimension.

            // Width and height.
            int originalWidth = original.Width;
            int originalHeight = original.Height;

            // Compute best factor to scale entire image based on larger dimension.
            double factor;
            if (originalWidth > originalHeight)
            {
                factor = (double)pixelSize / originalWidth;
            }
            else
            {
                factor = (double)pixelSize / originalHeight;
            }

            // Return thumbnail size.
            return new Size((int)(originalWidth * factor), (int)(originalHeight * factor));
        }

        public static byte[] imageToByteArray(Image imageIn)
        {
            byte[] retVal = null;
            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    imageIn.Save(ms, ImageFormat.Jpeg);
                    retVal = ms.ToArray();
                }
            }
            catch (Exception) { }
            return retVal;
        }

        public static Image byteArrayToImage(byte[] byteArrayIn)
        {
            Image returnImage = null;
            try
            {
                using (MemoryStream ms = new MemoryStream(byteArrayIn))
                {
                    returnImage = Image.FromStream(ms);
                }
            }
            catch (Exception)
            {

            }
            return returnImage;
        }

        public static void SaveToFile(Image img, string fileName)
        {
            string outputFileName = fileName;
            using (MemoryStream memory = new MemoryStream())
            {
                using (FileStream fs = new FileStream(outputFileName, FileMode.Create, FileAccess.ReadWrite))
                {
                    img.Save(memory, ImageFormat.Jpeg);
                    byte[] bytes = memory.ToArray();
                    fs.Write(bytes, 0, bytes.Length);
                }
            }
        }

        public static void SaveToFile(byte[] res, string fileName)
        {
            var fs = new BinaryWriter(new FileStream(fileName, FileMode.Create, FileAccess.Write));
            fs.Write(res);
            fs.Close();
        }


        public static Bitmap Resize(Image image, int maxWidth, int maxHeight, int quality, ImageFormat imageFormat = null)
        {
            // Get the image's original width and height
            int originalWidth = image.Width;
            int originalHeight = image.Height;

            // To preserve the aspect ratio
            float ratioX = (float)maxWidth / (float)originalWidth;
            float ratioY = (float)maxHeight / (float)originalHeight;
            float ratio = System.Math.Min(ratioX, ratioY);

            // New width and height based on aspect ratio
            int newWidth = (int)(originalWidth * ratio);
            int newHeight = (int)(originalHeight * ratio);

            // Convert other formats (including CMYK) to RGB.
            Bitmap newImage = new Bitmap(newWidth, newHeight, PixelFormat.Format24bppRgb);

            // Draws the image in the specified size with quality mode set to HighQuality
            using (Graphics graphics = Graphics.FromImage(newImage))
            {
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.DrawImage(image, 0, 0, newWidth, newHeight);
            }

            // Get an ImageCodecInfo object that represents the PNG/... codec.
            ImageCodecInfo imageCodecInfo = GetEncoderInfo(imageFormat ?? ImageFormat.Png);

            // Create an Encoder object for the Quality parameter.
            System.Drawing.Imaging.Encoder encoder = System.Drawing.Imaging.Encoder.Quality;

            // Create an EncoderParameters object. 
            EncoderParameters encoderParameters = new EncoderParameters(1);

            // Save the image as a JPEG file with quality level.
            EncoderParameter encoderParameter = new EncoderParameter(encoder, quality);
            encoderParameters.Param[0] = encoderParameter;
            return newImage; //.Save(filePath, imageCodecInfo, encoderParameters);
        }


        public enum ImageResizeMode
        {
            /// <summary>
            /// Resize to the specified size, allow aspect ratio change<br/>
            /// </summary>
            Fixed,
            /// <summary>
            /// Resize to the specified width, height is calculated by the aspect ratio<br/>
            /// </summary>
            ByWidth,
            /// <summary>
            /// Resize to the specified height, width is calculated by the aspect ratio<br/>
            /// </summary>
            ByHeight,
            /// <summary>
            /// Resize to the specified size, keep aspect ratio and cut the overflow part<br/>
            /// </summary>
            Cut,
            /// <summary>
            /// Resize to the specified size, keep aspect ratio and padding the insufficient part<br/>
            /// </summary>
            Padding
        }


        public static Image Resize(Image image, int width, int height, ImageResizeMode mode, System.Drawing.Color? background = null)
        {
            var src = new Rectangle(0, 0, image.Width, image.Height);
            var dst = new Rectangle(0, 0, width, height);
            // Calculate destination rectangle by resize mode
            if (mode == ImageResizeMode.Fixed)
            {
            }
            else if (mode == ImageResizeMode.ByWidth)
            {
                height = (int)((decimal)src.Height / src.Width * dst.Width);
                dst.Height = height;
            }
            else if (mode == ImageResizeMode.ByHeight)
            {
                width = (int)((decimal)src.Width / src.Height * dst.Height);
                dst.Width = width;
            }
            else if (mode == ImageResizeMode.Cut)
            {
                if ((decimal)src.Width / src.Height > (decimal)dst.Width / dst.Height)
                {
                    src.Width = (int)((decimal)dst.Width / dst.Height * src.Height);
                    src.X = (image.Width - src.Width) / 2; // Cut left and right
                }
                else
                {
                    src.Height = (int)((decimal)dst.Height / dst.Width * src.Width);
                    src.Y = (image.Height - src.Height) / 2; // Cut top and bottom
                }
            }
            else if (mode == ImageResizeMode.Padding)
            {
                if ((decimal)src.Width / src.Height > (decimal)dst.Width / dst.Height)
                {
                    dst.Height = (int)((decimal)src.Height / src.Width * dst.Width);
                    dst.Y = (height - dst.Height) / 2; // Padding left and right
                }
                else
                {
                    dst.Width = (int)((decimal)src.Width / src.Height * dst.Height);
                    dst.X = (width - dst.Width) / 2; // Padding top and bottom
                }
            }
            // Draw new image
            var newImage = new Bitmap(width, height);
            using (var graphics = Graphics.FromImage(newImage))
            {
                // Set smoothing mode
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                // Set background color
                graphics.Clear(background ?? System.Drawing.Color.Transparent);
                // Render original image with the calculated rectangle
                graphics.DrawImage(image, dst, src, GraphicsUnit.Pixel);
            }
            return newImage;
        }

        /// <summary>
        /// Method to get encoder infor for given image format.
        /// </summary>
        /// <param name="format">Image format</param>
        /// <returns>image codec info.</returns>
        public static ImageCodecInfo GetEncoderInfo(ImageFormat format)
        {
            return ImageCodecInfo.GetImageDecoders().SingleOrDefault(c => c.FormatID == format.Guid);
        }

        public static String ToHtml(this Color c)
        {
            return ColorTranslator.ToHtml(c);
        }
        public static Int32 ToWin32(this Color c)
        {
            return ColorTranslator.ToWin32(c);
        }
        /// <summary>
        ///     An Image extension method that cuts an image.
        /// </summary>
        /// <param name="this">The @this to act on.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        /// <returns>The cutted image.</returns>
        public static Image Cut(this Image @this, int width, int height, int x, int y)
        {
            var r = new Bitmap(width, height);
            var destinationRectange = new Rectangle(0, 0, width, height);
            var sourceRectangle = new Rectangle(x, y, width, height);

            using (Graphics g = Graphics.FromImage(r))
            {
                g.DrawImage(@this, destinationRectange, sourceRectangle, GraphicsUnit.Pixel);
            }

            return r;
        }


        #region < Scale >

        // <summary>
        ///     An Image extension method that scales an image to the specific ratio.
        /// </summary>
        /// <param name="this">The @this to act on.</param>
        /// <param name="ratio">The ratio.</param>
        /// <returns>The scaled image to the specific ratio.</returns>
        public static Image Scale(this Image @this, double ratio)
        {
            int width = Convert.ToInt32(@this.Width * ratio);
            int height = Convert.ToInt32(@this.Height * ratio);

            var r = new Bitmap(width, height);

            using (Graphics g = Graphics.FromImage(r))
            {
                g.CompositingQuality = CompositingQuality.HighQuality;
                g.SmoothingMode = SmoothingMode.HighQuality;
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;

                g.DrawImage(@this, 0, 0, width, height);
            }

            return r;
        }

        /// <summary>
        ///     An Image extension method that scales an image to a specific with and height.
        /// </summary>
        /// <param name="this">The @this to act on.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <returns>The scaled image to the specific width and height.</returns>
        public static Image Scale(this Image @this, int width, int height)
        {
            var r = new Bitmap(width, height);

            using (Graphics g = Graphics.FromImage(r))
            {
                g.CompositingQuality = CompositingQuality.HighQuality;
                g.SmoothingMode = SmoothingMode.HighQuality;
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;

                g.DrawImage(@this, 0, 0, width, height);
            }

            return r;
        }
        #endregion

    }
    #endregion

}
