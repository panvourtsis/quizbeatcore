﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using Core.Common;

namespace Core.Helpers
{

    #region < WEB Request/GET/POST >

    public enum enWebRequestType
    {
        GET = 1, POST = 2, PUT = 3
    }

    public static class Web
    {

        public static string Request(string url, string parameters, Dictionary<string, string> headers, CookieContainer cnt = null, enWebRequestType rqType = enWebRequestType.GET, bool throwException = true)
        {
            string JsonResult = string.Empty;
            try
            {
                switch (rqType)
                {
                    case enWebRequestType.GET:
                        JsonResult = Get(url, headers, cnt);
                        break;
                    case enWebRequestType.POST:
                        JsonResult = Post(url, parameters, headers, cnt);
                        break;
                }
            }
            catch (WebException wex)
            {
                if (wex.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)wex.Response)
                    {
                        using (var reader = new System.IO.StreamReader(errorResponse.GetResponseStream()))
                        {
                            string error = reader.ReadToEnd();
                            if (throwException)
                            {
                                if (error != null && error.Length > 300) error = error.Substring(0, 300) + "...";
                                throw new Exception(wex.Message + (error.IsNullOrEmpty() ? "" : System.Environment.NewLine + "=> " + error));
                            }
                            else
                            {
                                JsonResult = error;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (throwException)
                {
                    throw ex;
                }
            }
            return JsonResult;

        }

        public static string Request(string url, Dictionary<string, string> headers, CookieContainer cnt = null, enWebRequestType rqType = enWebRequestType.GET, bool throwException = true)
        {
            return Request(url, null, headers, cnt, rqType, throwException);
        }


        internal static string Get(string address = null, Dictionary<string, string> headers = null, CookieContainer cnt = null)
        {
            string JsonResult = string.Empty;
            using (var client = new WebClient())
            {

                if (headers != null)
                {
                    foreach (var k in headers)
                    {
                        client.Headers.Add(k.Key, k.Value);
                    }
                }
                JsonResult = client.DownloadString(address);
            }
            return JsonResult;
        }

        public static byte[] GetBytes(string address = null, Dictionary<string, string> headers = null)
        {
            byte[] Result = null;
            using (var client = new WebClientEx())
            {
                if (headers != null)
                {
                    foreach (var k in headers)
                    {
                        client.Headers.Add(k.Key, k.Value);
                    }
                }
                Result = client.DownloadData(address);
            }
            return Result;
        }

        public static void GetFile(string address = null, string localFileName = null, Dictionary<string, string> headers = null)
        {
            using (var client = new WebClient())
            {
                if (headers != null)
                {
                    foreach (var k in headers)
                    {
                        client.Headers.Add(k.Key, k.Value);
                    }
                }
                client.DownloadFile(address, localFileName);
            }
        }


        internal static string Post(string URI, string Parameters, Dictionary<string, string> headers = null, CookieContainer cnt = null)
        {
            System.Net.WebResponse resp = null;
            System.Net.HttpWebRequest req = (HttpWebRequest)System.Net.WebRequest.Create(URI);
            req.CookieContainer = cnt;
            req.Timeout = 999999999;
            req.ContentType = "application/json";
            req.Method = "POST";
            if (headers != null)
            {
                foreach (var k in headers)
                {
                    req.Headers.Add(k.Key, k.Value);
                }
            }
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(Parameters);
            req.ContentLength = bytes.Length;
            System.IO.Stream os = req.GetRequestStream();
            os.Write(bytes, 0, bytes.Length);
            os.Close();
            resp = req.GetResponse();
            if (resp == null) return null;
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            return sr.ReadToEnd().Trim();
        }


        #region < Extensions >
        /// <summary>
        ///     A WebRequest extension method that gets the WebRequest response or the WebException response.
        /// </summary>
        /// <param name="this">The @this to act on.</param>
        /// <returns>The WebRequest response or WebException response.</returns>
        public static WebResponse GetResponseSafe(this WebRequest @this)
        {
            try
            {
                return @this.GetResponse();
            }
            catch (WebException e)
            {
                return e.Response;
            }
        }

        public static string ReadToEnd(this WebResponse @this)
        {
            using (Stream stream = @this.GetResponseStream())
            {
                using (var reader = new StreamReader(stream))
                {
                    return reader.ReadToEnd();
                }
            }
        }
        public static string ReadToEndAndDispose(this WebResponse @this)
        {
            using (WebResponse response = @this)
            {
                using (Stream stream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(stream, Encoding.Default))
                    {
                        return reader.ReadToEnd();
                    }
                }
            }
        }

        public static void Send(this MailMessage @this)
        {
            using (var smtpClient = new SmtpClient())
            {
                smtpClient.Send(@this);
            }
        }

        public static void SendAsync(this MailMessage @this, object userToken)
        {
            using (var smtpClient = new SmtpClient())
            {
                smtpClient.SendAsync(@this, userToken);
            }
        }

        #endregion

    }

    #endregion


    public class WebClientEx : WebClient
    {
        private readonly CookieContainer cookieContainer = new CookieContainer();

        public WebClientEx() : base()
        {

        }
        public WebClientEx(CookieContainer c) : base()
        {
            cookieContainer = c;
        }

        protected override WebRequest GetWebRequest(Uri address)
        {
            WebRequest request = base.GetWebRequest(address);
            HttpWebRequest webRequest = request as HttpWebRequest;
            if (webRequest != null)
            {
                webRequest.CookieContainer = cookieContainer;
            }
            return request;
        }
    }
}
