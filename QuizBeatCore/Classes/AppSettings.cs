﻿using System.Collections.Generic;

namespace Core.Common
{

    public class AppConfig
    {
        public AppSettings AppSettings { get; set; }
    }

    public interface IAppSettings
    {
        string DataSvc { get; set; }
        string ActiveConnStr { get; set; }
        Dictionary<string, string> ConnectionStrings { get; set; }
    }

    public class AppSettings : IAppSettings
    {
        public string DataSvc { get; set; }
        public string ActiveConnStr { get; set; }
        public Dictionary<string, string> ConnectionStrings { get; set; } = new Dictionary<string, string>();
    }

}
