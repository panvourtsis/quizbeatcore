﻿using Core.Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Linq;
using System.Text;

namespace Core.Helpers
{
    public static class Json
    {
        public static Newtonsoft.Json.JsonSerializerSettings JsonSerializerSettings(IContractResolver ContractResolver = null)
        {
            if (ContractResolver == null) ContractResolver = new CamelCasePropertyNamesContractResolver();

            return new Newtonsoft.Json.JsonSerializerSettings()
            {
                NullValueHandling = NullValueHandling.Ignore,
                Formatting = Newtonsoft.Json.Formatting.Indented,
                MissingMemberHandling = MissingMemberHandling.Ignore,
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                Culture = System.Globalization.CultureInfo.InvariantCulture,
                DateTimeZoneHandling = Newtonsoft.Json.DateTimeZoneHandling.Unspecified,
                ContractResolver = ContractResolver,
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Serialize,
                PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.None
                
                //PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects,
                //, TypeNameHandling = TypeNameHandling.All
            };
        }


        public static T As<T>(string json, Newtonsoft.Json.JsonSerializerSettings settings = null)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(json, settings ?? JsonSerializerSettings());
        }

        public static string ToString(object obj, Newtonsoft.Json.JsonSerializerSettings settings = null)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(obj, Newtonsoft.Json.Formatting.None, settings ?? JsonSerializerSettings());
        }

        public static string ToJson(object obj, Newtonsoft.Json.JsonSerializerSettings settings = null)
        {
            return ToString(obj, settings);
        }

        private const string INDENT_STRING = "    ";
        /// <summary>
        /// Returns indented (formatted) json string.
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string FormatJson(string str)
        {
            var indent = 0;
            var quoted = false;
            var sb = new StringBuilder();
            for (var i = 0; i < str.Length; i++)
            {
                var ch = str[i];
                switch (ch)
                {
                    case '{':
                    case '[':
                        sb.Append(ch);
                        if (!quoted)
                        {
                            sb.Append(Environment.NewLine);
                            Enumerable.Range(0, ++indent).ForEach(item => sb.Append(INDENT_STRING));
                        }
                        break;
                    case '}':
                    case ']':
                        if (!quoted)
                        {
                            sb.Append(Environment.NewLine);
                            Enumerable.Range(0, --indent).ForEach(item => sb.Append(INDENT_STRING));
                        }
                        sb.Append(ch);
                        break;
                    case '"':
                        sb.Append(ch);
                        bool escaped = false;
                        var index = i;
                        while (index > 0 && str[--index] == '\\')
                            escaped = !escaped;
                        if (!escaped)
                            quoted = !quoted;
                        break;
                    case ',':
                        sb.Append(ch);
                        if (!quoted)
                        {
                            sb.Append(Environment.NewLine);
                            Enumerable.Range(0, indent).ForEach(item => sb.Append(INDENT_STRING));
                        }
                        break;
                    case ':':
                        sb.Append(ch);
                        if (!quoted)
                            sb.Append(" ");
                        break;
                    default:
                        sb.Append(ch);
                        break;
                }
            }
            return sb.ToString();
        }
    }
}
