using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using QuizBeat.Classes;
using QuizBeat.Models.Database;
using QuizBeatAPI.Repositories;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;

namespace QuizBeatAPI.Hubs
{
    public static class UserHandler
    {
        public static HashSet<string> ConnectedIds = new HashSet<string>();
    }

    [HubName("QuestionHub")]
    public class QuestionHub : Hub
    {
        #region---SignalR Methods---

        public override System.Threading.Tasks.Task OnConnected()
        {
            //Groups.Add(Context.ConnectionId, "QuestionHub");
            //var email = HttpContext.Current.User.Identity.Name;
            //UserRepository _userRepository = new UserRepository();
            //User user = _userRepository.Find(email: email);
            UserHandler.ConnectedIds.Add(Context.ConnectionId);
            Groups.Add(Context.ConnectionId, "QuestionHub");
            //string username = Context.User.Identity.Name;
            //SocketConnectionRepository socketConnectionRepository = new SocketConnectionRepository();
            //SocketConnection socketConnection = new SocketConnection();
            //socketConnection.Date = DateTime.UtcNow;
            //socketConnection.ID = Guid.NewGuid();
            //socketConnection.SocketID = Context.ConnectionId;
            //socketConnection.UserID = Guid.NewGuid().ToString();
            //socketConnectionRepository.Insert(socketConnection);
            //socketConnectionRepository.SaveChanges();
            return base.OnConnected();
        }

        public override System.Threading.Tasks.Task OnReconnected()
        {
            Groups.Add(Context.ConnectionId, "QuestionHub");
            return base.OnReconnected();
        }

        public override System.Threading.Tasks.Task OnDisconnected(bool stopCalled)
        {
            UserHandler.ConnectedIds.Remove(Context.ConnectionId);
            Groups.Remove(Context.ConnectionId, "QuestionHub");
            return base.OnDisconnected(stopCalled);
        }

        #endregion
    }
}