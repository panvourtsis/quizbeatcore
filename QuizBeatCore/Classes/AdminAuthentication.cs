﻿using QuizBeat.Models.Database;
using QuizBeatAPI.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;

namespace QuizBeat.Classes
{

    public class AdminAuthentication : ActionFilterAttribute, System.Web.Http.Filters.IAuthenticationFilter
    {
        public Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken) {
            var challenge = new AuthenticationHeaderValue("Basic");
            context.Result = new AddChallengeOnUnauthorizedResult(challenge, context.Result);
            return Task.FromResult(0);
        }

        public async Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {
            // 1. Look for credentials in the request.
            HttpRequestMessage request = context.Request;
            AuthenticationHeaderValue authorization = request.Headers.Authorization;

            // 2. If there are no credentials, do nothing.
            if (authorization == null)
            {
                context.ErrorResult = new AuthenticationFailureResult("Missing credentials", request);
            }

            //// 3. If there are credentials but the filter does not recognize the 
            ////    authentication scheme, do nothing.
            //if (authorization.Scheme != "Basic")
            //{
            //    return;
            //}

            // 4. If there are credentials that the filter understands, try to validate them.
            // 5. If the credentials are bad, set the error result.
            if (String.IsNullOrEmpty(authorization.Scheme))
            {
                context.ErrorResult = new AuthenticationFailureResult("Missing credentials", request);
                return;
            }

            UserRepository _userRepository = new UserRepository();
            IPrincipal principal = new AdminPrincipal();

            if (authorization.Scheme == "410f34cc-a9dc-4d2c-999a-b90048411e22")
            {
                context.Principal = principal;
            }
            else {
                context.ErrorResult = new AuthenticationFailureResult("Invalid username or password", request);
            }

        }
    }

    public class AdminPrincipal : IPrincipal
    {
        public IIdentity Identity { get; private set; }
        public bool IsInRole(string role) { return false; }

        public AdminPrincipal()
        {
         
        }
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public int IsAdmin { get; set; }
    }


}