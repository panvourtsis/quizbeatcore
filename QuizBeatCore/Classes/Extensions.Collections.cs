﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Core.Common
{
    public static class CollectionsExtentions
    {
        public static T Push<T>(this List<T> list, T item, int index)
        {
            list.Insert(index, item);
            return item;
        }
        public static T Pop<T>(this List<T> list, int index)
        {
            T r = list[index];
            list.RemoveAt(index);
            return r;
        }
        public static T PopFirst<T>(this List<T> list, Predicate<T> predicate)
        {
            var index = list.FindIndex(predicate);
            var r = list[index];
            list.RemoveAt(index);
            return r;
        }

        public static T PopFirstOrDefault<T>(this List<T> list, Predicate<T> predicate) where T : class
        {
            var index = list.FindIndex(predicate);
            if (index > -1)
            {
                var r = list[index];
                list.RemoveAt(index);
                return r;
            }
            return null;
        }


        public static int FirstIndexOf<T>(this List<T> source, T value,  int rangeFrom, int rangeTo = 0)
        {
            if (source == null)
                throw new ArgumentException(nameof(source));
            if(rangeTo <= rangeFrom)
                throw new ArgumentException(nameof(rangeTo));

            for(int i=rangeFrom;i < (rangeTo > 0 ? rangeTo : source.Count - 1); i++)
            {
                if(source[i].Equals(value))
                {
                    return i;
                }
            }
            return 0;
        }

        public static int FirstIndexOfLessOrEqual(this List<decimal> source, decimal value, int rangeFrom, int rangeTo = 0)
        {
            if (source == null)
                throw new ArgumentException(nameof(source));
            if (rangeTo > 0 && rangeTo <= rangeFrom)
                throw new ArgumentException(nameof(rangeTo));

            for (int i = rangeFrom; i < (rangeTo > 0 ? rangeTo : source.Count - 1); i++)
            {
                if (source[i] <= value)
                {
                    return i;
                }
            }
            return 0;
        }


        public static int LastIndexOf<T>(this List<T> source, T value, int rangeFrom, int rangeTo = 0)
        {
            if (source == null)
                throw new ArgumentException(nameof(source));
            if (rangeTo <= rangeFrom)
                throw new ArgumentException(nameof(rangeTo));

            for (int i = (rangeTo > 0 ? rangeTo : source.Count - 1); i >= rangeFrom; i--)
            {
                if (source[i].Equals(value))
                {
                    return i;
                }
            }
            return 0;
        }

        public static int LastIndexOfLessOrEqual(this List<decimal> source, decimal value, int rangeFrom, int rangeTo = 0)
        {
            if (source == null)
                throw new ArgumentException(nameof(source));
            if (rangeTo <= rangeFrom)
                throw new ArgumentException(nameof(rangeTo));

            for (int i = (rangeTo > 0 ? rangeTo : source.Count - 1); i >= rangeFrom; i--)
            {
                if (source[i] <= value)
                {
                    return i;
                }
            }
            return 0;
        }

        public static List<decimal> MovingAverage<TSource>(this List<TSource> source, Func<TSource, decimal> selector, int range)
        {
            if (selector == null)
                throw new ArgumentException(nameof(selector));
            if (source == null)
                throw new ArgumentException(nameof(source));
            if (range <= 0)
                throw new ArgumentException(nameof(range));

            List<decimal> tret = new List<decimal>();

            checked
            {
                decimal val = 0;
                int idx = 0;
                foreach (TSource item in source)
                {
                    if (idx > 0)
                    {
                        if (idx < range)
                        {
                            for (int j = 0; j < idx; j++)
                            {
                                val += selector(source[idx - j]);
                            }
                            if (val != 0) val = val / idx;
                            tret.Add(val);
                        }
                        else
                        {
                            for (int j = 0; j < range; j++)
                            {
                                val += selector(source[idx - j]);
                            }
                            if (val != 0) val = val / range;
                            tret.Add(val);
                        }
                    }
                    else
                    {
                        tret.Add(selector(item));
                    }
                    val = 0;
                    idx++;
                }
            }

            return tret;
        }


        public static Dictionary<decimal, decimal> MovingAverage(this Dictionary<decimal, List<decimal>> source, int range)
        {
            if (source == null)
                throw new ArgumentException(nameof(source));
            if (range <= 0)
                throw new ArgumentException(nameof(range));

            Dictionary<decimal, decimal> tret = new Dictionary<decimal, decimal>();

            checked
            {
                decimal val = 0;
                int idx = 0;

                List<decimal> keys = source.Select(x => x.Key).ToList();
                List<decimal> values = source.Select(x => x.Value.Average()).ToList();

                foreach (var item in values)
                {
                    if (idx > 0)
                    {
                        if (idx < range)
                        {
                            for (int j = 0; j < idx; j++)
                            {
                                val += values[idx - j];
                            }
                            if (val != 0) val = val / idx;
                            tret.Add(keys[idx], val);
                        }
                        else
                        {
                            for (int j = 0; j < range; j++)
                            {
                                val += values[idx - j];
                            }
                            if (val != 0) val = val / range;
                            tret.Add(keys[idx], val);
                        }
                    }
                    else
                    {
                        tret.Add(keys[idx], item);
                    }
                    val = 0;
                    idx++;
                }
            }

            return tret;
        }


        public static IEnumerable<T> ToIEnumerable<T>(this IEnumerator<T> enumerator)
        {
            while (enumerator.MoveNext())
            {
                yield return enumerator.Current;
            }
        }

        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            if (action == null) throw new ArgumentNullException(nameof(action));
            foreach (var element in source)
            {
                action(element);
            }
        }

        /// <summary>
        /// Immediately executes the given action on each element in the source sequence.
        /// Each element's index is used in the logic of the action.
        /// </summary>
        /// <typeparam name="T">The type of the elements in the sequence</typeparam>
        /// <param name="source">The sequence of elements</param>
        /// <param name="action">The action to execute on each element; the second parameter
        /// of the action represents the index of the source element.</param>

        public static void ForEach<T>(this IEnumerable<T> source, Action<T, int> action)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            if (action == null) throw new ArgumentNullException(nameof(action));

            var index = 0;
            foreach (var element in source)
                action(element, index++);
        }

        public static TSource GetMaxBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> selector)
        {
            return source.GetMaxBy(selector, Comparer<TKey>.Default);
        }

        public static TSource GetMaxBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> selector, IComparer<TKey> comparer)
        {
            if (source == null) throw new ArgumentNullException("source");
            if (selector == null) throw new ArgumentNullException("selector");
            if (comparer == null) throw new ArgumentNullException("comparer");
            using (var sourceIterator = source.GetEnumerator())
            {
                if (!sourceIterator.MoveNext())
                {
                    throw new InvalidOperationException("Sequence contains no elements");
                }
                var max = sourceIterator.Current;
                var maxKey = selector(max);
                while (sourceIterator.MoveNext())
                {
                    var candidate = sourceIterator.Current;
                    var candidateProjected = selector(candidate);
                    if (candidate.IsNull()) continue;
                    if (maxKey.IsNull())
                    {
                        max = candidate;
                        maxKey = candidateProjected;
                        continue;
                    }
                    if (comparer.Compare(candidateProjected, maxKey) > 0)
                    {
                        max = candidate;
                        maxKey = candidateProjected;
                    }
                }
                return max;
            }
        }

        /// <summary>
        /// Returns the only element of a sequence, or a default value if the sequence is empty; 
        /// this method DOES NOT throw an exception if there is more than one element in the sequence.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="elements"></param>
        /// <returns></returns>
        public static T SingleOrDefaultSafe<T>(this IEnumerable<T> elements)
        {
            T first = default(T);
            bool isFirst = true;
            foreach (var element in elements)
            {
                if (isFirst)
                {
                    first = element;
                    isFirst = false;
                }
                else
                {
                    first = default(T);
                    break;
                }
            }
            return first;
        }

        /// <summary>
		/// Find element index that match the given predicate<br/>
		/// Return -1 if not found<br/>
		/// </summary>
		/// <typeparam name="T">Element type</typeparam>
		/// <param name="items">Elements</param>
		/// <param name="startIndex">Start position</param>
		/// <param name="match">The predicate</param>
		/// <returns></returns>
		/// <example>
		/// <code language="cs">
		/// IList&lt;int&gt; list = new List&lt;int&gt;() { 1, 2, 4 };
		/// var index = list.FindIndex(2, x =&gt; x % 2 == 0); // 2
		/// </code>
		/// </example>
		public static int FindIndex<T>(this IList<T> items, int startIndex, Predicate<T> match)
        {
            if (startIndex < 0)
            {
                startIndex = 0;
            }
            for (int i = startIndex; i < items.Count; ++i)
            {
                if (match(items[i]))
                {
                    return i;
                }
            }
            return -1;
        }

        /// <summary>
        /// Find element index that match the given predicate<br/> Return -1 if not found<br/>
        /// </summary>
        /// <typeparam name="T">Element type</typeparam>
        /// <param name="items">Elements</param>
        /// <param name="match">The predicate</param>
        /// <returns></returns>
        /// <example>
        /// <code language="cs">
        /// IList&lt;int&gt; list = new List&lt;int&gt;() { 1, 2, 4 };
        /// var index = list.FindIndex(x =&gt; x % 2 == 0); // 1
        /// </code>
        /// </example>
        public static int FindIndex<T>(this IList<T> items, Predicate<T> match)
        {
            return FindIndex(items, 0, match);
        }

        /// <summary>
        /// Find element index that match the given predicate from back to front<br/>
        /// Return -1 if not found<br/>
        /// </summary>
        /// <typeparam name="T">Element type</typeparam>
        /// <param name="items">Elements</param>
        /// <param name="startIndex">Start position from back</param>
        /// <param name="match">The predicate</param>
        /// <returns></returns>
        /// <example>
        /// <code language="cs">
        /// IList&lt;int&gt; list = new List&lt;int&gt;() { 1, 2, 4 };
        /// var index = list.FindLastIndex(1, x => x % 2 == 0); // 1
        /// </code>
        /// </example>
        public static int FindLastIndex<T>(this IList<T> items, int startIndex, Predicate<T> match)
        {
            if (startIndex > items.Count - 1)
            {
                startIndex = items.Count - 1;
            }
            for (int i = startIndex; i >= 0; --i)
            {
                if (match(items[i]))
                {
                    return i;
                }
            }
            return -1;
        }

        /// <summary>
        /// Find element index that match the given predicate from back to front<br/>
        /// Return -1 if not found<br/>
        /// </summary>
        /// <typeparam name="T">Element type</typeparam>
        /// <param name="items">Elements</param>
        /// <param name="match">The predicate</param>
        /// <returns></returns>
        /// <example>
        /// <code language="cs">
        /// IList&lt;int&gt; list = new List&lt;int&gt;() { 1, 2, 4 };
        /// var index = list.FindLastIndex(x =&gt; x % 2 == 0); // 2
        /// </code>
        /// </example>
        public static int FindLastIndex<T>(this IList<T> items, Predicate<T> match)
        {
            return FindLastIndex(items, items.Count - 1, match);
        }

        /// <summary>
        /// Add element before the other element that match the given predicate<br/>
        /// If no other elements matched then add the element to front<br/>
        /// </summary>
        /// <typeparam name="T">Element type</typeparam>
        /// <param name="items">Elements</param>
        /// <param name="before">The predicate</param>
        /// <param name="obj">Element want's to add</param>
        /// <example>
        /// <code language="cs">
        /// var list = new List&lt;int&gt;() { 1, 2, 4 };
        /// list.AddBefore(x =&gt; x == 4, 3); // 1, 2, 3, 4
        /// </code>
        /// </example>
        public static void AddBefore<T>(this IList<T> items, Predicate<T> before, T obj)
        {
            var a = new List<int>();
            a.FindIndex(_ => true);

            var index = items.FindIndex(x => before(x));
            if (index < 0)
            {
                index = 0;
            }
            items.Insert(index, obj);
        }

        /// <summary>
        /// Add element after the other element that match the given predicate<br/>
        /// If no other elements matched then add the element to front<br/>
        /// </summary>
        /// <typeparam name="T">Element type</typeparam>
        /// <param name="items">Elements</param>
        /// <param name="after">The predicate</param>
        /// <param name="obj">Element want's to add</param>
        /// <example>
        /// <code language="cs">
        /// var list = new List&lt;int&gt;() { 1, 2, 4 };
        /// list.AddAfter(x =&gt; x == 2, 3); // 1, 2, 3, 4
        /// </code>
        /// </example>
        public static void AddAfter<T>(this IList<T> items, Predicate<T> after, T obj)
        {
            var index = items.FindLastIndex(x => after(x));
            if (index < 0)
            {
                index = items.Count - 1;
            }
            items.Insert(index + 1, obj);
        }

        /// <summary>
        /// Batch add elements<br/>
        /// </summary>
        /// <typeparam name="T">Element type</typeparam>
        /// <param name="list">Elements</param>
        /// <param name="items">Elements want's to add</param>
        /// <returns></returns>
        /// <example>
        /// <code language="cs">
        /// IList&lt;int&gt; list = new List&lt;int&gt;();
        /// list.AddRange(new[] { 1, 2, 3 });
        /// </code>
        /// </example>
        public static void AddRange<T>(this IList<T> list, IEnumerable<T> items)
        {
            foreach (var item in items)
            {
                list.Add(item);
            }
        }

    }
}
