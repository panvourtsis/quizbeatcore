﻿using Newtonsoft.Json.Linq;
using PushSharp.Apple;
using PushSharp.Core;
using PushSharp.Google;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace QuizBeat.Classes
{
    public class PushHelper
    {
        public static void sendPushNotification(string deviceToken, string notificationMessage)
        {
            // Configuration (NOTE: .pfx can also be used here)
#if DEBUG
            var config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Sandbox, System.Web.HttpContext.Current.Request.MapPath("~\\Resources\\Push\\quizbeat_dev.p12"), "kopros1");
#else
             var  config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Production, System.Web.HttpContext.Current.Request.MapPath("~\\Resources\\Push\\quizbeat_prod.p12"), "kopros1");
#endif
            // Create a new broker
            var apnsBroker = new ApnsServiceBroker(config);

            // Wire up events
            apnsBroker.OnNotificationFailed += (notification, aggregateEx) =>
            {

                aggregateEx.Handle(ex =>
                {

                    // See what kind of exception it was to further diagnose
                    if (ex is ApnsNotificationException)
                    {
                        var notificationException = (ApnsNotificationException)ex;

                        // Deal with the failed notification
                        var apnsNotification = notificationException.Notification;
                        var statusCode = notificationException.ErrorStatusCode;

                        Console.WriteLine($"Apple Notification Failed: ID={apnsNotification.Identifier}, Code={statusCode}");

                    }
                    else
                    {
                        // Inner exception might hold more useful information like an ApnsConnectionException			
                        Console.WriteLine($"Apple Notification Failed for some unknown reason : {ex.InnerException}");
                    }

                    // Mark it as handled
                    return true;
                });
            };

            apnsBroker.OnNotificationSucceeded += (notification) =>
            {
                Console.WriteLine("Apple Notification Sent!");
            };

            // Start the broker
            apnsBroker.Start();

            //foreach (var deviceToken in MY_DEVICE_TOKENS) {
            // Queue a notification to send
            apnsBroker.QueueNotification(new ApnsNotification
            {
                DeviceToken = deviceToken,
                Payload = JObject.Parse("{\"aps\":{\"alert\":\"" + notificationMessage + "\", \"sound\" : \"default\"}}")
            });

            //}

            // Stop the broker, wait for it to finish   
            // This isn't done after every message, but after you're
            // done with the broker
            apnsBroker.Stop();
        }

        public static void sendPushNotificationInBulk(List<string> deviceTokens, string notificationMessage, string notificationFilename, int deviceType = 0)
        {
            if (deviceType == 2) {

                // Configuration
                var config = new GcmConfiguration("702847811221", "AAAAo6T-fpU:APA91bFK6PUwu3iy1lEwHR6O_eRv1jU_3ePXForDVUVAhW2jUtnSL57cay26q-FKkr3LYGYwjv0qC6RcxJfxdTuOeMGeBrW81-wzg_4TsnRUm0JqxODXbWNfZRWNdftxyd1ZC3RDviAT", null);

                // Create a new broker
                var gcmBroker = new GcmServiceBroker(config);

                // Wire up events
                gcmBroker.OnNotificationFailed += (notification, aggregateEx) => {

                    aggregateEx.Handle(ex => {

                        // See what kind of exception it was to further diagnose
                        if (ex is GcmNotificationException)
                        {
                            var notificationException = (GcmNotificationException)ex;

                            // Deal with the failed notification
                            var gcmNotification = notificationException.Notification;
                            var description = notificationException.Description;

                            Console.WriteLine($"GCM Notification Failed: ID={gcmNotification.MessageId}, Desc={description}");
                        }
                        else if (ex is GcmMulticastResultException)
                        {
                            var multicastException = (GcmMulticastResultException)ex;

                            foreach (var succeededNotification in multicastException.Succeeded)
                            {
                                //Console.WriteLine($"GCM Notification Succeeded: ID={succeededNotification.MessageId}");
                                Trace.TraceError($"GCM Notification Succeeded: ID={succeededNotification.MessageId}");
                            }

                            foreach (var failedKvp in multicastException.Failed)
                            {
                                var n = failedKvp.Key;
                                var e = failedKvp.Value;

                               // Console.WriteLine($"GCM Notification Failed: ID={n.MessageId}, Desc={e.Message}");
                                Trace.TraceError($"GCM Notification Failed: ID={n.MessageId}, Desc={e.Message}");
                            }

                        }
                        else if (ex is DeviceSubscriptionExpiredException)
                        {
                            var expiredException = (DeviceSubscriptionExpiredException)ex;

                            var oldId = expiredException.OldSubscriptionId;
                            var newId = expiredException.NewSubscriptionId;

                            //Console.WriteLine($"Device RegistrationId Expired: {oldId}");
                            Trace.TraceError($"Device RegistrationId Expired: {oldId}");

                            if (!String.IsNullOrWhiteSpace(newId))
                            {
                                // If this value isn't null, our subscription changed and we should update our database
                                Trace.TraceError($"Device RegistrationId Changed To: {newId}");
                            }
                        }
                        else if (ex is RetryAfterException)
                        {
                            var retryException = (RetryAfterException)ex;
                            // If you get rate limited, you should stop sending messages until after the RetryAfterUtc date
                            Trace.TraceError($"GCM Rate Limited, don't send more until after {retryException.RetryAfterUtc}");
                        }
                        else
                        {
                            Trace.TraceError("GCM Notification Failed for some unknown reason");
                        }

                        // Mark it as handled
                        return true;
                    });
                };

                gcmBroker.OnNotificationSucceeded += (notification) => {
                    Trace.TraceError("GCM Notification Sent!");
                };

                // Start the broker
                gcmBroker.Start();

                foreach (string deviceToken in deviceTokens)
                {
                    // Queue a notification to send
                    gcmBroker.QueueNotification(new GcmNotification
                    {
                        RegistrationIds = new List<string> {
            deviceToken
        },
                        Data = JObject.Parse("{ \"message\" : \""+notificationMessage+ "\", \"soundName\": \"" + notificationFilename + "\" }")
                    });
                }

                // Stop the broker, wait for it to finish   
                // This isn't done after every message, but after you're
                // done with the broker
                gcmBroker.Stop();


            } else {
                // Configuration (NOTE: .pfx can also be used here)
#if DEBUG
                var config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Sandbox, System.Web.HttpContext.Current.Request.MapPath("~\\Resources\\Push\\quizbeat_dev.p12"), "kopros1");
#else
          var  config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Production, System.Web.HttpContext.Current.Request.MapPath("~\\Resources\\Push\\quizbeat_prod.p12"), "kopros1");
#endif
                config.InternalBatchSize = 50;
                config.InternalBatchingWaitPeriod = System.TimeSpan.FromMilliseconds(750);
                config.InternalBatchFailureRetryCount = 2;

                // Create a new broker
                var apnsBroker = new ApnsServiceBroker(config);
                apnsBroker.ChangeScale(10);
                // Wire up events
                apnsBroker.OnNotificationFailed += (notification, aggregateEx) =>
                {

                    aggregateEx.Handle(ex =>
                    {

                        // See what kind of exception it was to further diagnose
                        if (ex is ApnsNotificationException)
                        {
                            var notificationException = (ApnsNotificationException)ex;

                            // Deal with the failed notification
                            var apnsNotification = notificationException.Notification;
                            var statusCode = notificationException.ErrorStatusCode;

                            //Console.WriteLine($"Apple Notification Failed: ID={apnsNotification.Identifier}, Code={statusCode}");
                            Trace.TraceError($"Apple Notification Failed: ID={apnsNotification.Identifier}, Code={statusCode},Token={apnsNotification.DeviceToken}");

                        }
                        else
                        {
                            // Inner exception might hold more useful information like an ApnsConnectionException			
                            //Console.WriteLine($"Apple Notification Failed for some unknown reason : {ex.InnerException}");
                            Trace.TraceError($"Apple Notification Failed for some unknown reason : {ex.InnerException}");

                        }

                        // Mark it as handled
                        return true;
                    });
                };

                apnsBroker.OnNotificationSucceeded += (notification) =>
                {
                    Trace.TraceError($"Apple Notification Sent: ID={notification.Identifier},Token={notification.DeviceToken}");
                };

                // Start the broker
                apnsBroker.Start();

                foreach (string deviceToken in deviceTokens)
                {
                    var notification = new ApnsNotification
                    {
                        DeviceToken = deviceToken,
                        Payload = JObject.Parse("{\"aps\":{\"alert\":\"" + notificationMessage + "\", \"sound\" : \"" + notificationFilename + ".caf\"}}")
                    };
                    try
                    {
                        var bytes = notification.ToBytes();
                        if (bytes.Length > 0)
                        {
                            apnsBroker.QueueNotification(notification);
                        }
                    }
                    catch
                    {
                    }
                }
                // Stop the broker, wait for it to finish   
                // This isn't done after every message, but after you're
                // done with the broker
                apnsBroker.Stop();
            }
        }
    }
}