﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Newtonsoft.Json;
using System.Globalization;

namespace Core.Common
{
    public static class Dates
    {
        public static string AsSqlDate(this DateTime dt)
        {
            return string.Format("{0:yyyy-MM-dd}", dt);
        }
        public static string AsSqlDate(this DateTime? dt)
        {
            return string.Format("{0:yyyy-MM-dd}", dt);
        }

        public static string AsSqlDateExact(this DateTime dt)
        {
            return string.Format("{0:yyyy-MM-dd HH:mm:ss}", dt);
        }


        public static DateTime AsDateOnly(this DateTime obj)
        {
            return new DateTime(obj.Year, obj.Month, obj.Day, 0, 0, 0);
        }

        public static DateTime? AsDateOnly(this DateTime? obj)
        {
            return new DateTime(obj.Value.Year, obj.Value.Month, obj.Value.Day, 0, 0, 0);
        }
        
        /// <summary>
        /// Returns {0:dd/MM/yyyy}
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string AsDateString(this object obj)
        {
            if (obj.IsNull()) return string.Empty;
            if (obj is DateTime)
                return string.Format("{0:dd/MM/yyyy}", obj);
            else
                return string.Empty;
        }

        /// <summary>
        /// Returns {0:dd/MM/yyyy hh:mm:ss}
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string AsDateTimeString(this object obj)
        {
            if (obj.IsNull()) return string.Empty;
            if (obj is DateTime)
                return string.Format("{0:dd/MM/yyyy HH:mm:ss}", obj);
            else
                return string.Empty;
        }

        /// <summary>
        /// Return Json Date string e.g. "2000-04-19T16:32:33"
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string AsJsonDate(this DateTime dt)
        {
            return $"{dt.Year}-{dt.Month.AsString().PadLeft(2, '0')}-{dt.Day.AsString().PadLeft(2, '0')}T{dt.Hour.AsString().PadLeft(2, '0')}:{dt.Minute.AsString().PadLeft(2, '0')}:{dt.Second.AsString().PadLeft(2, '0')}";
        }

        public static DateTime? FromJsonDate(this string dtString)
        {
            DateTime? ret = null;
            try
            {
                ret = JsonConvert.DeserializeObject<DateTime?>(dtString);
            }
            catch (Exception)
            {
                DateTime dt = default(DateTime);
                DateTime.TryParse(dtString, out dt);
                ret = dt;
            }
            return ret;
        }


        public static DateTime BeginOfYear(this DateTime date)
        {
            return new DateTime(date.Year, 1, 1);
        }

        public static DateTime EndOfYear(this DateTime date)
        {
            return new DateTime(date.Year, 12, 31);
        }

        public static DateTime HalfOfYear(this DateTime date)
        {
            return new DateTime(date.Year, 6, 30);
        }

        public static DateTime BeginOfMonth(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, 1);
        }

        public static DateTime EndOfMonth(this DateTime date)
        {
            var lastdate = date.EndOfYear().AddMonths(-(12 - date.Month));
            return new DateTime(date.Year, date.Month, lastdate.Day);
        }

        public static int GetQuarter(this DateTime date)
        {
            return (int)((date.Month - 1) / 3) + 1;
        }

        public static DateTime BeginOfQuarter(this DateTime date)
        {
            int q = date.GetQuarter();
            return new DateTime(date.Year, (q - 1) * 3 + 1, 1);
        }

        public static DateTime EndOfQuarter(this DateTime date)
        {
            int q = date.GetQuarter();
            return (new DateTime(date.Year, q * 3, 1)).EndOfMonth();
        }

        public static string ToMonthName(this DateTime dateTime)
        {
            return CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dateTime.Month);
        }

        public static int GetWeek(this DateTime dateTime)
        {
            DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
            Calendar cal = dfi.Calendar;
            return cal.GetWeekOfYear(dateTime, dfi.CalendarWeekRule, dfi.FirstDayOfWeek);
        }

        /// <summary>
        /// Get Orthodox easter for requested year
        /// </summary>
        /// <param name="year">Year of easter</param>
        /// <returns>DateTime of Orthodox Easter</returns>
        public static DateTime GetOrthodoxEaster(this DateTime date, int year)
        {
            int a = year % 19;
            int b = year % 7;
            int c = year % 4;

            int d = (19 * a + 16) % 30;
            int e = (2 * c + 4 * b + 6 * d) % 7;
            int f = (19 * a + 16) % 30;
            int key = f + e + 3;

            int month = (key > 30) ? 5 : 4;
            int day = (key > 30) ? key - 30 : key;

            return new DateTime(year, month, day);
        }

        /// <summary>
        /// Get Catholic easter for requested year
        /// </summary>
        /// <param name="year">Year of easter</param>
        /// <returns>DateTime of Catholic Easter</returns>
        public static DateTime GetCatholicEaster(this DateTime date, int year)
        {
            int month = 3;
            int G = year % 19 + 1;
            int C = year / 100 + 1;
            int X = (3 * C) / 4 - 12;
            int Y = (8 * C + 5) / 25 - 5;
            int Z = (5 * year) / 4 - X - 10;
            int E = (11 * G + 20 + Y - X) % 30;
            if (E == 24) { E++; }
            if ((E == 25) && (G > 11)) { E++; }
            int N = 44 - E;
            if (N < 21) { N = N + 30; }
            int P = (N + 7) - ((Z + N) % 7);
            if (P > 31)
            {
                P = P - 31;
                month = 4;
            }
            return new DateTime(year, month, P);
        }


        public static double ToJsTimestamp(this DateTime dt)
        {
            DateTime unixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime utcNow = dt;  
            var elapsedTime = utcNow - unixEpoch;
            double millis = elapsedTime.TotalMilliseconds;
            return millis;
        }

    }
}
