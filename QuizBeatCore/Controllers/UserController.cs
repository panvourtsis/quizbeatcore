﻿using Core.AspNet.Services;
using Core.Common;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using QuizBeat.Classes;
using QuizBeat.Models.Database;
using QuizBeat.Models.Dto;
using QuizBeat.Models.Options;
using QuizBeatCore.Services;
using RNCryptor;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace QuizBeat.Controllers
{
    [Produces("application/json")]
    [Route("api/user")]
    [Controller]
    public class UserController : BaseController
    {
        #region Constants
        private const string urlSandbox = "https://sandbox.itunes.apple.com/verifyReceipt";
        private const string urlProduction = "https://buy.itunes.apple.com/verifyReceipt";
        #endregion

        private IImageStorageService imgSvc = null;

        public UserController(IContextService ctx, IHostingEnvironment env, IConfiguration c, IImageStorageService img, IQbDataService da, IDbCacheService dbc)
            : base(ctx, env, c, da, dbc)
        {
            imgSvc = img;
        }




        [HttpGet]
        [Route("checkusername")]
        public async Task<IActionResult> UsernameExists(string username)
        {
            bool userExists = await dbSvc.DoesUserExist(null, username, null);
            return Json(new { userExists = userExists });
        }

        [HttpGet]
        [Route("checkemail")]
        public async Task<IActionResult> EmailExists(string email)
        {
            bool emailExists = await dbSvc.DoesUserExist(null, null, email);
            return Json(new { emailExists = emailExists });
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> LoginUser([FromBody] UserOption userOption)
        {
            try
            {

                if (userOption.Password.IsNullOrEmpty() || userOption.Email.IsNullOrEmpty())
                {
                    return Unauthorized();
                }

                using (DataSvc da = DataSvc.GetDataService())
                {
                    User user = await da.GetUserByEmail(userOption.Email);

                    var md5 = new MD5CryptoServiceProvider();
                    byte[] md5data = md5.ComputeHash(new UTF8Encoding().GetBytes(userOption.Password));
                    ASCIIEncoding encoder = new ASCIIEncoding();
                    var hashedPassword = encoder.GetString(md5data);

                    Boolean userIsAuthenticated = hashedPassword == user.Password;
                    if (!userIsAuthenticated || !user.IsApproved)
                    {
                        return StatusCode(HttpStatusCode.Conflict.AsInt());
                    }

                    user.AuthToken = da.NEWGUID();

                    await da.UpdateUser(user);

                    return Ok(new { authToken = user.AuthToken });
                }
            }
            catch (Exception ex)
            {
                LogError(ex, ex.Message, new object[] { });
                return BadRequest(new ResponseMessage() { Message = ErrorCode.CouldNotCreateUser.ToString() });
            }
        }

        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> RegisterUser([FromBody]UserOption userOption)
        {
            if (userOption == null) return BadRequest(ErrorCode.CouldNotCreateUser.ToString());

            try
            {
                using (DataSvc da = DataSvc.GetDataService())
                {

                    var qrEncryptor = new Decryptor();

                    String emailValidation = "";
                    if (userOption.HashString.IsNullOrEmpty()) return BadRequest("Incomplete Request");
                    if (Request.Headers["Client"] == "iphone")
                    {
                        emailValidation = qrEncryptor.Decrypt(userOption.HashString, "ZHBFFcTzcrYcb$Z-&hv8T7fwpY4TSt");
                    }
                    else
                    {
                        emailValidation = QBEncryption.Decrypt(userOption.HashString, "ZHBFFcTzcrYcb$Z-&hv8T7fwpY4TSt");
                    }

                    if (emailValidation != userOption.Email)
                    {
                        return BadRequest(emailValidation + "-" + userOption.Email);
                    }

                    // TODO: Search in one call?
                    bool usernameExists = await da.DoesUserExist(null, userOption.Username, null);
                    if (usernameExists)
                    {
                        return BadRequest(new ResponseMessage() { Message = ErrorCode.UsernameInUse.ToString() });
                    }

                    bool emailExists = await da.DoesUserExist(null, null, userOption.Email);
                    if (emailExists)
                    {
                        return BadRequest(new ResponseMessage() { Message = ErrorCode.EmailInUse.ToString() });
                    }

                    User userThatInvitedMe = null;
                    if (userOption.InvitationCode != null && userOption.InvitationCode.Length > 0)
                    {
                        userThatInvitedMe = await da.GetUserByUserName(userName: userOption.InvitationCode);
                        if (userThatInvitedMe == null)
                        {
                            // return BadRequest(ErrorCode.InvitationCodeNotFound.ToString());
                        }
                    }

                    userOption.ID = Guid.NewGuid().ToString();
                    userOption.ExtraLives = 1;
                    userOption.Money_Won = 0;
                    userOption.RankIndex = 1000;

                    var md5 = new MD5CryptoServiceProvider();
                    byte[] md5data = md5.ComputeHash(new UTF8Encoding().GetBytes(userOption.Password));
                    ASCIIEncoding encoder = new ASCIIEncoding();
                    userOption.Password = encoder.GetString(md5data);
                    userOption.AuthToken = Guid.NewGuid().ToString();
                    User user = userOption.ConvertToUser();
                    user.IsApproved = true;

                    await da.InsertUser(user);


                    if (userOption.InvitationCode != null && userOption.InvitationCode.Length > 0 && userThatInvitedMe != null)
                    {
                        Invitation invitation = new Invitation();
                        invitation.ID = Guid.NewGuid().ToString();
                        invitation.UserID = userThatInvitedMe.ID;
                        invitation.InvitedUserID = userOption.ID;
                        invitation.HasBeenRedeemed = false;
                        invitation.Date = DateTime.UtcNow;
                        await da.InsertInvitation(invitation);
                    }

                }

                return Ok(new { authToken = userOption.AuthToken });
            }
            catch (Exception ex)
            {
                LogError(ex, ex.Message, new object[] { });
                return BadRequest(new ResponseMessage() { Message = ErrorCode.CouldNotCreateUser.ToString() });
            }
        }

        [HttpGet]
        [Route("profile")]
        public async Task<IActionResult> GetProfile()
        {
            try
            {
                GetAuthorization();

                using (DataSvc da = DataSvc.GetDataService())
                {
                    User user = await GetUser(da, authToken: authorization, fromCache: false);
                    if (user == null) return BadRequest(ErrorCode.ValidationError.ToString());

                    user.LastActive_At = DateTime.UtcNow;
                    await da.UpdateUserLastActive(user);

                    UserDto userDto = new UserDto(user);
                    userDto.Email = user.Email;
                    return Ok(userDto);
                }
            }
            catch (Exception ex)
            {
                LogError(ex, ex.Message, new object[] { });
                return BadRequest(ErrorCode.CouldNotCreateUser.ToString());
            }
        }



        [HttpPatch]
        [Route("update")]
        public async Task<IActionResult> Update([FromBody]JObject data)
        {
            GetAuthorization();

            try
            {
                bool hasSharedAppOnFacebook = false;
                bool hasSharedAppOnTwitter = false;
                using (DataSvc da = DataSvc.GetDataService())
                {
                    User user = await GetUser(da, authToken: authorization, fromCache: false);
                    if (user == null) return BadRequest(ErrorCode.ValidationError.ToString());

                    if (data.ContainsKey("hasSharedAppOnFacebook"))
                    {
                        hasSharedAppOnFacebook = (bool)data["hasSharedAppOnFacebook"];
                    }


                    if (data.ContainsKey("hasSharedAppOnTwitter"))
                    {
                        hasSharedAppOnTwitter = (bool)data["hasSharedAppOnTwitter"];
                    }


                    if (hasSharedAppOnFacebook == true && (user.HasSharedAppOnFacebook == false || user.HasSharedAppOnFacebook == null))
                    {
                        user.HasSharedAppOnFacebook = true;
                        user.ExtraLives = user.ExtraLives + 1;
                    }

                    if (hasSharedAppOnTwitter == true && (user.HasSharedAppOnTwitter == false || user.HasSharedAppOnTwitter == null))
                    {
                        user.HasSharedAppOnTwitter = true;
                        user.ExtraLives = user.ExtraLives + 1;
                    }

                    await da.UpdateUser(user);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                LogError(ex, ex.Message, new object[] { });
                return BadRequest(ErrorCode.CouldNotCreateUser.ToString());
            }
        }

        [HttpGet]
        [Route("updatePushtoken")]
        public async Task<IActionResult> UpdatePushtoken(string pushToken)
        {
            GetAuthorization();


            try
            {
                using (DataSvc da = DataSvc.GetDataService())
                {
                    User user = await GetUser(authToken: authorization, fromCache: false);
                    if (user == null) return BadRequest(ErrorCode.ValidationError.ToString());
                    if (Request.Headers["Client"] == "iphone")
                    {
                        user.DeviceType = 1;
                    }
                    else
                    {
                        user.DeviceType = 2;
                    }

                    user.PushToken = pushToken;
                    await da.UpdateUser(user);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                LogError(ex, ex.Message, new object[] { });
                return BadRequest(ErrorCode.CouldNotCreateUser.ToString());
            }
        }

        [HttpPost]
        [Route("checkForceUpdate")]
        public async Task<IActionResult> CheckForceUpdate([FromBody]AppOption appOption)
        {
            GetAuthorization();

            try
            {
                User user = await GetUser(authToken: authorization, fromCache: false);
                if (user == null) return BadRequest(ErrorCode.ValidationError.ToString());

                if (appOption.Version == "1.2.3" || appOption.Version == "1.2.5" || appOption.Version == "1.6.1" || appOption.Version == "1.6.2" || appOption.Version == "1.6.3")
                {
                    return Ok(new { showPopup = "false" });
                }
                else
                {
                    return Ok(new { showPopup = "true" });
                }
            }
            catch (Exception ex)
            {
                LogError(ex, ex.Message, new object[] { });
                return BadRequest(ErrorCode.CouldNotCreateUser.ToString());
            }
        }

        private class InvUser
        {
            [JsonProperty(PropertyName = "HasBeenRedeemed")]
            public bool? HasBeenRedeemed { get; set; } = false;
            [JsonProperty(PropertyName = "Username")]
            public string Username { get; set; } = "";

        }

        [HttpGet]
        [Route("invitedContacts")]
        public async Task<IActionResult> InvitedContacts()
        {
            GetAuthorization();
            try
            {
                using (DataSvc da = DataSvc.GetDataService())
                {
                    User user = await GetUser(da, authToken: authorization, fromCache: false);
                    if (user == null) return BadRequest(ErrorCode.ValidationError.ToString());

                    IEnumerable<Invitation> invitations = await da.FindInvitations(user.ID);  //.ToList<Invitation>();
                    return Ok(invitations.Select(x => new InvUser()
                    {
                        HasBeenRedeemed = x.HasBeenRedeemed,
                        Username = x.User.Username
                    }));
                }
            }
            catch (Exception ex)
            {
                LogError(ex, ex.Message, new object[] { });
                return BadRequest();
            }

        }

        [HttpGet]
        [Route("checkInfoMessage")]
        public IActionResult CheckInfoMessage([FromBody]AppOption appOption)
        {
            return Ok();
            //       InfoMessageDto messageDto = new InfoMessageDto();
            //     messageDto.ID = "2";
            //   messageDto.Title = "Σημαντική Ενημέρωση";
            //   messageDto.Subtitle = "Tο σημερινό μας επεισόδιο μεταφέρεται αύριο Τετάρτη 20:45 λόγω τεχνικής αναβάθμισης! Σας περιμένουμε!!!";
            // messageDto.ButtonText = "OK";
            //      messageDto.Show = true;

            //    return Ok(messageDto);
        }



        [HttpPost]
        [Route("checkReceiptWithAppStore")]
        public async Task<HttpResponseMessage> CheckReceiptWithAppStore()
        {
            GetAuthorization();

            User user = await GetUser(authToken: authorization, fromCache: false);

            if (user == null) return new HttpResponseMessage(HttpStatusCode.BadRequest);
            /*
            var uploadPath = hostEnv.WebRootPath + @"/Temp/Receipt";
            Directory.CreateDirectory(uploadPath);
            var provider = new MultipartFormDataStreamProvider(uploadPath);
            MultipartFormDataStreamProvider result = await ctxSvc.GetContext().ReadAsMultipartAsync(provider);

            string uri = urlSandbox;
            string receiptData = System.IO.File.ReadAllText(result.FileData[0].LocalFileName);

            int extraLives = 0;
            if (receiptData.Contains("com.growthhero.quizbeat.20lives"))
            {
                extraLives = 20;
            }
            else if (receiptData.Contains("com.growthhero.quizbeat.10lives"))
            {
                extraLives = 10;
            }
            else if (receiptData.Contains("com.growthhero.quizbeat.1live"))
            {
                extraLives = 1;
            }
            else if (receiptData.Contains("com.growthhero.quiz_master"))
            {
                extraLives = 1;
            }
            var json = new JObject(new JProperty("receipt-data", receiptData),
                new JProperty("password", "39333afb25214d2a9a08cd20fffe3bbc")).ToString();

            using (var httpClient = new HttpClient())
            {
                if (receiptData != null)
                {
                    HttpContent content = new StringContent(json);
                    try
                    {
                        Task<HttpResponseMessage> getResponse = httpClient.PostAsync(uri, content);
                        HttpResponseMessage response = await getResponse;
                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            user.ExtraLives = user.ExtraLives + extraLives;

                            LivesBought livesBought = new LivesBought();
                            livesBought.ID = Guid.NewGuid().ToString();
                            livesBought.DateCreated = DateTime.UtcNow;
                            livesBought.NumberOfLives = extraLives;
                            livesBought.UserID = user.ID;
                            livesBought.ReceiptData = receiptData;
                            _databaseRepository.InsertLivesBought(livesBought);

                            Donation donation = _databaseRepository.FindDonation(isActive: true);
                            donation.MoneyReached = donation.MoneyReached + extraLives;

                            await _databaseRepository.SaveChanges();

                            System.IO.File.Delete(result.FileData[0].LocalFileName);
                        }
                    }
                    catch (Exception ex)
                    {
                        LogError(ex, ex.Message, new object[] { });
                        //Core.Common.Diagnostics.Trace.Log(ex);
                        Console.WriteLine("Error verifying receipt: " + e.Message);
                    }
                }
            }

            */
            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        [HttpPost]
        [Route("checkReceiptWithPlayStore")]
        public async Task<IActionResult> CheckReceiptWithPlayStore([FromBody]JObject data)
        {
            GetAuthorization();

            try
            {
                using (DataSvc da = DataSvc.GetDataService())
                {
                    User user = await GetUser(da, authToken: authorization);
                    if (user == null) return BadRequest(ErrorCode.ValidationError.ToString());

                    string sku = data["sku"].ToString();
                    string token = data["token"].ToString();
                    string encrypted = data["encrypted"].ToString();

                    int extraLives = 0;
                    if (sku == "life_20")
                        extraLives = 20;
                    else if (sku == "life_10")
                        extraLives = 10;
                    else if (sku == "life_1")
                        extraLives = 1;
                    else if (sku == "android.test.purchased")
                        extraLives = 3;

                    var qrEncryptor = new Decryptor();

                    String tokenValidation = QBEncryption.Decrypt(encrypted, "ZHBFFcTzcrYcb$Z-&hv8T7fwpY4TSt");
                    if (token == tokenValidation)
                    {
                        user.ExtraLives = user.ExtraLives + extraLives;
                        await da.UpdateUser(user);

                        LivesBought livesBought = new LivesBought();
                        livesBought.ID = Guid.NewGuid().ToString();
                        livesBought.DateCreated = DateTime.UtcNow;
                        livesBought.NumberOfLives = extraLives;
                        livesBought.UserID = user.ID;
                        livesBought.ReceiptData = token;
                        await da.InsertLivesBought(livesBought);

                        Donation donation = await da.FindDonation(isActive: true);
                        donation.MoneyReached = donation.MoneyReached + extraLives;

                        await da.UpdateDonation(donation);
                    }
                }
                return Ok();
            }
            catch (Exception ex)
            {
                LogError(ex, ex.Message, new object[] { });
                return BadRequest(ErrorCode.UndefinedError.ToString());
            }
            // return BadRequest(ErrorCode.UndefinedError.ToString());
        }



        [HttpPost]
        [Route("postBlobImage")]
        public async Task<IActionResult> PostBlobImage()   //[FromBody] jsonImageModel model
        {

            GetAuthorization();

            try
            {
                User user = await GetUser(authToken: authorization, fromCache: false);
                if (user == null) return Unauthorized();

                if (Request.Form.Files.Count > 0)
                {
                    var file = Request.Form.Files[0];
                    bool resizeImage = true;  //@@@ resize flag
                    byte[] fileData = null;

                    if (resizeImage)
                    {
                        Image image = Image.FromStream(file.OpenReadStream(), true, true);
                        var newImage = new Bitmap(image.Width, image.Height);
                        Size imgSize = Core.Helpers.Imaging.GetThumbnailSize(image, 200);
                        fileData = Core.Helpers.Imaging.imageToByteArray(Core.Helpers.Imaging.Resize(new Bitmap(image), imgSize.Width, imgSize.Height, 8, ImageFormat.Jpeg));
                    }
                    else
                    {
                        using (var binaryReader = new BinaryReader(file.OpenReadStream()))
                        {
                            fileData = binaryReader.ReadBytes(file.Length.AsInt());
                        }
                    }

                    string url = await imgSvc.StoreImage(file.FileName, fileData);

                    return Ok(url);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                LogError(ex, ex.Message, new object[] { });
                return BadRequest(ErrorCode.UndefinedError.ToString());
            }
        }


        [HttpGet("getUserImage")]
        public async Task<FileResult> GetUserImage(string userid)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                dynamic data = await dbSvc.GetUserImage(userid);
                if (data != null)
                {
                    byte[] e = data.IMAGE as byte[];
                    string fileName = data.FILENAME;
                    if (!e.IsNullOrEmpty())
                    {
                        return base.File(e, $"image/jpg", $"{Path.GetFileNameWithoutExtension(fileName)}.jpg");
                    }
                }
            }
            return base.File(new byte[] { }, "image/jpg", $"{userid}.jpg"); ;
        }



        [HttpPost]
        [Route("createCashOutRequest")]
        public async Task<IActionResult> CreateCashOutRequest([FromBody]JObject data)
        {
            GetAuthorization();


            try
            {
                string IBAN = data["IBAN"].ToString();
                string fullName = data["fullName"].ToString();

                using (DataSvc da = DataSvc.GetDataService())
                {
                    User user = await GetUser(da, authToken: authorization, fromCache: false);
                    if (user == null) return BadRequest(ErrorCode.ValidationError.ToString());

                    CashOut pendingCashout = await da.FindPendingCashOut(userId: user.ID);
                    if (pendingCashout != null) return BadRequest(ErrorCode.PendingCashOutRequestFound.ToString());

                    CashOut cashout = new CashOut();
                    cashout.ID = Guid.NewGuid().ToString();
                    cashout.UserID = user.ID;
                    cashout.Money = user.Money_Won - user.Money_CashedOut;
                    cashout.Date = DateTime.UtcNow;
                    cashout.IsApproved = false;
                    cashout.IBAN = IBAN;
                    cashout.FullName = fullName;
                    cashout.Type = "CashOutRequest";

                    await da.InsertCashOut(cashout);

                    user.Money_CashedOut = user.Money_CashedOut + user.Money_Won;

                    await da.UpdateUser(user);
                }
                return Ok();

            }
            catch (Exception ex)
            {
                LogError(ex, ex.Message, new object[] { });
                return BadRequest("Some error occured.");
            }

        }

        [HttpGet]
        [Route("resetPassword")]
        public async Task<IActionResult> ResetPasswordAsync(string email)
        {
            try
            {
                using (DataSvc da = DataSvc.GetDataService())
                {
                    User user = await da.GetUserByEmail(email.Trim());
                    if (user == null) return Ok();

                    string passToken = Guid.NewGuid().ToString();
                    user.ResetPasswordToken = passToken;
                    user.ResetPasswordRequestedDate = DateTime.Now;
                    await da.UpdateUserReset(user);

                    var apiKey = "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";
                    var client = new SendGridClient(apiKey);
                    var from = new EmailAddress("info@quizbeat.co", "QuizBeat Support");
                    var subject = "Quizbeat - Reset Password.";
                    var to = new EmailAddress(email, user.Username);
                    string url = @"https:\\quizbeat.azurewebsites.net\Home\ResetPassword?ResetPasswordToken=" + passToken;
                    var htmlContent = $@"<html><body><p>Εαν ξέχασες το password σου και ζήτησες να το επαναφέρεις κλικαρε στο παρακάτω link.</p><p><a href=""{ url}"">Επανάκτηση Κωδικού</a> </p><p>Αν δεν δουλεύει το παραπάνω link, κάνε copy-paste το παρακάτω url στον browser σου: <br>{url}</p><p>Εαν δεν ζητησες επαναφορά αγνοήσε το mail.</p></body></html>";
                    var plainTextContent = "";
                    var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
                    var response = await client.SendEmailAsync(msg);
                }

                return Ok();
            }
            catch (Exception ex)
            {
                LogError(ex, ex.Message, new object[] { });
                return BadRequest(ErrorCode.CouldNotCreateUser.ToString());
            }
        }


        public class jreset
        {
            public string password { get; set; }
            public string token { get; set; }
        }

        [HttpGet]
        [Route("resetPasswordWithToken")]
        public async Task<IActionResult> ResetPasswordWithToken([FromQuery]jreset data)
        {
            string token = data.token;
            string password = data.password;
            try
            {
                using (DataSvc da = DataSvc.GetDataService())
                {
                    User user = await da.FindUserWithResetPassword(resetPasswordToken: token);

                    if (user == null) return BadRequest(ErrorCode.CouldNotUpdateUser.ToString());

                    var md5 = new MD5CryptoServiceProvider();
                    byte[] md5data = md5.ComputeHash(new UTF8Encoding().GetBytes(password));
                    ASCIIEncoding encoder = new ASCIIEncoding();
                    user.Password = encoder.GetString(md5data);

                    user.ResetPasswordToken = null;
                    user.ResetPasswordRequestedDate = null;
                    await da.UpdateUser(user);
                }
                return Ok(new { success = "Valid" });

            }
            catch (Exception ex)
            {
                LogError(ex, ex.Message, new object[] { });
                return BadRequest(ErrorCode.CouldNotUpdateUser.ToString());
            }
        }

    }

    public class CustomMultipartFormDataStreamProvider : MultipartFormDataStreamProvider
    {
        public CustomMultipartFormDataStreamProvider(string path) : base(path) { }

        public override string GetLocalFileName(HttpContentHeaders headers)
        {
            return headers.ContentDisposition.FileName.Replace("\"", string.Empty);
        }
    }

    public enum ErrorCode
    {
        UndefinedError,
        UsernameInUse,
        EmailInUse,
        InvitationCodeNotFound,
        RecordNotFound,
        CouldNotCreateUser,
        CouldNotUpdateUser,
        CouldNotDeleteUser,
        PendingCashOutRequestFound,
        MaximumBetaUsersReached,
        ValidationError

    }
}
