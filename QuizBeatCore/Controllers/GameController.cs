﻿using Core.AspNet.Services;
using Core.Common;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using QuizBeat.Models.Database;
using QuizBeat.Models.Dto;
using QuizBeatCore.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace QuizBeat.Controllers
{
    [Route("api/game")]
    public class GameController : BaseController
    {

        public GameController(IContextService ctx, IHostingEnvironment env, IConfiguration c, IQbDataService da, IDbCacheService dbc) : 
            base(ctx, env, c, da, dbc)
        {

        }


        [HttpGet]
        [Route("GetActiveSocketGame")]
        public async Task<IActionResult> GetActiveSocketGame()
        {

            Game g = await GetActiveGame(true, true);
            if (g != null)
            {
                return Ok(g);
            }
            return NotFound();
        }



        [HttpGet]
        [Route("GetStartedSocketGame")]
        public async Task<IActionResult> GetStartedSocketGame()
        {

            Game g = await GetActiveGame(true, true);
            if (g != null)
            {
                return Ok(g);
            }
            return NotFound();
        }

        [HttpGet]
        [Route("EndSocketGame")]
        public async Task<IActionResult> EndSocketGame()
        {
            if (Request.Headers.ContainsKey("AuthToken"))
            {
                if (Request.Headers["AuthToken"] != ADMIN_AUTH_TOKEN)
                {
                    return NotFound();
                }

                using (DataSvc da = DataSvc.GetDataService())
                {
                    await ClearCache(da);
                    await ClearCacheAsync(da);
                }

                int availableWorker, availableIO;
                int maxWorker, maxIO;

                ThreadPool.GetAvailableThreads(out availableWorker, out availableIO);
                ThreadPool.GetMaxThreads(out maxWorker, out maxIO);

                return Ok(new
                {
                    processorCounter = System.Environment.ProcessorCount,
                    AvailableWorkerThreads = availableWorker,
                    MaxWorkerThreads = maxWorker,
                    OccupiedThreads = maxWorker - availableWorker
                });

            }
            else
            {
                return NotFound();
            }
        }

        [HttpGet]
        [Route("GameStarted")]
        public async Task<IActionResult> GameStarted()
        {
            if (Request.Headers.ContainsKey("AuthToken"))
            {
                if (Request.Headers["AuthToken"] != ADMIN_AUTH_TOKEN)
                {
                    return NotFound();
                }

                Game g = await GetActiveGame();
                if (g != null)
                {
                    g.IsActive = true;
                    g.HasStarted = true;
                    await SetGame(g);
                }

                return Ok(g);
            }
            return NotFound();
        }


        [HttpGet]
        [Route("GameInit")]
        public async Task<IActionResult> GameInit()
        {
            if (Request.Headers.ContainsKey("AuthToken"))
            {
                if (Request.Headers["AuthToken"] != ADMIN_AUTH_TOKEN)
                {
                    return NotFound();
                }

                try
                {
                    using (DataSvc da = DataSvc.GetDataService())
                    {
                        await ClearCache(da);
                        await ClearCacheAsync(da);
                    }

                    var gcBefore = GC.GetTotalMemory(false) / 1024 / 1024;
                    Process proc = Process.GetCurrentProcess();
                    var memBefore = proc.WorkingSet64 / 1024 / 1024;
                    var wksetBefore = Environment.WorkingSet / 1024 / 1024;
                    int usrCount = 0;
                    long sec1 = 0, sec2 = 0;
                    int errors = 0;
                    int totalSize = 0;
                    using (DataSvc da = DataSvc.GetDataService())
                    {
                        var watch = System.Diagnostics.Stopwatch.StartNew();
                        List<User> ls = await da.ExecuteListAsync<User>("select top 10000 * from [User] order by LastActive_At desc", da.Args());
                        if (ls.Count > 0)
                        {
                            usrCount = ls.Count;
                            foreach (User u in ls)
                            {
                                await SetUser(da, u);
                                totalSize += u.AsJson().Length;
                            }
                            watch.Stop();
                            sec1 = watch.ElapsedMilliseconds / 60;
                            watch = System.Diagnostics.Stopwatch.StartNew();
                            foreach (User u in ls)
                            {
                                User test1 = await GetUser(email: u.Email, fromCache: true);
                                if (test1 == null) errors++;
                                User test2 = await GetUser(username: u.Username, fromCache: true);
                                if (test2 == null) errors++;
                                if (u.AuthToken.HasValue)
                                {
                                    User test3 = await GetUser(authToken: u.AuthToken.Value, fromCache: true);
                                    if (test3 == null) errors++;
                                }
                            }
                            watch.Stop();
                            sec2 = watch.ElapsedMilliseconds / 60;

                        }

                    }

                    var gcAfter = GC.GetTotalMemory(false) / 1024 / 1024;
                    proc = Process.GetCurrentProcess();
                    var memAfter = proc.WorkingSet64 / 1024 / 1024;
                    var wksetAfter = Environment.WorkingSet / 1024 / 1024;


                    int availableWorker, availableIO;
                    int maxWorker, maxIO;

                    ThreadPool.GetAvailableThreads(out availableWorker, out availableIO);
                    ThreadPool.GetMaxThreads(out maxWorker, out maxIO);

                    return Ok(new
                    {
                        usersCount = usrCount,
                        totalSize = $"{totalSize / 1024 / 1024} Mb",
                        secsToLoad = sec1,
                        secsToParse = sec2,
                        parseErrors = errors,
                        WorkingSetBefore = $"{wksetBefore} Mb",
                        WorkingSetAfter = $"{wksetAfter} Mb",
                        GCBefore = $"{gcBefore} Mb",
                        GCAfter = $"{gcAfter} Mb",
                        procBefore = $"{memBefore} Mb",
                        procAfter = $"{memAfter} Mb",
                        processorCounter = System.Environment.ProcessorCount,
                        AvailableWorkerThreads = availableWorker,
                        MaxWorkerThreads = maxWorker,
                        OccupiedThreads = maxWorker - availableWorker
                    });
                }
                catch (Exception ex)
                {
                    LogError(ex, ex.Message, null);

                    return Ok(new
                    {
                        error = ex.Message
                    });
                }
            }
            return NotFound();
        }

        [HttpGet]
        [Route("PreheatGame")]
        public async Task<IActionResult> PreheatGame()
        {
            if (Request.Headers.ContainsKey("AuthToken"))
            {
                if (Request.Headers["AuthToken"] != ADMIN_AUTH_TOKEN)
                {
                    return NotFound();
                }

                try
                {
                    await Task.Yield();

                    int availableWorker, availableIO;
                    int maxWorker, maxIO;

                    ThreadPool.GetAvailableThreads(out availableWorker, out availableIO);
                    ThreadPool.GetMaxThreads(out maxWorker, out maxIO);

                    return Ok(new
                    {
                        usersCacheCount = cacheSvc.Count(),
                        processorCounter = System.Environment.ProcessorCount,
                        AvailableWorkerThreads = availableWorker,
                        MaxWorkerThreads = maxWorker,
                        OccupiedThreads = maxWorker - availableWorker
                    });
                }
                catch (Exception ex)
                {
                    LogError(ex, ex.Message, null);

                    return Ok(new
                    {
                        error = ex.Message
                    });

                }
            }
            return NotFound();
        }


        [HttpGet]
        [Route("NextQuestion")]
        public async Task<IActionResult> NextQuestion()
        {
            if (Request.Headers.ContainsKey("AuthToken"))
            {
                if (Request.Headers["AuthToken"] != ADMIN_AUTH_TOKEN)
                {
                    return NotFound();
                }
                try
                {
                    using (DataSvc da = DataSvc.GetDataService())
                    {
                        /*
                         *  Load full game object
                         */
                        Game game = await da.FindActiveGame(true, true);
                        /*
                         * save cache 
                         */
                        await SetGame(game);
                        return Ok(game);
                    }
                }
                catch (Exception ex)
                {
                    return NotFound(ex.Message);
                }
            }
            return NotFound();
        }



        [HttpGet]
        [Route("next")]
        public async Task<IActionResult> GetNext()
        {
            GetAuthorization();
            User user = await GetUser(authToken: authorization);

            if (user == null)
            {
                return BadRequest(ErrorCode.ValidationError.ToString());
            }

            try
            {
                Game game = await GetNextGame();
                if (game != null)
                {
                    GameDto gameDto = new GameDto(game);
                    return Ok(gameDto);
                }
                else
                {
                    return BadRequest("Some error occured.");
                }
            }
            catch (Exception ex)
            {
                LogError(ex, ex.Message, new object[] { });
                return BadRequest("Some error occured.");
            }
        }



        [HttpGet]
        [Route("active")]
        public async Task<IActionResult> GetActive()
        {
            GetAuthorization();
            try
            {
                using (DataSvc da = DataSvc.GetDataService())
                {
                    User user = await GetUser(da, authToken: authorization);

                    if (user == null)
                    {
                        return BadRequest(ErrorCode.ValidationError.ToString());
                    }


                    Game game = await GetRunningGame(da, includeQuestions: false);
                    if (game != null)
                    {
                        if (game.Prize == null) game.Prize = await da.FindGamePrize(game.PrizeID);
                        GameDto gameDto = new GameDto(game: game);
#if DEBUG
                        gameDto.StreamingURLString = "rtmp://94.229.67.38/qbTVStaging/test";
#else
                    gameDto.StreamingURLString = "rtmp://94.229.67.38/live/test";
#endif
                        return Ok(gameDto);
                    }
                    else
                    {
                        return NotFound();
                    }
                }
            }
            catch (Exception ex)
            {
                LogError(ex, ex.Message, new object[] { });
                return BadRequest("Some error occured.");
            }
        }

        [HttpGet]
        [Route("activetest")]
        public async Task<IActionResult> GetActiveTest()
        {
            GetAuthorization();

            try
            {

                using (DataSvc da = DataSvc.GetDataService())
                {
                    User user = await GetUser(da, authToken: authorization);
                    if (user == null)
                        return BadRequest(ErrorCode.ValidationError.ToString());

                    Game game = await GetActiveGame(da, includeQuestions: false);
                    if (game != null)
                    {
                        GameDto gameDto = new GameDto(game: game);
#if DEBUG
                        gameDto.StreamingURLString = "rtmp://94.229.67.38/qbTVStaging/test";
#else
                    gameDto.StreamingURLString = "rtmp://94.229.67.38/live/test";
#endif
                        return Ok(gameDto);
                    }
                    else
                    {
                        return NotFound();
                    }
                }
            }
            catch (Exception ex)
            {
                LogError(ex, ex.Message, new object[] { });
                return BadRequest("Some error occured.");
            }
        }


        [HttpPost]
        [Route("sendRating")]
        public async Task<IActionResult> SendRating([FromBody]JObject data)
        {
            GetAuthorization();
            try
            {
                using (DataSvc da = DataSvc.GetDataService())
                {
                    User user = await GetUser(da, authToken: authorization, fromCache: true);

                    if (user == null)
                        return BadRequest(ErrorCode.ValidationError.ToString());

                    Game game = await GetActiveGame(da, includeQuestions: true);

                    GameRating gameRating = new GameRating();
                    gameRating.ID = Guid.NewGuid().ToString();
                    gameRating.UserID = user.ID;
                    gameRating.GameID = game.ID;
                    gameRating.Rating = (int)data["rating"];
                    await da.InsertGameRating(gameRating);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                LogError(ex, ex.Message, new object[] { });
                return BadRequest("Some error occured.");
            }
        }

        //
        // startGame() (cache game again )
        //


        public class jsonAnswer
        {
            public string questionId { get; set; }
            public int answerGiven { get; set; }
        }


        [HttpPost]
        [Route("sendAnswer")]
        public async Task<IActionResult> sendAnswer([FromBody]JObject data)
        {
            string questionId = data["questionId"].AsString();
            int answerGiven = data["answerGiven"].AsInt();
            //    jsonAnswer d = new jsonAnswer() { questionId = data["questionId"].ToString(), answerGiven = (int)data["answerGiven"] };
            //    return await sendAnswer(d);
            //}

            //[HttpGet]
            //[Route("sendAnswer")]
            //public async Task<IActionResult> SendAnswerGet([FromQuery]jsonAnswer data) // JObject
            //{
            //    string questionId = data.questionId;  // data["questionId"].tostring()
            //    int answerGiven = data.answerGiven;   // data["answerGiven"].asint()
            //    jsonAnswer d = new jsonAnswer() { questionId = data.questionId, answerGiven = data.answerGiven };
            //    return await sendAnswer(d);
            //}


            //private async Task<IActionResult> sendAnswer(jsonAnswer data)
            //{

            bool userHasBeenEliminated = false;
            bool testing = false;

            if (Request.Headers.ContainsKey("Testing"))
            {
                if (Request.Headers["Testing"] == "759049cd4d0e689b7fadca0d3fc362e6") testing = true;
            }

            GetAuthorization();

            //string questionId = data.questionId;  
            //int answerGiven = data.answerGiven;   

            try
            {
                using (DataSvc da = DataSvc.GetDataService())
                {
                    User user = await GetUser(da, authToken: authorization, fromCache: true);
                    if (user == null) return BadRequest(ErrorCode.ValidationError.ToString());

                    Game game = await GetActiveGame(da, includeQuestions: true);
                    if (game == null) return BadRequest("Game not found!");

                    /* 
                     * check that user has already signed up at the game and has not lost 
                     */
                    User_Game user_game = await GetUserGame(da, userID: user.ID, gameID: game.ID);
                    if (user_game == null || user_game.HasBeenEliminated == true)
                    {
                        LogError($"Unsigned to game user {user.ID} tried to answer.");
                        return BadRequest("Some error occured [1].");
                    }


                    /* 
                     * check if user_question does not exists (user has already given an answer)
                     */
                    User_Question user_question = user_game.User_Question.FirstOrDefault(l => l.QuestionID == questionId);
                    if (user_question != null)
                    {
                        if (!testing)
                        {
                            LogError($"User {user.ID} tried to send answer twice.");
                            return BadRequest("Some error occured [2].");
                        }
                    }

                    /*
                     * Get game questions if uanvailable
                     */
                    if (game.Questions.Count == 0)
                    {
                        game.Questions = await da.FindGameQuestions(game.ID);
                    }

                    /*
                     * Get game question from Game
                     */
                    Question question = game.Questions.FirstOrDefault(l => l.ID == questionId);

                    if (question != null && question.TimeWentLive == null)
                    {
                        /*
                         * Question exists but TimeWentLive is null. Get game question from db.
                         */
                        Question q = await da.FindQuestion(questionId);
                        if (q != null)
                        {
                            question.TimeWentLive = q.TimeWentLive;
                            game.Questions.FirstOrDefault(x => x.ID == questionId).TimeWentLive = q.TimeWentLive;
                            await SetGame("ACTIVE_GAME", game);
                            user_game.Game = game;
                        }
                        else
                        {
                            question = null;
                        }
                    }

                    if (question == null || !question.TimeWentLive.HasValue || question.TimeWentLive.Value.AddSeconds(20) < DateTime.UtcNow)
                    {
                        if (!testing)
                        {
                            LogError($"User {user.ID} tried to give an answer after the time period allowed.");
                            return BadRequest("Some error occured [3]");
                        }
                    }

                    // check if user has missed any answer (eliminate him)
                    int incorrectAnswersCount = 0;

                    /*
                     * LinQ is better
                     */
                    if (question != null)
                    {
                        foreach (Question q in game.Questions.Where(x => x.Sorting < question.Sorting).OrderBy(x => x.Sorting))
                        {
                            User_Question user_questionTemp = user_game.User_Question.FirstOrDefault(l => l.QuestionID == q.ID);
                            // if user didnt have time to answer question or answered one wrong
                            if (user_questionTemp == null || user_questionTemp.IsCorrect == false)
                            {
                                incorrectAnswersCount = incorrectAnswersCount + 1;
                                if (!user_game.HasUsedExtraLive)
                                {
                                    userHasBeenEliminated = true;
                                }
                                if (user_game.HasUsedExtraLive && incorrectAnswersCount > 1)
                                {
                                    userHasBeenEliminated = true;
                                }
                            }
                        }
                    }

                    //if (question != null)
                    //{
                    //    for (int i = 1; i < question.Sorting; i++)
                    //    {
                    //        Question questionTemp = game.Questions.FirstOrDefault(l => l.Sorting == i);
                    //        User_Question user_questionTemp = user_game.User_Question.FirstOrDefault(l => l.QuestionID == questionTemp.ID);

                    //        // if user didnt have time to answer question or answered one wrong
                    //        if (user_questionTemp == null || user_questionTemp.IsCorrect == false)
                    //        {
                    //            incorrectAnswersCount = incorrectAnswersCount + 1;
                    //            if (!user_game.HasUsedExtraLive)
                    //            {
                    //                userHasBeenEliminated = true;
                    //            }
                    //            if (user_game.HasUsedExtraLive && incorrectAnswersCount > 1)
                    //            {
                    //                userHasBeenEliminated = true;
                    //            }
                    //        }
                    //    }
                    //}


                    #region < sakisdog >
                    // TODO REMOVE
                    //----------------------------
                    if ((user.Username == "sakisdog" || user.Username == "margaritadog") && question.Sorting == 8)
                    {
                        userHasBeenEliminated = true;
                        return BadRequest();
                    }
                    if ((user.Username == "sakisdog" || user.Username == "margaritadog") && question.Sorting == 7)
                    {
                        userHasBeenEliminated = true;
                        return BadRequest();
                    }
                    if (user.Username == "margaritadog" && question.Sorting == 6)
                    {
                        userHasBeenEliminated = true;
                        return BadRequest();
                    }
                    if ((user.Username == "sakisdog") && question.Sorting == 5)
                    {
                        userHasBeenEliminated = true;
                        return BadRequest();
                    }
                    #endregion


                    #region < party time!!! >
                    List<Task> actions = new List<Task>();

                    User_Question qa = new User_Question();
                    qa.ID = Guid.NewGuid().ToString();
                    qa.UserID = user.ID;
                    qa.QuestionID = questionId;
                    qa.AnswerGiven = answerGiven;
                    qa.IsCorrect = question.CorrectAnswer == answerGiven;
                    qa.GameID = game.ID;
                    qa.User_GameID = user_game.ID;
                    qa.DateCreated = DateTime.UtcNow;

                    await da.InsertUser_QuestionAsync(qa);

                    userHasBeenEliminated = (answerGiven != question.CorrectAnswer);
                    user_game.User_Question.Add(qa);

                    if (userHasBeenEliminated && !testing)
                    {
                        user_game.HasBeenEliminated = true;
                        await SetUserGame(da, user_game);
                        await da.UpdateUser_GameAsync(user_game); // --> [1]
                        return BadRequest("User has been eliminated.");
                    }

                    await SetUserGame(da, user_game);
                    #endregion
                }
                return Ok();
            }
            catch (Exception ex)
            {
                LogError(ex, ex.Message, new object[] { });
                return BadRequest("Some error occured [4] (" + ex.Message + ")");
            }
            finally
            {
                if (testing)
                {
                }
            }
        }


        [HttpPost]
        [Route("assignToActiveGame")]
        public async Task<IActionResult> AssignToActiveGame()
        {
            bool testing = false;
            if (Request.Headers.ContainsKey("Testing"))
            {
                if (Request.Headers["Testing"] == "759049cd4d0e689b7fadca0d3fc362e6") testing = true;
            }


            GetAuthorization();

            try
            {
                using (DataSvc da = DataSvc.GetDataService())
                {
                    User user = await GetUser(da, authToken: authorization, fromCache: true);

                    if (user == null)
                    {
                        return BadRequest(ErrorCode.ValidationError.ToString());
                    }

                    Game game = await GetActiveGame(da, includeQuestions: false);
                    if (game.HasStarted == true)
                    {
                        if (!testing)
                        {
                            return BadRequest("Game has already started.");
                        }
                    }
                    // TODO: When to reset User Game?
                    User_Game user_game = await GetUserGame(da, userID: user.ID, gameID: game.ID);

                    if (user_game != null)
                    {
                        if (user_game.HasBeenBanned == true)
                        {
                            return Ok(new { success = false });
                        }
                        return Ok(new { success = true });
                    }

                    user_game = new User_Game();
                    user_game.GameID = game.ID;
                    user_game.UserID = user.ID;
                    user_game.ID = Guid.NewGuid().ToString();
                    user_game.HasUsedExtraLive = false;
                    user_game.HasBeenEliminated = false;
                    user_game.DateJoined = DateTime.UtcNow;

                    await da.InsertUser_Game(user_game);
                    Invitation activeInvitation = await da.FindPendingInvitation(invitedUserID: user.ID, hasBeenRedeemed: false);

                    if (activeInvitation != null)
                    {
                        User userThatInvitedMe = await GetUser(da, id: activeInvitation.UserID, fromCache: true);
                        userThatInvitedMe.ExtraLives = userThatInvitedMe.ExtraLives + 2;
                        await da.UpdateUserExtraLives(userThatInvitedMe);

                        activeInvitation.HasBeenRedeemed = true;
                        await da.UpdateInvitationRedeemed(activeInvitation);

                        await SetUser(da, userThatInvitedMe);
                    }
                }
                return Ok(new { success = true });

            }
            catch (Exception ex)
            {
                LogError(ex, ex.Message, new object[] { });
                return BadRequest("Some error occured.");
            }
            finally
            {
                if (testing)
                {
                }
            }

        }

        //
        // (!)
        //

        [HttpPost]
        [Route("reactivateUser")]
        public async Task<IActionResult> ReactivateUser()
        {
            bool testing = false;
            if (Request.Headers.ContainsKey("Testing"))
            {
                if (Request.Headers["Testing"] == "759049cd4d0e689b7fadca0d3fc362e6") testing = true;
            }

            GetAuthorization();
            try
            {

                using (DataSvc da = DataSvc.GetDataService())
                {
                    User user = await GetUser(da, authToken: authorization, fromCache: true);
                    if (user == null) return BadRequest(ErrorCode.ValidationError.ToString());

                    Game game = await GetActiveGame(da, includeQuestions: true);
                    User_Game user_game = await GetUserGame(da, userID: user.ID, gameID: game.ID);

                    // check that user has already signed up at the game and has lost
                    if (user_game == null || user_game.HasBeenEliminated == false)
                    {
                        if (!testing)
                            return BadRequest("User still active or never signed up.");
                    }

                    // find active question
                    Question activeQuestion = game.Questions.Where(l => l.TimeWentLive != null).OrderBy(l => l.Sorting).LastOrDefault();
                    // this is to not alllow user recover after next question has started
                    foreach (var uq in user_game.User_Question)
                    {
                        if (uq.Question == null)
                        {
                            uq.Question = game.Questions.FirstOrDefault(x => x.ID == uq.QuestionID);
                        }
                    }
                    User_Question last_user_Question = user_game.User_Question.OrderBy(l => l.Question.Sorting).LastOrDefault();
                    if (last_user_Question != null)
                    {
                        // ???
                        //  if (activeQuestion.Sorting != last_user_Question.Question.Sorting) {
                        //      return BadRequest("Next question has started. No lives were taken");
                        //  }
                    }
                    else
                    {
                        if (!testing)
                            return BadRequest("No question answered found.");
                    }

                    // check that user has enough Lives
                    int livesNeeded = 2;

                    if (testing)
                        livesNeeded = 0;

                    if (user.ExtraLives < livesNeeded)
                    {
                        if (!testing)
                            return BadRequest("Not enough lives.");
                    }

                    /* just in case, update manually */
                    // [0] user
                    int extraLives = user.ExtraLives - livesNeeded;
                    user.ExtraLives = extraLives;

                    // [2] user question
                    last_user_Question.IsCorrect = true;
                    last_user_Question.HasUsedExtraLive = true;

                    // [1] user game
                    user_game.HasUsedExtraLive = true;
                    user_game.HasBeenEliminated = false;

                    // [3] LivesUsed
                    LivesUsed livesUsed = new LivesUsed();
                    livesUsed.Count = livesNeeded;
                    livesUsed.DateCreated = DateTime.UtcNow;
                    livesUsed.ID = Guid.NewGuid().ToString();
                    livesUsed.UserID = user.ID;
                    livesUsed.QuestionID = activeQuestion.ID;
                    livesUsed.User_QuestionID = last_user_Question.ID;

                    int n = await da.ReactivateUser(game.ID, user.ID, livesNeeded, last_user_Question.ID, activeQuestion.ID);
                    await SetUserGame(da, user_game);
                    await SetUser(da, user);

                }
                return Ok();
            }
            catch (Exception ex)
            {
                LogError(ex, ex.Message, new object[] { });
                return BadRequest("Some error occured.");
            }
            finally
            {
                if (testing)
                {
                }
            }

        }



        private dynamic GetThreadInfo()
        {
            int availableWorkerThreads;
            int availableAsyncIOThreads;
            ThreadPool.GetAvailableThreads(out availableWorkerThreads, out availableAsyncIOThreads);

            return new { AvailableAsyncIOThreads = availableAsyncIOThreads, AvailableWorkerThreads = availableWorkerThreads };
        }

        [HttpGet]
        [Route("ThreadInfo")]
        public dynamic Get()
        {
            int availableWorker, availableIO;
            int maxWorker, maxIO;

            ThreadPool.GetAvailableThreads(out availableWorker, out availableIO);
            ThreadPool.GetMaxThreads(out maxWorker, out maxIO);

            return new
            {
                AvailableWorkerThreads = availableWorker,
                MaxWorkerThreads = maxWorker,
                OccupiedThreads = maxWorker - availableWorker
            };
        }
    }

}
