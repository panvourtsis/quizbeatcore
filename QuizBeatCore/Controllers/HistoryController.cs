﻿using Core.AspNet.Services;
using Core.Common;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using QuizBeat.Models.Database;
using QuizBeat.Models.Dto;
using QuizBeatCore.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace QuizBeat.Controllers
{
    [Produces("application/json")]
    [Route("api/history")]
    [Controller]
    public class HistoryController : BaseController
    {


        public HistoryController(IContextService ctx, IHostingEnvironment env, IConfiguration c, IQbDataService da, IDbCacheService dbc) : base(ctx, env, c, da, dbc)
        {

        }

        [HttpGet]
        [Route("alltime")]
        public async Task<IActionResult> GetTopAllTime()
        {
            GetAuthorization();

            try
            {
                using (DataSvc da = DataSvc.GetDataService())
                {
                    User user = await GetUser(da, authToken: authorization);
                    if (user == null)
                    {
                        return BadRequest(ErrorCode.ValidationError.ToString());
                    }
                    IEnumerable<User> users = await da.Top100AllTimeUsers();
                    List<UserShortDtoTop100> usersDtos = UserShortDtoTop100.ConvertToDto(users);
                    if (usersDtos != null)
                        return Ok(usersDtos);
                    else
                        return NotFound();
                }
            }
            catch (Exception ex)
            {
                LogError(ex, ex.Message, new object[] { });
                //Core.Common.Diagnostics.Trace.Log(ex);
                return NotFound();
            }
        }


        [HttpGet]
        [Route("myalltime")]
        public async Task<IActionResult> GetMyTopAllTime()
        {
            GetAuthorization();

            try
            {
                using (DataSvc da = DataSvc.GetDataService())
                {
                    User user = await GetUser(da, authToken: authorization);
                    if (user == null) return BadRequest(ErrorCode.ValidationError.ToString());

                    UserShortDto userDto = new UserShortDto(user);
                    if (userDto != null)
                        return Ok(userDto);
                    else
                        return NotFound();
                }
            }
            catch (Exception ex)
            {
                LogError(ex, ex.Message, new object[] { });
                return NotFound();
            }
        }



        [HttpGet]
        [Route("myinvitationranking")]
        public async Task<IActionResult> GetMyInvitationRanking()
        {
            GetAuthorization();

            try
            {
                using (DataSvc da = DataSvc.GetDataService())
                {
                    User user = await GetUser(da, authToken: authorization);
                    if (user == null) return BadRequest(ErrorCode.ValidationError.ToString());

                    //@@@ why all???
                    IEnumerable<InvitationRankingDto> invitationRankings = await da.AllInvitationRankingsDtos(user.ID);

                    InvitationRankingDto rankingDto = invitationRankings.FirstOrDefault(l => l.UserID == user.ID);
                    rankingDto = new InvitationRankingDto();
                    rankingDto.Ranking = user.Ranking.Value;
                    string monthName = DateTime.UtcNow.ToString("MMMM", CultureInfo.CreateSpecificCulture("el"));
                    rankingDto.Month = monthName;

                    return Ok(rankingDto);
                }

            }
            catch (Exception ex)
            {
                LogError(ex, ex.Message, new object[] { });
                //Core.Common.Diagnostics.Trace.Log(ex);
                return NotFound();
            }
        }
    }
}