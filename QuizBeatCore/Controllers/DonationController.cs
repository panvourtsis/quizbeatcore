﻿using Core.AspNet.Services;
using Core.Common;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using QuizBeat.Models.Database;
using QuizBeat.Models.Dto;
using QuizBeatCore.Services;
using System;
using System.Threading.Tasks;

namespace QuizBeat.Controllers
{
    [Route("api/donation")]
    public class DonationController : BaseController
    {

        public DonationController(IContextService ctx, IHostingEnvironment env, IConfiguration c, IQbDataService da, IDbCacheService dbc) : base(ctx, env, c, da, dbc)
        {
        }

        [HttpGet]
        [Route("active")]
        public async Task<IActionResult> GetActiveDonation()
        {
            //GetAuthorization();
            try
            {
                using (DataSvc da = DataSvc.GetDataService())
                {
                    Donation donation = await GetDonation(da);
                    if (donation != null)
                    {
                        DonationDto donationDto = new DonationDto(donation);
                        return Ok(donationDto);
                    }
                    else
                    {
                        return BadRequest("Some error occured.");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError(ex, ex.Message, new object[] { });
                return BadRequest("Some error occured.");
            }
        }



        [HttpPost]
        [Route("donate")]
        public async Task<IActionResult> DonatePrize([FromBody]JObject data)
        {
            GetAuthorization();

            try
            {
                using (DataSvc da = DataSvc.GetDataService())
                {
                    User user = await GetUser(da, authToken: authorization, fromCache: false);
                    if (user == null)
                    {
                        return BadRequest(ErrorCode.ValidationError.ToString());
                    }

                    double moneyDonated = (double)data["money"];

                    if (moneyDonated - 0.01 <= (user.Money_Won - user.Money_CashedOut))
                    {

                        Donation donation = await GetDonation(da);

                        donation.MoneyReached = donation.MoneyReached + (float)moneyDonated;
                        var t1 = da.UpdateDonation(donation);

                        CashOut cashout = new CashOut();
                        cashout.UserID = user.ID;
                        cashout.Money = moneyDonated;
                        cashout.Date = DateTime.UtcNow;
                        cashout.IsApproved = true;
                        cashout.Type = "Donation";
                        cashout.ID = Guid.NewGuid().ToString();
                        var t2 = da.InsertCashOut(cashout);

                        // TODO: UpdateUserCachedOut()
                        user.Money_CashedOut = user.Money_CashedOut + moneyDonated;
                        var t3 = da.UpdateUserCashedOut(user);

                        var t4 = SetUser(da, user); ;

                        await Task.WhenAll(t1, t2, t3, t4);


                        return Ok();
                    }
                    else
                    {
                        return BadRequest("Some error occured.");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError(ex, ex.Message, new object[] { });
                return BadRequest("Some error occured.");
            }
        }
    }
}


