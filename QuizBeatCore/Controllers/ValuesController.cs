﻿using Core.AspNet.Services;
using Core.Common;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using QuizBeat.Controllers;
using QuizBeat.Models.Database;
using QuizBeatCore.Services;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace QuizBeatCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : BaseController
    {

        public ValuesController(IContextService ctx, IHostingEnvironment env, IConfiguration c, IQbDataService da, IDbCacheService dbc)
            : base(ctx, env, c, da, dbc)
        {

        }

        // GET api/values
        [HttpGet("")]
        public async Task<ActionResult<IEnumerable<string>>> Index()
        {
            var ls = new List<string>();

            try
            {
                int availableWorkerThreads;
                int availableAsyncIOThreads;
                ThreadPool.GetAvailableThreads(out availableWorkerThreads, out availableAsyncIOThreads);

                ls.Add($"processorCounter = {System.Environment.ProcessorCount}");
                ls.Add($"AvailableWorkerThreads = {availableWorkerThreads}");
                ls.Add($"availableAsyncIOThreads = {availableAsyncIOThreads}");

                ls.Add($"<strong>Cache:</strong><hr>");


 

                //foreach (var k in cacheSvc)
                //{
                //    ls.Add(k.Key.ToString() + " - " + k.Value.ToString() + "<br>");
                //}
                await Task.Yield();

                int maxWorker, maxIO, availableWorker, availableIO;
                ThreadPool.GetAvailableThreads(out availableWorker, out availableIO);
                ThreadPool.GetMaxThreads(out maxWorker, out maxIO);

                LogInfo($"maxWorker:{maxWorker} - maxIO:{maxIO} - availableWorker:{availableWorker} - availableIO:{availableIO}");
            }
            catch (Exception ex)
            {
                ls.Add(ex.Message);
            }
            return View(ls);  // new string[] { "value1", "value2" };
        }

        // GET api/values
        [HttpGet("Test")]
        public async Task<ActionResult<IEnumerable<string>>> Test()
        {
            var ls = new List<string>();

            try
            {
                int availableWorkerThreads;
                int availableAsyncIOThreads;
                ThreadPool.GetAvailableThreads(out availableWorkerThreads, out availableAsyncIOThreads);

                ls.Add($"processorCounter = {System.Environment.ProcessorCount}");
                ls.Add($"AvailableWorkerThreads = {availableWorkerThreads}");
                ls.Add($"availableAsyncIOThreads = {availableAsyncIOThreads}");

                ls.Add($"<strong>Cache:</strong><hr>");


                //await cacheSvc.Set<object>("sss", "sss");
                //await cacheSvc.Get<object>("sss");

                //await cacheSvc.Set<object>("sss", 567899);
                //await cacheSvc.Get<object>("sss");

                //await cacheSvc.Get<string>("xxx");
                //await cacheSvc.Get<string>("vvv");


                //var result = this.cacheSvc.Select(t => new
                //{
                //    Key = t.Key,
                //    Value = t.Value
                //}).ToArray();

                //foreach (var k in cacheSvc)
                //{
                //    ls.Add(k.Key.ToString() + " - " + k.Value.ToString() + "<br>");
                //}
                await Task.Yield();

                //using(DataSvc da = DataSvc.GetDataService())
                //{
                //    List<Cache> lsC = await da.ExecuteListAsync<Cache>("select * from cache order by datecreated", da.Args());
                //    foreach(var c in lsC)
                //    {
                //        ls.Add($"{c.Key}-{c.Value}");
                //    }
                //}

                //Game g = await GetActiveGame();
                //await dbCacheSvc.Set<Game>("GAME", g);
                //Game g2 = await dbCacheSvc.Get<Game>("GAME");

                int maxWorker, maxIO, availableWorker, availableIO;
                ThreadPool.GetAvailableThreads(out availableWorker, out availableIO);
                ThreadPool.GetMaxThreads(out maxWorker, out maxIO);
                LogInfo($"conn limit:{System.Net.ServicePointManager.DefaultConnectionLimit} - maxWorker:{maxWorker} - maxIO:{maxIO} - availableWorker:{availableWorker} - availableIO:{availableIO}");
                return Ok(ls);  // new string[] { "value1", "value2" };
            }
            catch (Exception ex)
            {
                ls.Add(ex.Message);
                return BadRequest(ls);
            }
        }



        //// GET api/values/5
        //[HttpGet("{id}")]
        //public ActionResult<string> Get(int id)
        //{
        //    return "value";
        //}

        //// POST api/values
        //[HttpPost]
        //public void Post([FromBody] string value)
        //{
        //}

        //// PUT api/values/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        //// DELETE api/values/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
