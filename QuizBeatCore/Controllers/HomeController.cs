﻿using Core.AspNet.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using QuizBeat.Models.Database;
using QuizBeatCore.Services;

namespace QuizBeat.Controllers
{
    [Route("home")]
    public class HomeController : BaseController
    {

        public HomeController(IContextService ctx, IHostingEnvironment env, IConfiguration c, IQbDataService da, IDbCacheService dbc) : base(ctx, env,  c, da, dbc)
        {

        }

        public ActionResult Index()
        {

            ViewBag.Title = "Home Page";
            return View();
        }

        [Route("/loaderio-b77d527e43c696e4c354db43accbb8b2")]
        public ActionResult loaderio()
        {

            return Content("loaderio-b77d527e43c696e4c354db43accbb8b2");
        }

        [Route("/loaderio-759049cd4d0e689b7fadca0d3fc362e6")]
        public ActionResult loaderioLive()
        {

            return Content("loaderio-759049cd4d0e689b7fadca0d3fc362e6");
        }

        
        [Route("/loaderio-c19041c1cdd16c90fa8b0def196bb80c")]
        public ActionResult loaderiocONVEYOR()
        {

            return Content("loaderio-c19041c1cdd16c90fa8b0def196bb80c");
        }

        [Route("faq")]
        public ActionResult Faq()
        {
            ViewBag.Title = "FAQ";
            return View();
        }

        [Route("rules")]
        public ActionResult Rules()
        {
            ViewBag.Title = "Rules";

            return View();
        }

        [Route("terms")]
        public ActionResult Terms()
        {
            ViewBag.Title = "Terms";

            return View();
        }

        [Route("privacy")]
        public ActionResult Privacy()
        {
            ViewBag.Title = "Privacy";

            return View();
        }

        [Route("resetPassword")]
        public ActionResult ResetPassword()
        {
            ViewBag.Title = "Reset Password";

            return View();
        }
    }
}
