﻿using Microsoft.AspNet.SignalR;
using QuizBeat.Classes;
using QuizBeat.Models.Database;
using QuizBeat.Models.Dto;
using QuizBeatAPI.Hubs;
using QuizBeatAPI.Repositories;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;

namespace QuizBeat.Controllers
{
    [FromUri, RoutePrefix("api/camera")]
    public class CameraController : ApiController
    {
        private readonly IDataBaseRepository _databaseRepository;

        private readonly IHubContext hubContext;

        public CameraController()
        {
            _databaseRepository = new DataBaseRepository();
            hubContext = GlobalHost.ConnectionManager.GetHubContext<QuestionHub>();
        }

        [HttpGet]
        [Route("nextQuestion")]
        [AdminAuthentication]
        public IHttpActionResult ShowNextQuestion(int sorting)
        {
            try
            {
                Game game = _databaseRepository.FindActiveGame(includeQuestions: true);
                if (game != null)
                {
                    Question question = game.Questions.FirstOrDefault(l => l.Sorting == sorting);
                    hubContext.Clients.Group("QuestionHub").showNextQuestion(new { question = new QuestionDto(question) });
                    Trace.TraceError("Question " + sorting.ToString() + " shown");
                    return Ok();
                }
                else
                {
                    return Content(HttpStatusCode.BadRequest, "Some error occured.");
                }
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, "Some error occured.");
            }
        }

        [HttpGet]
        [Route("endOfQuestion")]
        [AdminAuthentication]
        public IHttpActionResult EndOfQuestion(int sorting)
        {
            //this is to eliminate all players that didnt answer the question
            try
            {
                Game game = _databaseRepository.FindActiveGame(includeQuestions: true);
                if (game != null)
                {
                    Question question = game.Questions.FirstOrDefault(l => l.Sorting == sorting);
                    List<User_Game> user_games = _databaseRepository.AllUser_Games(gameID: game.ID).ToList();
                    List<User_Question> user_questions = _databaseRepository.FindAnswers(question.ID).ToList();

                    foreach (User_Game user_game in user_games)
                    {
                        User_Question question_temp = user_questions.FirstOrDefault(l => l.UserID == user_game.User.ID);
                        if (question_temp == null)
                        {
                            user_game.HasBeenEliminated = true;
                            _databaseRepository.UpdateUser_Game(user_game);
                        }
                    }
                    _databaseRepository.SaveChanges();

                    Trace.TraceError("Question " + sorting.ToString() + " closed");
                    return Ok();
                }
                return Content(HttpStatusCode.BadRequest, "Some error occured.");
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, "Some error occured.");
            }
        }

        [HttpGet]
        [Route("showAnswer")]
        [AdminAuthentication]
        public IHttpActionResult ShowAnswer(int sorting)
        {
            try
            {
                Game game = _databaseRepository.FindActiveGame(includeQuestions: true);
                if (game != null)
                {
                    Question question = game.Questions.FirstOrDefault(l => l.Sorting == sorting);
                    List<User_Question> questionAnswers = _databaseRepository.FindAnswers(questionID: question.ID).ToList();
                    int answer1Count = questionAnswers.Where(l => l.AnswerGiven == 1).Count();
                    int answer2Count = questionAnswers.Where(l => l.AnswerGiven == 2).Count();
                    int answer3Count = questionAnswers.Where(l => l.AnswerGiven == 3).Count();

                    hubContext.Clients.Group("QuestionHub").showAnswer(new { sorting = sorting, correctAnswer = question.CorrectAnswer, answer1Count = answer1Count, answer2Count = answer2Count, answer3Count = answer3Count });
                    Trace.TraceError("Question " + sorting.ToString() + " answer shown");

                    return Ok();
                }
                else
                {
                    return Content(HttpStatusCode.BadRequest, "Some error occured.");
                }
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, "Some error occured.");
            }
        }

        [HttpGet]
        [Route("startGame")]
        public IHttpActionResult StartGame()
        {


            //List<User_Question> userQuestions = _databaseRepository.AllUser_Questions.Where(l => l.QuestionID == "43de8fff-c7c5-4be0-a1e0-273144d8b6fd" && l.IsCorrect == true).ToList<User_Question>();

            //foreach (User_Question userQ in userQuestions) {
            //    User user = _databaseRepository.FindUser(id: userQ.UserID);
            //    user.ExtraLives = user.ExtraLives + 1;
            //}

            //_databaseRepository.SaveChanges();


            return Ok();
        }


        [HttpGet]
        [Route("sendPush")]
        [AdminAuthentication]
        public IHttpActionResult SendPush(string notificationMessage, string notificationFilename)
        {
            List<User> allUsersWithPush = _databaseRepository.AllUsers(hasPushNotification: true).ToList<User>();
            PushHelper.sendPushNotificationInBulk(allUsersWithPush.Where(l => l.DeviceType == 1 || l.DeviceType == 0 || l.DeviceType == null).Select(l => l.PushToken).ToList<string>(), notificationMessage, notificationFilename, deviceType: 1);
            PushHelper.sendPushNotificationInBulk(allUsersWithPush.Where(l => l.DeviceType == 2).Select(l => l.PushToken).ToList<string>(), notificationMessage, notificationFilename, deviceType: 2);
            return Ok();
        }


        [HttpGet]
        [Route("eliminateUser")]
        [AdminAuthentication]
        public IHttpActionResult EliminateUser(string username)
        {
            hubContext.Clients.Group("QuestionHub").eleminateUser(username);
            return Ok();
        }

        [HttpGet]
        [Route("endGame")]
        [AdminAuthentication]
        public IHttpActionResult EndGame()
        {
            hubContext.Clients.Group("QuestionHub").endGame();
            Trace.TraceError("Game Ended");
            return Ok();
        }

        [HttpGet]
        [Route("showWinners")]
        [AdminAuthentication]
        public IHttpActionResult ShowWinners()
        {
            hubContext.Clients.Group("QuestionHub").showWinners();
            Trace.TraceError("Show Winners Pressed");
            return Ok();
        }

        [HttpGet]
        [Route("hideWinners")]
        [AdminAuthentication]
        public IHttpActionResult HideWinners()
        {
            hubContext.Clients.Group("QuestionHub").hideWinners();
            return Ok();
        }
    }
}
