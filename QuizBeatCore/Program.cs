﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyModel;
using Serilog;
using System;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;
using System.Threading;

namespace QuizBeatCore
{
    public class Program
    {
        public static void Main(string[] args)
        {
            /*
             * DON'T TOUCH!!!
            */
            int processorCounter = System.Environment.ProcessorCount;
            ThreadPool.SetMaxThreads(processorCounter * 20, processorCounter * 20);
            ThreadPool.SetMinThreads(processorCounter * 15, processorCounter * 15);

            System.Net.ServicePointManager.DefaultConnectionLimit = 1000; 
            System.Net.ServicePointManager.UseNagleAlgorithm = false;
            System.Net.ServicePointManager.ReusePort = true;

            Log.Logger = new LoggerConfiguration()
               .WriteTo.File("..\\logs\\QBLog.txt", rollingInterval: RollingInterval.Day, shared: true)
               .CreateLogger();

            System.Threading.ThreadPool.GetMaxThreads(out int workerThreads, out int completionThreads);
            //Log.Information($"Starting Server :: workerThreads {workerThreads} - completionThreads {completionThreads}");

            var host = CreateWebHostBuilder(args).Build();
            host.Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
            .UseKestrel(options => { options.Limits.MaxConcurrentConnections = null; options.Limits.MaxConcurrentUpgradedConnections = null; })
            .UseStartup<Startup>();
    }

}
