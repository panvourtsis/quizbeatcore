﻿using Microsoft.Extensions.Caching.Memory;

namespace QuizBeatBusiness.Services
{

    public interface ICachedSvc
    {
        void ClearCache();
        void ClearCache(string key);
    }
    public class CachedSvc : ICachedSvc
    {
        public IMemoryCache MemCache;
        public virtual string CacheKey { get; set; }

        public CachedSvc(IMemoryCache cache)
        {
            MemCache = cache;
        }

        public virtual void ClearCache()
        {
            MemCache.Remove(CacheKey);
        }

        public virtual void ClearCache(string key)
        {
            MemCache.Remove(key);
        }
    }

       
}
