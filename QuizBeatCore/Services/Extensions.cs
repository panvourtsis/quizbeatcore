﻿using Core.Common;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Core.AspNet
{
    #region < Microsoft.AspNetCore.Http.HttpContext >

    public static class HttpContextExtensions
    {

        public static string SetQueryFromRequest(this Microsoft.AspNetCore.Http.HttpContext ctx, string requestTemplate)
        {
            if (ctx != null && ctx.Request != null && ctx.Request.Query != null && !requestTemplate.IsNullOrEmpty())
            {
                IQueryCollection qc = ctx.Request.Query;
                if (qc != null)
                {
                    string rsp = string.Empty;
                    foreach (var x in requestTemplate.Split(','))
                    {
                        rsp += x + "=" + qc[x] + "&";
                    }
                    rsp = rsp.TrimEnd('&');
                    return rsp;
                }
            }
            return string.Empty;
        }

        public static Dictionary<string, object> GetRequestData(this Microsoft.AspNetCore.Http.HttpContext ctx)
        {
            Dictionary<string, object> val = new Dictionary<string, object>();
            if (ctx != null && ctx.Request != null && ctx.Request.Query != null)
            {
                IQueryCollection qc = ctx.Request.Query;
                IFormCollection data = null;

                if (ctx.Request.Method.ToLower() == "post" && ctx.Request.Form != null) data = ctx.Request.Form;
                if (data != null)
                {
                    foreach (var x in data)
                    {
                        Microsoft.Extensions.Primitives.StringValues tempOrder = new[] { "" };
                        if (data.TryGetValue(x.Key, out tempOrder))
                        {
                            val.Add(x.Key, x.Value);
                        }
                    }
                }
                if (qc != null)
                {
                    foreach (var x in qc)
                    {
                        val.Add(x.Key, x.Value);
                    }
                }
            }
            return val;
        }
    }
    #endregion


    public static class SessionExtensions
    {
        public static void SetObjectAsJson(this ISession session, string key, object value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value));
        }

        public static T GetObjectFromJson<T>(this ISession session, string key)
        {
            var value = session.GetString(key);
            return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
        }

        public static string ToJson<T>(this T obj)
        {
            return JsonConvert.SerializeObject(obj, QuizBeatBusiness.Common.JsonSerializerSettings());
        }

        public static string ToCamelCase(this string s)
        {
            if (s != string.Empty && char.IsUpper(s[0]))
            {
                s = char.ToLower(s[0]) + s.Substring(1);
            }

            return s;
        }


    }

}
