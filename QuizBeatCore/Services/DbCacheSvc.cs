﻿using Core.Common;
using Core.Common.SqlOM;
using Microsoft.Extensions.Caching.Memory;
using Reeb.SqlOM;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.AspNet.Services
{
    public interface IDbCacheService : IEnumerable<KeyValuePair<object, object>>, IMemoryCache
    {
        enCacheMode Mode { get; set; }

        Task Clear(string key = null);
        Task<object> Set<T>(string key, T value, DateTime offset);
        Task<object> Set<T>(string key, T value, TimeSpan? offset);
        Task<object> Set<T>(string key, T value);
        Task<T> Get<T>(string key) where T : class;

        Task Clear(DataSvc da, string key = null);
        Task<object> Set<T>(DataSvc da, string key, T value, DateTime offset);
        Task<object> Set<T>(DataSvc da, string key, T value, TimeSpan? offset);
        Task<object> Set<T>(DataSvc da, string key, T value);
        Task<T> Get<T>(DataSvc da, string key) where T : class;
    }

    public enum enCacheMode
    {
        Both, Memory, Distributed
    }


    public class DbCacheService : IDbCacheService
    {

        public enCacheMode Mode { get; set; } = enCacheMode.Memory;

        public DbCacheService(IMemoryCache cache)
        {
            _memoryCache = cache;
        }

        #region < cache methods >
        public async Task Clear(DataSvc da, string key = null)
        {
            if (Mode == enCacheMode.Both || Mode == enCacheMode.Memory)
            {
                this.MemClear();
            }
            if (Mode == enCacheMode.Both || Mode == enCacheMode.Distributed)
            {
                await da.ClearCacheAsync(key);
            }
        }

        public async Task<object> Set<T>(DataSvc da, string key, T value)
        {
            if (Mode == enCacheMode.Both || Mode == enCacheMode.Memory)
            {
                MemSet<T>(key, value);
            }
            if (Mode == enCacheMode.Both || Mode == enCacheMode.Distributed)
            {
                await da.SetCacheAsync<T>(key, value, new TimeSpan(24, 0, 0));
            }
            return value;
        }

        public async Task<object> Set<T>(DataSvc da, string key, T value, TimeSpan? offset)
        {
            if (Mode == enCacheMode.Both || Mode == enCacheMode.Memory)
            {
                MemSet<T>(key, value, offset ?? new TimeSpan(24, 0, 0));
            }
            if (Mode == enCacheMode.Both || Mode == enCacheMode.Distributed)
            {
                await da.SetCacheAsync<T>(key, value, offset);
            }
            return value;
        }
        public async Task<object> Set<T>(DataSvc da, string key, T value, DateTime offset)
        {
            if (Mode == enCacheMode.Both || Mode == enCacheMode.Memory)
            {
                MemSet(key, value, offset - DateTime.Now);
            }
            if (Mode == enCacheMode.Both || Mode == enCacheMode.Distributed)
            {
                await da.SetCacheAsync(key, value, offset - DateTime.Now);
            }
            return value;
        }
        public async Task<object> Set<T>(DataSvc da, string key, T value, TimeSpan timeSpan)
        {
            if (Mode == enCacheMode.Both || Mode == enCacheMode.Memory)
            {
                MemSet(key, value, timeSpan);
            }
            if (Mode == enCacheMode.Both || Mode == enCacheMode.Distributed)
            {
                await da.SetCacheAsync(key, value, timeSpan);
            }
            return value;
        }

        public async Task<T> Get<T>(DataSvc da, string key) where T : class
        {
            T t = null;
            if (Mode == enCacheMode.Both || Mode == enCacheMode.Memory)
            {
                t = this._memoryCache.Get<T>(key);
            }
            if (Mode == enCacheMode.Both || Mode == enCacheMode.Distributed)
            {
                if (t == null)
                {
                    t = await da.GetCacheAsync<T>(key);
                    if (t != null && (Mode == enCacheMode.Both || Mode == enCacheMode.Memory))
                    {
                        MemSet<T>(key, t);
                    }
                }
            }
            return t;
        }
        #endregion


        #region < interface >

        bool IMemoryCache.TryGetValue(object key, out object value)
        {
            if (Mode == enCacheMode.Both || Mode == enCacheMode.Memory)
            {
                return this._memoryCache.TryGetValue(key, out value);
            }
            else
            {
                value = null; return false;
            }

        }

        public ICacheEntry CreateEntry(object key)
        {
            ICacheEntry entry = null;
            entry = MemCreateEntry(key);
            if (Mode == enCacheMode.Both || Mode == enCacheMode.Distributed)
            {
                using (DataSvc da = DataSvc.GetDataService())
                {
                    Task.WhenAll(da.SetCacheAsync(key, string.Empty, new TimeSpan(0, 0, 0)));
                }
                entry = MemCreateEntry(key);
            }

            return entry;
        }

        public void Remove(object key)
        {
            MemRemove(key);
            if (Mode == enCacheMode.Both || Mode == enCacheMode.Distributed)
            {
                using (DataSvc da = DataSvc.GetDataService())
                {
                    Task.WhenAll(da.ClearCacheAsync(key));
                }
            }

        }
        #endregion


        #region < standalone >
        public async Task Clear(string key = null)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                await Clear(da, key);
            }
        }

        public async Task<object> Set<T>(string key, T value)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await Set<T>(da, key, value, new TimeSpan(24, 0, 0)); ;
            }
        }

        public async Task<object> Set<T>(string key, T value, TimeSpan? offset)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await Set<T>(da, key, value, offset); ;
            }
        }
        public async Task<object> Set<T>(string key, T value, DateTime offset)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await Set<T>(da, key, value, offset - DateTime.Now);
            }
        }
        public async Task<T> Get<T>(string key) where T : class
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await Get<T>(da, key);
            }
        }
        #endregion


        #region < MemCache interface >

        private readonly IMemoryCache _memoryCache;
        public virtual string CacheKey { get; set; }
        private readonly ConcurrentDictionary<object, ICacheEntry> _cacheEntries = new ConcurrentDictionary<object, ICacheEntry>();
        private object lockObj = new object();

        internal virtual void MemClearCache()
        {
            _memoryCache.Remove(CacheKey);
            if (Mode == enCacheMode.Both || Mode == enCacheMode.Distributed)
            {
                using (DataSvc da = DataSvc.GetDataService())
                {
                    poDeleteQuery q = new poDeleteQuery("cache");
                    q.WhereEquals("key", CacheKey);
                    da.ExecuteNonQuery(q);
                }
            }
        }

        internal virtual void MemClearCache(string key)
        {
            _memoryCache.Remove(key);
            if (Mode == enCacheMode.Both || Mode == enCacheMode.Distributed)
            {
                using (DataSvc da = DataSvc.GetDataService())
                {
                    poDeleteQuery q = new poDeleteQuery("cache");
                    q.WhereEquals("key", key);
                    da.ExecuteNonQuery(q);
                }
            }
        }

        //private void PostEvictionCallback(object key, object value, EvictionReason reason, object state)
        //{
        //    if (reason != EvictionReason.Replaced)
        //    {
        //        this._cacheEntries.TryRemove(key, out var _);
        //        if (Mode == enCacheMode.Both || Mode == enCacheMode.Distributed)
        //        {
        //            //using (DataSvc da = DataSvc.GetDataService())
        //            //{
        //            //    poDeleteQuery q = new poDeleteQuery("cache");
        //            //    q.WhereEquals("key", key);
        //            //    da.ExecuteNonQuery(q);
        //            //}
        //        }
        //    }
        //}

        /// <inheritdoc cref="IMemoryCache.TryGetValue"/>
        internal bool MemTryGetValue(object key, out object value)
        {
            return this._memoryCache.TryGetValue(key, out value);
        }

        /// <summary>
        /// Create or overwrite an entry in the cache and add key to Dictionary.
        /// </summary>
        /// <param name="key">An object identifying the entry.</param>
        /// <returns>The newly created <see cref="T:Microsoft.Extensions.Caching.Memory.ICacheEntry" /> instance.</returns>
        internal ICacheEntry MemCreateEntry(object key)
        {
            var entry = this._memoryCache.CreateEntry(key);
            //entry.RegisterPostEvictionCallback(PostEvictionCallback);
            this._cacheEntries.AddOrUpdate(key, entry, (o, cacheEntry) =>
            {
                cacheEntry.Value = entry;
                return cacheEntry;
            });
            return entry;
        }

        internal void MemRemove(object key)
        {
            this._memoryCache.Remove(key);
        }

        internal void MemClear()
        {
            lock (lockObj)
            {
                if (this._cacheEntries.Keys.Count > 0)
                {
                    foreach (var cacheEntry in this._cacheEntries.Keys.ToList())
                        this._memoryCache.Remove(cacheEntry);
                }
            }
        }

        internal void MemClear(string key)
        {
            _memoryCache.Remove(key);
        }


        public IEnumerator<KeyValuePair<object, object>> GetEnumerator()
        {
            return this._cacheEntries.Select(pair => new KeyValuePair<object, object>(pair.Key, pair.Value.Value)).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Gets keys of all items in MemoryCache.
        /// </summary>
        public IEnumerator<object> Keys => this._cacheEntries.Keys.GetEnumerator();

        public void Dispose()
        {
            this._memoryCache.Dispose();
        }

        #region < memcache extensions >


        internal T MemSet<T>(object key, T value)
        {
            var entry = this.MemCreateEntry(key);
            entry.Value = value;
            entry.Dispose();

            return value;
        }

        internal T MemSet<T>(object key, T value, CacheItemPriority priority)
        {
            var entry = this.MemCreateEntry(key);
            entry.Priority = priority;
            entry.Value = value;
            entry.Dispose();

            return value;
        }

        internal T MemSet<T>(object key, T value, DateTimeOffset absoluteExpiration)
        {
            var entry = this.MemCreateEntry(key);
            entry.AbsoluteExpiration = absoluteExpiration;
            entry.Value = value;
            entry.Dispose();

            return value;
        }

        internal T MemSet<T>(object key, T value, TimeSpan absoluteExpirationRelativeToNow)
        {
            var entry = this.MemCreateEntry(key);
            entry.AbsoluteExpirationRelativeToNow = absoluteExpirationRelativeToNow;
            entry.Value = value;
            entry.Dispose();

            return value;
        }

        internal T MemSet<T>(object key, T value, MemoryCacheEntryOptions options)
        {
            using (var entry = this.MemCreateEntry(key))
            {
                if (options != null)
                    entry.SetOptions(options);

                entry.Value = value;
            }

            return value;
        }

        //internal TItem GetOrCreate<TItem>(object key, Func<ICacheEntry, TItem> factory)
        //{
        //    if (!this.MemTryGetValue(key, out var result))
        //    {
        //        var entry = this.MemCreateEntry(key);
        //        result = factory(entry);
        //        entry.SetValue(result);
        //        entry.Dispose();
        //    }

        //    return (TItem)result;
        //}

        //internal async Task<TItem> GetOrCreateAsync<TItem>(object key, Func<ICacheEntry, Task<TItem>> factory)
        //{
        //    if (!this.MemTryGetValue(key, out object result))
        //    {
        //        var entry = this.MemCreateEntry(key);
        //        result = await factory(entry);
        //        entry.SetValue(result);
        //        entry.Dispose();
        //    }
        //    return (TItem)result;
        //}
        #endregion

        #endregion

    }

    public static class DbCacheHelperExtensions
    {
        #region Cache 
        public static async Task<int> ClearCacheAsync(this DataSvc da, string key = null)
        {
            try
            {
                if (key == null)
                {
                    return await da.ExecuteNonQueryAsync("TRUNCATE TABLE Cache");
                }
                else
                {
                    return await da.ExecuteNonQueryAsync("DELETE FROM Cache WHERE key=@key", da.Args("@key", key));
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static async Task<int> ClearCacheAsync(this DataSvc da, object key = null)
        {
            try
            {
                if (key == null)
                {
                    return await da.ExecuteNonQueryAsync("TRUNCATE TABLE Cache");
                }
                else
                {
                    poDeleteQuery q = new poDeleteQuery("cache");
                    q.WhereEquals("key", key);
                    return await da.ExecuteNonQueryAsync(q);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


        public static async Task<int> SetCacheAsync<T>(this DataSvc da, object key, T value, TimeSpan? offset)
        {
            int n = 0;
            try
            {
                DateTime dtOffset = default(DateTime);
                if (offset.HasValue)
                {
                    if (offset.Value.TotalSeconds > 0)
                    {
                        dtOffset = DateTime.Now.Add(offset.Value);
                    }
                }
                if (value == null)
                {
                    poUpdateQuery q = new poUpdateQuery("cache");
                    q.Terms.Add(new UpdateTerm("Value", SqlExpression.Null()));
                    q.Terms.Add(new UpdateTerm("Expires", SqlExpression.Null()));
                    q.AddTerm("Dateupdated", DateTime.Now);
                    q.Where("key", key, Reeb.SqlOM.CompareOperator.Equal);
                    n = await da.ExecuteNonQueryAsync(q);
                }
                else
                {
                    poUpdateQuery q = new poUpdateQuery("cache");
                    q.AddTerm("value", value.AsJson<T>());
                    q.AddTerm("Dateupdated", DateTime.Now);
                    if (dtOffset != default(DateTime)) q.AddTerm("Expires", dtOffset);
                    q.Where("key", key, Reeb.SqlOM.CompareOperator.Equal);
                    n = await da.ExecuteNonQueryAsync(q);
                    if (n == 0)
                    {
                        poInsertQuery qu = new poInsertQuery("cache");
                        qu.AddTerm("ID", da.NEWID());
                        qu.AddTerm("key", key);
                        qu.AddTerm("value", value.AsJson<T>());
                        qu.AddTerm("DateCreated", DateTime.Now);
                        if (dtOffset != default(DateTime)) q.AddTerm("Expires", dtOffset);
                        n = await da.ExecuteNonQueryAsync(qu);
                    }
                }
                return n;
            }
            catch (Exception)
            {
                throw;
            }
        }

        //public static async Task<int> SetCacheAsync(this DataSvc da, object key, object value, DateTime offset)
        //{
        //    int n = 0;
        //    try
        //    {
        //        if (value == null)
        //        {
        //            poUpdateQuery q = new poUpdateQuery("cache");
        //            q.Terms.Add(new UpdateTerm("value", SqlExpression.Null()));
        //            q.AddTerm("Dateupdated", DateTime.Now);
        //            q.Where("key", key, Reeb.SqlOM.CompareOperator.Equal);
        //            n = await da.ExecuteNonQueryAsync(q);
        //        }
        //        else
        //        {
        //            poUpdateQuery q = new poUpdateQuery("cache");
        //            q.AddTerm("value", value.AsJson());
        //            q.AddTerm("Dateupdated", DateTime.Now);
        //            q.Where("key", key, Reeb.SqlOM.CompareOperator.Equal);
        //            n = await da.ExecuteNonQueryAsync(q);
        //            if (n == 0)
        //            {
        //                poInsertQuery qu = new poInsertQuery("cache");
        //                qu.AddTerm("ID", da.NEWID());
        //                qu.AddTerm("key", key);
        //                qu.AddTerm("value", value.AsJson());
        //                qu.AddTerm("DateCreated", DateTime.Now);
        //                n = await da.ExecuteNonQueryAsync(qu);
        //            }
        //        }
        //        return n;
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}
        public static async Task<T> GetCacheAsync<T>(this DataSvc da, string key)
        {
            try
            {
                poSelectQuery q = new poSelectQuery("cache");
                q.AddColumn("Value", q.BaseTable);
                q.Where("key", key, Reeb.SqlOM.CompareOperator.Equal);

                WhereClause wAnd = new WhereClause(WhereClauseRelationship.And);
                wAnd.Terms.Add(WhereTerm.CreateIsNotNull(SqlExpression.Field("Expires")));
                wAnd.Terms.Add(WhereTerm.CreateCompare(SqlExpression.Field("Expires"), q.ParameterValue(DateTime.Now), Reeb.SqlOM.CompareOperator.Greater));

                WhereClause wOr = new WhereClause(WhereClauseRelationship.Or);
                wOr.Terms.Add(WhereTerm.CreateIsNotNull(SqlExpression.Field("Expires")));
                wOr.SubClauses.Add(wAnd);

                string json = await da.ExecuteScalarAsync<string>(q);
                if (json.IsNull())
                {
                    return default(T);
                }
                return json.FromJson<T>();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }

}
