﻿using Core.Common;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace Core.Common
{

    #region < Microsoft.AspNetCore.Http.HttpContext >

    public static class HttpContextExtensions
    {

        public static string SetQueryFromRequest(this Microsoft.AspNetCore.Http.HttpContext ctx, string requestTemplate)
        {
            if (ctx != null && ctx.Request != null && ctx.Request.Query != null && !requestTemplate.IsNullOrEmpty())
            {
                IQueryCollection qc = ctx.Request.Query;
                if (qc != null)
                {
                    string rsp = string.Empty;
                    foreach (var x in requestTemplate.Split(','))
                    {
                        rsp += x + "=" + qc[x] + "&";
                    }
                    rsp = rsp.TrimEnd('&');
                    return rsp;
                }
            }
            return string.Empty;
        }

        public static Dictionary<string, object> GetRequestData(this Microsoft.AspNetCore.Http.HttpContext ctx)
        {
            Dictionary<string, object> val = new Dictionary<string, object>();
            if (ctx != null && ctx.Request != null && ctx.Request.Query != null)
            {
                IQueryCollection qc = ctx.Request.Query;
                IFormCollection data = null;

                if (ctx.Request.Method.ToLower() == "post" && ctx.Request.Form != null) data = ctx.Request.Form;
                if (data != null)
                {
                    foreach (var x in data)
                    {
                        Microsoft.Extensions.Primitives.StringValues tempOrder = new[] { "" };
                        if (data.TryGetValue(x.Key, out tempOrder))
                        {
                            val.Add(x.Key, x.Value);
                        }
                    }
                }
                if (qc != null)
                {
                    foreach (var x in qc)
                    {
                        val.Add(x.Key, x.Value);
                    }
                }
            }
            return val;
        }
    }
    #endregion
    
    

}
