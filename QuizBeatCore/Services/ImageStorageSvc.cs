﻿
using Google.Apis.Auth.OAuth2;
using Google.Cloud.Storage.V1;
using Microsoft.Extensions.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using System.IO;
using System.Threading.Tasks;

namespace QuizBeatCore.Services
{
    public interface IImageStorageService
    {
        Task<string> StoreImage(string filename, byte[] image);
    }

    public class GoogleImageStorageSvc : IImageStorageService
    {
        private readonly IConfiguration _config;

        public GoogleImageStorageSvc(IConfiguration config)
        {
            _config = config;
        }

        public async Task<string> StoreImage(string filename, byte[] image)
        {
            var filenameonly = Path.GetFileNameWithoutExtension(filename);

            //
            /* 
             * API-KEY: 
             *    AIzaSyB5L9YfQcOs5iuer7zav9JXeWl0IRuW3kk
             * JSON Key:
             {
                  "type": "service_account",
                  "project_id": "qbtv-227610",
                  "private_key_id": "f191fc0c40d9cb88bf33c2e67b9d25071672a073",
                  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCoWVQQtlSyD3W8\nFFul8Jj1iuwU43guPqcymOUjdnqe99Lq6ae3EJV8A/8BycYIOdnUwx6MMW97stMd\n56SLSMAuL5RXg2fqhvrBkm6+hXAWzgplSA4X2V9LSYpSoJgszu7q7BHLn/61Rfcj\nsk1sKO7xKPeMmpZoWX3ASfOr4rr/WVK77fItOx9cEonIXfGRacsQMyL03iQeem7h\nyiHCDEGfnZ5PvJUitHWbWdjJfocERjPjMApnZYV72axJHNiVIKFqPDMMwNyIZX32\nnc100zKMrKe9p5of+BtGTFmYq9jOsdAxaG/pkOEFm4uKUiyokg81wDvS3hNAfBh+\n5HuN2DolAgMBAAECggEAHhXYsCPlfuzEXZhFyKH2s+rfxdvg3mLT/nbUfEHwJ/ih\n+NlI365XCJKuP+OPQGkWRK+aBfPPuIt8rxt2jG2z6o0WNY2GRKNarL0+jU3LekfY\nE8GMwzZVkV7TGXEZwOg6SkFl7L3rBvo58Gcs7udhzDdGBhqaRou3rWWJ5m3efUNv\nCcDb0KJ7lmVgyPDAnugC4pnkflgmtiCtWic/DSvAQwW3YMesmrgYEYN2qN2r7rpI\nPRTUPHDnwxXJXdCnQfKMZ2x/jnG2lAI7+mcDo2taqAZ5b9zEbO05wSE4RuqAcp0+\naoJ6GBUEgQoQKv538qOJtdQWF1LpqWBdb9Lf0tXVAQKBgQDV8CAMJpf/R7gMPtkJ\nEYv1JEd25V++iTrJr6sImKZRqmz0OqVrJT+YWDOFZ9c5RODkBMp1R4SlB6g9ZQh+\nYytndBXLh0T7N8eRUTetzu792td9OcaJQhlFnhUZLrzLs+mqzY62sAsGfMrXJiBp\n2niXrlG660V8RY4ZDASVw7JVGwKBgQDJcqBP9eM1OAEnMQEzsrzsEnYCQ1OeoEdD\nYqILbQJSQf/TNUk7bJEhmUe4iGDnWzzw7oxflqxTbUCMifsPpYDNrY/eigCqoekd\nrm0pNlnBj4jkx/C1udAAAo4gwrQi2ch5J62OrU1FxKZcf9pD1H5EnSmyUQRIuPY+\nC7LKYtrhvwKBgFLlUmWi5q6B6azFBW/gHq0EO7S0OB8qKV9Q9CLGk1t9f+46czJL\nIpMzSfNdQyXrrIBt4s0Cw0D28JepeFsQrhbOAkv0xCPbfzatk7eDQVOX4nuhUW6J\n8CTrlh1F4N3FfrsyHyRmnj8LR8WJ/nFFuAh+ASk+VJRMlzzZTUWCqK0VAoGAddl9\nN8odtdChvA/LKDd0lpbmOsSwcqhJiH53Vj/bj8vUofcWdJE7BnDpqGcAadjSehDU\nRkebYOMVVCKUob6s76aN+B677Rbzjc2xRt7o+3fBGjZt96JQsPDNvRSq1Yia42X2\n/vUbs6krugmB4xS5GeLntc33t6OdSwcE/al/a3kCgYEAlIQVEm/yWi6ItzzB/Rig\ngCBTYuab1kCtkAnscctjzdVpj35hJZ5dNiixf/P7PfkNtI06MHtn1Tn9VZHZEPka\nuzTJzZJZmQIBTPqrooRk+URyugB0r3rWHQJSk6RuFyTJ5PBSf72uf3CNk/U6CGqZ\n+m0Wyz3XRgB15sLxvKajY/Q=\n-----END PRIVATE KEY-----\n",
                  "client_email": "qbtv-227610@appspot.gserviceaccount.com",
                  "client_id": "103895337368051104483",
                  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
                  "token_uri": "https://oauth2.googleapis.com/token",
                  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
                  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/qbtv-227610%40appspot.gserviceaccount.com"
                } 
             */
            string bucketName = "qbtv";
            var credential = GoogleCredential.FromJson(System.Text.Encoding.UTF8.GetString( QuizBeatCore.Properties.Resources.QBTV_f191fc0c40d9) );
            StorageClient storageClient = StorageClient.Create(credential);
            var imageAcl = PredefinedObjectAcl.PublicRead;
            string URL = string.Empty;
            using (MemoryStream ms = new MemoryStream(image))
            {
                // 33bbcb96-b69f-4755-b3de-6cbaed4475fb.jpg
                ms.Position = 0;
                var imageObject = await storageClient.UploadObjectAsync(
                    bucket: bucketName,
                    objectName: "images/"+filename,
                    contentType: "image/jpg",
                    source: ms,
                    options: new UploadObjectOptions { PredefinedAcl = imageAcl }
                );
                string publicURL = $"http://storage.googleapis.com/qbtv/images/{filename}";
                URL = publicURL; //imageObject.MediaLink;
            }
            return URL;
        }
    }

    public class LocalImageStorageSvc : IImageStorageService
    {
        private readonly IConfiguration _config;
        IQbDataService dbSvc;

        public LocalImageStorageSvc(IConfiguration config, IQbDataService da)
        {
            _config = config;
            dbSvc = da;
        }

        public async Task<string> StoreImage(string filename, byte[] image)
        {
            var filenameonly = Path.GetFileNameWithoutExtension(filename);

            int n = await dbSvc.InsertUserImage(filenameonly, filename, image);

            return $"/api/User/GetUserImage?userid={filenameonly}";
        }
    }


    public class AzureImageStorageSvc : IImageStorageService
    {
        private readonly IConfiguration _config;

        public AzureImageStorageSvc(IConfiguration config)
        {
            _config = config;
        }

        public async Task<string> StoreImage(string filename, byte[] image)
        {
            var filenameonly = Path.GetFileName(filename);

            //https://qbtv.blob.core.windows.net/images/
            var url = string.Concat(_config["BlobService:StorageUrl"], filenameonly);

            var creds = new StorageCredentials(_config["BlobService:Account"], _config["BlobService:Key"]);

            // Create storagecredentials object by reading the values from the configuration (appsettings.json)
            StorageCredentials storageCredentials = new StorageCredentials(_config["BlobService:Account"], _config["BlobService:Key"]);

            // Create cloudstorage account by passing the storagecredentials
            CloudStorageAccount storageAccount = new CloudStorageAccount(storageCredentials, true);

            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            // Get reference to the blob container by passing the name by reading the value from the configuration (appsettings.json)
            CloudBlobContainer container = blobClient.GetContainerReference("images");

            // Get the reference to the block blob from the container
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(filename);

            // Upload the file
            await blockBlob.UploadFromByteArrayAsync(image, 0, image.Length);
            return url;
        }
    }

}
