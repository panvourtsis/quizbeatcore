﻿using Core.Common;
using Core.Common.SqlOM;
using Core.Helpers;
using QuizBeat.Models.Database;
using QuizBeat.Models.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QuizBeatCore.Services
{


    public interface IQbDataService
    {

        Task<IEnumerable<User>> Top100AllTimeUsers();
        Task<IEnumerable<InvitationRankingDto>> AllInvitationRankingsDtos(string userID);
        Task InsertInvitation(Invitation item);
        Task InsertLivesBought(LivesBought item);
        Task<IEnumerable<Invitation>> FindInvitations(string UserID);
        Task<Invitation> FindPendingInvitation(string invitedUserID, bool hasBeenRedeemed);
        Task<User> GetUserByID(string id);
        Task<User> GetUserByEmail(string email);
        Task<User> GetUserByUserName(string userName);
        Task<User> GetUserByAuthToken(string u);
        Task<bool> DoesUserExist(string id, string username, string email);
        Task<User> FindUserWithResetPassword(string resetPasswordToken = null);
        Task<User> FindUser(string id = null, string email = null, string username = null);
        Task<User> FindUser(string authToken);
        Task<User> FindUser(Guid authToken);
        Task InsertUser(User item);
        Task UpdateUser(User item);
        Task UpdateUserLastActive(User item);
        Task UpdateUserReset(User item);
        Task UpdateUserExtraLives(User item);
        Task<User_Game> FindUser_Game(string id = null);
        Task<User_Game> FindUser_GameByUserID(string userID);
        Task<User_Game> FindUser_Game(string userID, string gameID);
        Task<Question> FindQuestion(string id);
        Task<User_Question> FindUser_Question(string userID, string gameID, string questionID);
        Task<IEnumerable<User_Game>> AllUser_Games(string gameID);
        Task InsertUser_Game(User_Game item);
        Task UpdateUser_Game(User_Game item);
        Task UpdateUser_GameEliminated(User_Game item);
        Task<User_PrizeWon> FindUser_PrizeWon(string id);
        Task<int> ReactivateUser(string GameID, string UserID, int livesNeeded, string lastUserQuestionID, string activeQuestionID);
        //void ReactivateUser(User u, User_Question uq, User_Game ug, LivesUsed lu);

        Task<List<Game>> AllGames();
        Task<Game> FindStartedgGame();
        Task<Game> FindRunningGame(bool? includeQuestions = null, bool? includePrize = null);
        Task<Game> FindActiveGame(bool? includeQuestions = null, bool? includePrize = null);
        Task<IList<Question>> FindGameQuestions(string gameID);
        Task<Prize> FindGamePrize(string prizeID);
        Task<Game> FindNextGame();

        Task<Donation> FindDonation(string id = null, bool? isActive = null);
        Task UpdateDonation(Donation item);
        Task<CashOut> FindCashOut(string id);
        Task<CashOut> FindPendingCashOut(string userId = null);
        Task UpdateUserCashedOut(User item);
        Task InsertCashOut(CashOut item);
        Task InsertGameRating(GameRating item);
        Task InsertUser_Question(User_Question item);
        Task UpdateInvitation(Invitation item);
        Task UpdateInvitationRedeemed(Invitation item);

        Task<int> InsertUserImage(string userid, string fileName, byte[] data);
        Task<dynamic> GetUserImage(string userid);

        Task<int> Update<T>(T item) where T : class, new();
        Task<int> Insert<T>(T item) where T : class, new();

        //#region Cache 
        //Task<int> ClearCache(string key=null);
        //Task<int> SetCache<T>(string key, T value);
        //Task<T> GetCache<T>(string key);
        //#endregion
    }

    public class QbDataService : IQbDataService
    {

        public async Task<int> InsertUserImage(string userid, string fileName, byte[] data)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await da.InsertUserImage(userid, fileName, data);
            }
        }

        public async Task<dynamic> GetUserImage(string userid)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await GetUserImage(userid);
            }
        }


        public async Task<IEnumerable<User>> Top100AllTimeUsers()
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await da.Top100AllTimeUsers();
            }
        }

        #region < GetUser >
        public async Task<User> GetUserByID(string id)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await da.GetUserByID(id);
            }
        }

        public async Task<User> GetUserByEmail(string email)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await da.GetUserByEmail(email);
            }
        }

        public async Task<User> GetUserByUserName(string userName)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await da.GetUserByUserName(userName);
            }
        }

        public async Task<User> GetUserByAuthToken(string u)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await da.GetUserByAuthToken(u);
            }
        }

        public async Task<bool> DoesUserExist(string id, string username, string email)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await da.DoesUserExist(id, username, email);
            }
        }

        public async Task<User> FindUserWithResetPassword(string resetPasswordToken = null)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await da.FindUserWithResetPassword(resetPasswordToken);
            }
        }

        public async Task<User> FindUser(string id = null, string email = null, string username = null)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await da.FindUser(id, email, username);
            }
        }

        public async Task<User> FindUser(string authToken)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await da.FindUser(authToken);
            }
        }

        public async Task<User> FindUser(Guid authToken)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await da.FindUser(authToken);
            }
        }

        public async Task InsertUser(User item)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                await da.InsertUser(item);
            }
        }

        public async Task UpdateUser(User item)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                await da.UpdateUser(item);
            }
        }

        public async Task UpdateUserLastActive(User item)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                await da.UpdateUserLastActive(item);
            }
        }

        public async Task UpdateUserReset(User item)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                await da.UpdateUserReset(item);
            }
        }

        public async Task UpdateUserExtraLives(User item)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                await da.UpdateUserExtraLives(item);
            }
        }


        public async Task<User_Game> FindUser_Game(string id = null)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await FindUser_Game(id);
            }
        }

        public async Task<User_Game> FindUser_GameByUserID(string userID)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await FindUser_GameByUserID(userID);
            }
        }

        public async Task<User_Game> FindUser_Game(string userID, string gameID)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await da.FindUser_Game(userID, gameID);
            }
        }

        public async Task<Question> FindQuestion(string id)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await da.FindQuestion(id);
            }
        }


        public async Task<User_Question> FindUser_Question(string userID, string gameID, string questionID)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await da.FindUser_Question(userID, gameID, questionID);
            }
        }

        public async Task<IEnumerable<User_Game>> AllUser_Games(string gameID)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await da.AllUser_Games(gameID);
            }
        }

        public async Task InsertUser_Game(User_Game item)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                await da.InsertUser_Game(item);
            }
        }

        public async Task UpdateUser_Game(User_Game item)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                await da.UpdateUser_GameAsync(item);
            }
        }

        public async Task UpdateUser_GameEliminated(User_Game item)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                await da.UpdateUser_GameEliminated(item);
            }
        }

        public async Task<IEnumerable<InvitationRankingDto>> AllInvitationRankingsDtos(string userID)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await da.AllInvitationRankingsDtos(userID);
            }
        }


        public async Task InsertInvitation(Invitation item)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                await da.InsertInvitation(item);
            }

        }

        public async Task InsertLivesBought(LivesBought item)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                await da.InsertLivesBought(item);
            }
        }



        public async Task<IEnumerable<Invitation>> FindInvitations(string UserID)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await da.FindInvitations(UserID);
            }
        }


        #endregion


        #region < Game >

        public async Task<List<Game>> AllGames()
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await da.AllGames();
            }
        }

        public async Task<Game> FindGame(string id)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await da.FindGame(id);
            }
        }


        public async Task<Game> FindNextGame()
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await da.FindNextGame();
            }
        }

        public async Task<Game> FindActiveGame(bool? includeQuestions = null, bool? includePrize = null)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await da.FindActiveGame(includeQuestions, includePrize);
            }
        }

        public async Task<IList<Question>> FindGameQuestions(string gameID)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await da.FindGameQuestions(gameID);
            }
        }

        public async Task<Prize> FindGamePrize(string prizeID)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await da.FindGamePrize(prizeID);
            }
        }


        public async Task<Game> FindRunningGame(bool? includeQuestions = null, bool? includePrize = null)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await da.FindRunningGame(includeQuestions, includePrize);
            }
        }

        public async Task<Game> FindStartedgGame()
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await da.FindStartedgGame();
            }
        }


        #endregion


        public async Task<User_PrizeWon> FindUser_PrizeWon(string id)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await da.FindUser_PrizeWon(id);
            }
        }

        public async Task<Donation> FindDonation(string id = null, bool? isActive = null)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {

                return await da.FindDonation(id, isActive);
            }
        }

        public async Task UpdateDonation(Donation item)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                await da.UpdateDonation(item);
            }
        }

        public async Task UpdateInvitation(Invitation item)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                await da.UpdateInvitation(item);
            }
        }

        public async Task UpdateInvitationRedeemed(Invitation item)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                await da.UpdateInvitationRedeemed(item);
            }
        }

        #region < Cashout >

        public async Task<CashOut> FindCashOut(string id)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await da.FindCashOut(id);
            }
        }

        public async Task<CashOut> FindPendingCashOut(string userId = null)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await da.FindPendingCashOut(userId);
            }
        }

        public async Task UpdateUserCashedOut(User item)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                await da.UpdateUserCashedOut(item);
            }
        }

        public async Task<Invitation> FindPendingInvitation(string invitedUserID, bool hasBeenRedeemed)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await da.FindPendingInvitation(invitedUserID, hasBeenRedeemed);
            }
        }


        public async Task InsertCashOut(CashOut item)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                await da.InsertCashOut(item);
            }
        }

        public async Task InsertGameRating(GameRating item)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                await da.InsertGameRating(item);
            }
        }


        public async Task InsertUser_Question(User_Question item)
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                await da.InsertUser_QuestionAsync(item);
            }
        }


        public async Task<int> Insert<T>(T item) where T : class, new()
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await da.Insert<T>(item);
            }
        }


        public async Task<int> Update<T>(T item) where T : class, new()
        {
            using (DataSvc da = DataSvc.GetDataService())
            {
                return await da.Update<T>(item);
            }
        }

        #endregion


        public async Task<int> ReactivateUser(string GameID, string UserID, int livesNeeded, string lastUserQuestionID, string activeQuestionID)
        {
            try
            {
                using (DataSvc da = DataSvc.GetDataService())
                {
                    return await da.ReactivateUser(GameID, UserID, livesNeeded, lastUserQuestionID, activeQuestionID);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public void ReactivateUser(User u, User_Question uq, User_Game ug, LivesUsed lu)
        //{
        //    try
        //    {
        //        using (DataSvc da = DataSvc.GetDataService())
        //        {
        //            da.ReactivateUser(u, uq, ug, lu);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}



        //#region Cache 
        //public async Task<int> ClearCache(string key=null)
        //{
        //    try
        //    {
        //        using (DataSvc da = DataSvc.GetDataService())
        //        {
        //            return await da.ClearCache(key);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}


        //public async Task<int> SetCache<T>(string key, T value)
        //{
        //    try
        //    {
        //        using (DataSvc da = DataSvc.GetDataService())
        //        {
        //            return await da.SetCache<T>(key, value);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public async Task<T> GetCache<T>(string key) 
        //{
        //    try
        //    {
        //        using (DataSvc da = DataSvc.GetDataService())
        //        {
        //            return await da.GetCache<T>(key);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //#endregion



    }




}
