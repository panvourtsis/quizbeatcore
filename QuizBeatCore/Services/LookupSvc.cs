﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.AspNet.Services
{
    public class LookupService : ILookupService
    {
        private readonly IHttpContextAccessor contextAccessor;
        private readonly IMemoryCache MemCache;

        public LookupService(IHttpContextAccessor contextAccessor, IMemoryCache cache)
        {
            this.contextAccessor = contextAccessor;
            this.MemCache = cache;
        }

        public HttpContext GetContext()
        {
            return contextAccessor.HttpContext;
        }

        public IMemoryCache GetCache()
        {
            return this.MemCache;
        }

        public void Add(string key, object data, int hours = 3)
        {
            MemCache.Remove(key);
            MemCache.Set(key, data, DateTimeOffset.UtcNow.AddHours(hours));
        }

        public T Get<T>(string id)
        {
            T d = default(T);
            MemCache.TryGetValue<T>(id, out d);
            return d;
        }

        public object Get(string id)
        {
            object d = null;
            MemCache.TryGetValue(id, out d);
            return d;
        }

    }


    public interface ILookupService
    {
        HttpContext GetContext();
        IMemoryCache GetCache();
        void Add(string key, object data, int hours = 3);
        T Get<T>(string id);
        object Get(string id);
    }
}
