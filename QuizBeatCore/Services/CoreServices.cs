﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using System;

namespace Core.AspNet
{

    #region < HttpHelper >
    public static class HttpHelper
    {
        private static IHttpContextAccessor _accessor;
        public static void Configure(IHttpContextAccessor httpContextAccessor)
        {
            _accessor = httpContextAccessor;
        }

        public static IHttpContextAccessor HttpContextAccessor => _accessor;

        public static HttpContext HttpContext => _accessor.HttpContext;

        public static string Url => _accessor.HttpContext.Request.GetDisplayUrl();

    }
    #endregion
    
    #region < ContextAccessor >

    public interface IContextAccessor
    {
        HttpContext GetContext();
        string GetSession(string key);
        ISession GetSession();
    }

    public class ContextAccessor : IContextAccessor
    {
        private readonly IHttpContextAccessor contextAccessor;

        public ContextAccessor(IHttpContextAccessor contextAccessor)
        {
            this.contextAccessor = contextAccessor;
        }

        public HttpContext GetContext()
        {
            return contextAccessor.HttpContext;
        }

        public string GetSession(string key)
        {
            return contextAccessor.HttpContext.Session.GetString(key);
        }
        public ISession GetSession()
        {
            return contextAccessor.HttpContext.Session;
        }
    }

    #endregion

    #region < AppHttpContext >
    public static class AppHttpContext
    {
        static IServiceProvider services = null;

        /// <summary>
        /// Provides static access to the framework's services provider
        /// </summary>
        public static IServiceProvider Services {
            get { return services; }
            set {
                if (services != null)
                {
                    throw new Exception("Can't set once a value has already been set.");
                }
                services = value;
            }
        }

        /// <summary>
        /// Provides static access to the current HttpContext
        /// </summary>
        public static HttpContext Current {
            get {
                IHttpContextAccessor httpContextAccessor = services.GetService(typeof(IHttpContextAccessor)) as IHttpContextAccessor;
                return httpContextAccessor?.HttpContext;
            }
        }
    }
    #endregion
    
    /*
    #region < ConfigHelper >
    public static class CoreConfigHelper
    {
        private static AppSettings _config;
        private static IServiceProvider _services;
        private static ILogger _logger;

        public static void Configure(IOptions<AppSettings> appSettings)
        {
            _config = appSettings.Value;
        }

        public static void Configure(AppSettings appSettings)
        {
            _config = appSettings;
        }

        public static void Configure(IServiceProvider Services)
        {
            _services = Services;
        }

        public static void Configure(ILogger logger)
        {
            _logger = logger;
        }

        public static IAppSettings Current => _config;
        public static ILogger Logger => _logger;
        public static IServiceProvider Services => _services;

    }
    #endregion
    
    #region < SettingsReader >

    public interface ISettingsReader
    {
        T Load<T>() where T : class, new();
        T LoadSection<T>() where T : class, new();
        object Load(Type type);
        object LoadSection(Type type);
    }

    public class SettingsReader : ISettingsReader
    {
        private readonly string _configurationFilePath;
        private readonly string _sectionNameSuffix;

        private static readonly JsonSerializerSettings JsonSerializerSettings = new JsonSerializerSettings
        {
            ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor,
            ContractResolver = new SettingsReaderContractResolver(),
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
        };

        public SettingsReader(string configurationFilePath, string sectionNameSuffix = "Settings")
        {
            _configurationFilePath = configurationFilePath;
            _sectionNameSuffix = sectionNameSuffix;
        }

        public T Load<T>() where T : class, new() => Load(typeof(T)) as T;

        public T LoadSection<T>() where T : class, new() => LoadSection(typeof(T)) as T;

        public object Load(Type type)
        {
            if (!File.Exists(_configurationFilePath))
                return Activator.CreateInstance(type);

            var jsonFile = File.ReadAllText(_configurationFilePath);

            return JsonConvert.DeserializeObject(jsonFile, type, JsonSerializerSettings);
        }

        public object LoadSection(Type type)
        {
            if (!File.Exists(_configurationFilePath))
                return Activator.CreateInstance(type);

            var jsonFile = File.ReadAllText(_configurationFilePath);
            var section = ToCamelCase(type.Name.Replace(_sectionNameSuffix, string.Empty));
            var settingsData = JsonConvert.DeserializeObject<dynamic>(jsonFile, JsonSerializerSettings);
            var settingsSection = settingsData[section];

            return settingsSection == null
                ? Activator.CreateInstance(type)
                : JsonConvert.DeserializeObject(JsonConvert.SerializeObject(settingsSection), type,
                    JsonSerializerSettings);
        }

        private class SettingsReaderContractResolver : DefaultContractResolver
        {
            protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
            {
                var props = type.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)
                    .Select(p => CreateProperty(p, memberSerialization))
                    .Union(type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)
                        .Select(f => CreateProperty(f, memberSerialization)))
                    .ToList();
                props.ForEach(p =>
                {
                    p.Writable = true;
                    p.Readable = true;
                });

                return props;
            }
        }

        private static string ToCamelCase(string text)
            => string.IsNullOrWhiteSpace(text)
                ? string.Empty
                : $"{text[0].ToString().ToLowerInvariant()}{text.Substring(1)}";
    }

    #endregion
    */
}
