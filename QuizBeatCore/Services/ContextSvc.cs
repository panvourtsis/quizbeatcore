﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.AspNet.Services
{
    public class ContextService : IContextService
    {
        private readonly IHttpContextAccessor contextAccessor;
        private readonly IMemoryCache MemCache;

        public ContextService(IHttpContextAccessor contextAccessor, IMemoryCache cache)
        {
            this.contextAccessor = contextAccessor;
            this.MemCache = cache;
        }

        public HttpContext GetContext()
        {
            return contextAccessor.HttpContext;
        }

        public IMemoryCache GetCache()
        {
            return this.MemCache;
        }
    }


    public interface IContextService
    {
        HttpContext GetContext();
        IMemoryCache GetCache();
    }
}
