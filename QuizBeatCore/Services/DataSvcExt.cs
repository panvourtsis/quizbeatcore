﻿using Core.Common;
using Core.Common.SqlOM;
using QuizBeat.Models.Database;
using QuizBeat.Models.Dto;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizBeatCore.Services
{



    public static class QbDatasvcExtensions
    {

        public static async Task<int> InsertUserImage(this DataSvc da, string userid, string fileName, byte[] data)
        {
            int n = 0;
            poUpdateQuery qu = new poUpdateQuery("User_Image");
            qu.AddTerm("Image", data);
            qu.AddTerm("FileName", fileName);
            qu.WhereEquals("UserID", userid);
            n = await da.ExecuteNonQueryAsync(qu);

            if (n == 0)
            {
                poInsertQuery q = new poInsertQuery("User_Image");
                q.AddTerm("ID", DataSvc.NEWSEQUENTIALID());
                q.AddTerm("UserID", userid);
                q.AddTerm("FileName", fileName);
                q.AddTerm("Image", data);
                n = await da.ExecuteNonQueryAsync(q);
            }
            return n;
        }

        public static async Task<dynamic> GetUserImage(this DataSvc da, string userid)
        {
            poSelectQuery q = new poSelectQuery("User_Image");
            q.Top = 1;
            q.AddColumn("Image", q.BaseTable);
            q.AddColumn("FileName", q.BaseTable);
            q.WhereEquals("UserID", userid);

            return await da.ExecuteDynamicObjectAsync(q);
        }


        public static async Task<IEnumerable<User>> Top100AllTimeUsers(this DataSvc da)
        {
            List<User> ls = new List<User>();
            return await da.ExecuteListAsync<User>("select top 100 ID, Username, email, Money_Won, Ranking  from [user] where ranking > 0 order by ranking asc");
        }

        #region < GetUser >
        public static async Task<User> GetUserByID(this DataSvc da, string id)
        {
            return await da.ExecuteObjectAsync<User>("select * from [user] where id=@id", da.Args("@id", id));
        }

        public static async Task<User> GetUserByEmail(this DataSvc da, string email)
        {
            return await da.ExecuteObjectAsync<User>("select * from [user] where email=@email", da.Args("@email", email));
        }

        public static async Task<User> GetUserByUserName(this DataSvc da, string userName)
        {
            User usr = null;

            {
                usr = await da.ExecuteObjectAsync<User>("select * from [user] where UserName=@u", da.Args("@u", userName));
            }
            return usr;
        }

        public static async Task<User> GetUserByAuthToken(this DataSvc da, string u)
        {
            return await da.ExecuteObjectAsync<User>("select * from [user] where AuthToken=@u", da.Args("@u", u));
        }

        public static async Task<bool> DoesUserExist(this DataSvc da, string id, string username, string email)
        {

            int n = 0;

            {
                if (!id.IsNullOrEmpty())
                {
                    n = await da.ExecuteScalarAsync<int>("select count(*) from [user] where ID = @id", da.Args("@id", id));
                }
                else if (!username.IsNullOrEmpty())
                {
                    n = await da.ExecuteScalarAsync<int>("select count(*) from [user] where username = @id", da.Args("@id", username));
                }
                else if (!email.IsNullOrEmpty())
                {
                    n = await da.ExecuteScalarAsync<int>("select count(*) from [user] where email = @id", da.Args("@id", email));
                }

            }
            return n == 1;
        }

        public static async Task<User> FindUserWithResetPassword(this DataSvc da, string resetPasswordToken = null)
        {
            var todayMinus7 = DateTime.UtcNow.AddDays(-7);

            //IEnumerable<User> users = await _context.Users.Where(item => item.ResetPasswordToken == resetPasswordToken && todayMinus7 < item.ResetPasswordRequestedDate.AsDateOnly()).ToListAsync();
            return await da.ExecuteObjectAsync<User>("select top 1 * from [user] where ResetPasswordToken=@u and @d < ResetPasswordRequestedDate ", da.Args("@u", resetPasswordToken, "@d", todayMinus7.AsDateOnly()));
        }

        public static async Task<User> FindUser(this DataSvc da, string id = null, string email = null, string username = null)
        {
            if (!id.IsNullOrEmpty())
                return await da.GetUserByID(id);
            else if (!email.IsNullOrEmpty())
                return await da.GetUserByEmail(email);
            else if (!username.IsNullOrEmpty())
                return await da.GetUserByUserName(username);

            return null;
        }

        public static async Task<User> FindUser(this DataSvc da, string authToken)
        {
            return await da.GetUserByAuthToken(authToken);
        }

        public static async Task<User> FindUser(this DataSvc da, Guid authToken)
        {
            return await da.GetUserByAuthToken(authToken.AsString());
        }

        public static async Task InsertUser(this DataSvc da, User item)
        {
            poInsertQuery q = new poInsertQuery("User");
            if (item.ID.IsNull()) item.ID = da.NEWID();
            q.AddTerm("ID", item.ID);
            q.AddInsertTerms(item);
            await da.ExecuteNonQueryAsync(q);
        }

        public static async Task UpdateUser(this DataSvc da, User item)
        {
            poUpdateQuery q = new poUpdateQuery("User");
            q.AddUpdateTerms(item);
            q.WhereEquals("id", item.ID);
            await da.ExecuteNonQueryAsync(q);

            if (item.User_Game != null && item.User_Game.Count > 0)
            {
                foreach (var ug in item.User_Game)
                {
                    await da.UpdateUser_GameAsync(ug);
                }
            }
        }

        public static async Task UpdateUserLastActive(this DataSvc da, User item)
        {
            poUpdateQuery q = new poUpdateQuery("User");
            q.AddTerm("LastActive_At", item.LastActive_At);
            q.WhereEquals("id", item.ID);
            await da.ExecuteNonQueryAsync(q);
        }

        public static async Task UpdateUserReset(this DataSvc da, User item)
        {
            poUpdateQuery q = new poUpdateQuery("User");
            q.AddTerm("ResetPasswordToken", item.ResetPasswordToken);
            q.AddTerm("ResetPasswordRequestedDate", item.ResetPasswordRequestedDate);
            q.WhereEquals("id", item.ID);
            await da.ExecuteNonQueryAsync(q);
        }

        public static async Task UpdateUserExtraLives(this DataSvc da, User item)
        {
            poUpdateQuery q = new poUpdateQuery("User");
            q.AddTerm("ExtraLives", item.ExtraLives);
            q.WhereEquals("ID", item.ID);
            await da.ExecuteNonQueryAsync(q);
        }


        public static async Task<User_Game> FindUser_Game(this DataSvc da, string id = null)
        {
            return await da.ExecuteObjectAsync<User_Game>("select top 1 * from [User_Game] where id=@id", da.Args("@id", id));
        }

        public static async Task<User_Game> FindUser_GameByUserID(this DataSvc da, string userID)
        {
            return await da.ExecuteObjectAsync<User_Game>("select top 1 * from [User_Game] where userid=@id order by DateJoined DESC", da.Args("@id", userID));
        }

        public static async Task<User_Game> FindUser_Game(this DataSvc da, string userID, string gameID)
        {
            var ug = await da.ExecuteObjectAsync<User_Game>("select top 1 * from [User_Game] where UserID=@userid and GameID=@gameid ", da.Args("@userid", userID, "@gameid", gameID));
            if (ug != null)
            {
                ug.User_Question = await da.ExecuteListAsync<User_Question>("select * from [User_Question] where User_GameID=@user_gameid and userid=@userid and gameid=@gameid", da.Args("@userid", userID, "@gameid", gameID, "@user_gameid", ug.ID));
                if (ug.User_Question.Count > 0)
                {
                    string sql = @"SELECT q.* FROM Question q INNER JOIN User_Question uq ON uq.QuestionID=q.ID WHERE uq.User_Gameid=@ugid AND uq.Gameid=@gameid ORDER BY q.Sorting";
                    List<Question> lsq = await da.ExecuteListAsync<Question>(sql, da.Args("@ugid", ug.ID, "@gameid", gameID));
                    foreach (var q in ug.User_Question)
                    {
                        q.Question = lsq.FirstOrDefault<Question>(x => x.ID == q.QuestionID); ;
                    }
                }
            }
            return ug;

            //return await _context.User_Game.Include("User_Question").Where(item => item.UserID == userID && item.GameID == gameID).FirstOrDefaultAsync();
            //User_Game ug = await _context.User_Game.Include("User_Question").Where(item => item.UserID == userID && item.GameID == gameID).FirstOrDefaultAsync();
            //if (ug != null)
            //{
            //    foreach (var q in ug.User_Question)
            //    {
            //        q.Question = await FindQuestion(q.ID);
            //    }
            //}
            //return ug;
        }

        public static async Task<Question> FindQuestion(this DataSvc da, string id)
        {
            return await da.ExecuteObjectAsync<Question>("select top 1 * from [Question] where id=@id", da.Args("@id", id));
        }

        public static async Task<Question> FindQuestion(this DataSvc da, string id, string usergameid)
        {
            return await da.ExecuteObjectAsync<Question>("select top 1 * from [Question] where id=@id", da.Args("@id", id));
        }

        public static async Task<User_Question> FindUser_Question(this DataSvc da, string userID, string gameID, string questionID)
        {
            User_Question q = await da.ExecuteObjectAsync<User_Question>("select top 1 * from [User_Question] where questionid=@questionid", da.Args("@questionid", questionID));
            if (q != null)
            {
                q.User_Game = await da.ExecuteObjectAsync<User_Game>("select top 1 * from [User_Game] where userid=@userid and gameid=@gameid", da.Args("@userid", userID, "@gameid", gameID));
                q.Question = await da.FindQuestion(questionID);
                q.User = await da.GetUserByID(userID);
            }
            return q;
            //return await _context.User_Question.Include("User_Game").Where(l => l.User_Game.UserID == userID && l.User_Game.GameID == gameID).FirstOrDefaultAsync(item => item.QuestionID == questionID);
        }

        public static async Task<IEnumerable<User_Game>> AllUser_Games(this DataSvc da, string gameID)
        {
            List<User_Game> ls = await da.ExecuteListAsync<User_Game>("select * from [User_Game] where gameid=@gameid", da.Args("@gameid", gameID));
            return ls;
        }

        public static async Task InsertUser_Game(this DataSvc da, User_Game item)
        {
            poInsertQuery q = new poInsertQuery("User_Game");
            if (item.ID.IsNull()) item.ID = da.NEWID();
            q.AddTerm("ID", item.ID);
            q.AddInsertTerms(item);
            await da.ExecuteNonQueryAsync(q);
        }

        public static async Task UpdateUser_GameAsync(this DataSvc da, User_Game item)
        {
            try
            {
                //da.BeginTransaction();
                poUpdateQuery q = new poUpdateQuery("User_Game");
                q.AddUpdateTerms(item);
                q.WhereEquals("id", item.ID);
                await da.ExecuteNonQueryAsync(q);
                //da.CommitTransaction();
            }
            catch (Exception)
            {
                //da.RollbackTransaction();
            }

        }
        public static void UpdateUser_Game(this DataSvc da, User_Game item)
        {
            try
            {
                poUpdateQuery q = new poUpdateQuery("User_Game");
                q.AddUpdateTerms(item);
                q.WhereEquals("id", item.ID);
                da.ExecuteNonQuery(q);
            }
            catch (Exception)
            {
                throw;
            }

        }

        public static async Task UpdateUser_GameEliminated(this DataSvc da, User_Game item)
        {
            try
            {
                //da.BeginTransaction();
                poUpdateQuery q = new poUpdateQuery("User_Game");
                q.AddTerm("HasBeenEliminated", item.HasBeenEliminated ? 1 : 0);
                q.WhereEquals("ID", item.ID);
                await da.ExecuteNonQueryAsync(q);
                //da.CommitTransaction();
            }
            catch (Exception)
            {
                //da.RollbackTransaction();
            }
        }

        public static async Task<IEnumerable<InvitationRankingDto>> AllInvitationRankingsDtos(this DataSvc da, string userID)
        {
            int month = DateTime.Now.Month;
            string monthName = DateTime.UtcNow.ToString("MMMM", CultureInfo.CreateSpecificCulture("el"));
            int year = DateTime.Now.Year;
            List<InvitationRankingDto> lsRet = new List<InvitationRankingDto>();
            List<dynamic> ls = await da.ExecuteDynamicListAsync("SELECT COUNT(*) AS [Count], UserID FROM Invitation WHERE MONTH([date]) = @month AND YEAR([date]) = @year AND Userid=@userid GROUP BY Userid ",
                da.Args("@month", month, "@year", year, "@userid", userID), false);

            foreach (var inv in ls)
            {
                lsRet.Add(new InvitationRankingDto() { UserID = inv.UserID, Month = monthName, NumberOfInvitationSent = inv.Count });
            }

            return lsRet.OrderByDescending(x => x.NumberOfInvitationSent);
        }


        public static async Task InsertInvitation(this DataSvc da, Invitation item)
        {
            poInsertQuery q = new poInsertQuery("Invitation");
            if (item.ID.IsNull()) item.ID = da.NEWID();
            q.AddTerm("ID", item.ID);
            q.AddInsertTerms(item);
            await da.ExecuteNonQueryAsync(q);
        }

        public static async Task InsertLivesBought(this DataSvc da, LivesBought item)
        {
            poInsertQuery q = new poInsertQuery("LivesBought");
            if (item.ID.IsNull()) item.ID = da.NEWID();
            q.AddTerm("ID", item.ID);
            q.AddInsertTerms(item);
            await da.ExecuteNonQueryAsync(q);
        }



        public static async Task<IEnumerable<Invitation>> FindInvitations(this DataSvc da, string UserID)
        {
            List<Invitation> ls = new List<Invitation>();

            ls = await da.ExecuteListAsync<Invitation>("select * from [invitation] where userid=@id", da.Args("@id", UserID));
            if (ls != null && ls.Count > 0)
            {
                foreach (var inv in ls)
                {
                    inv.User = await da.GetUserByID(inv.UserID);
                }
            }
            return ls;

            //return await _context.Invitations.Include("User").Where(item => item.UserID == UserID).ToListAsync();
        }


        #endregion


        #region < Game >

        public static async Task<List<Game>> AllGames(this DataSvc da)
        {
            return await da.ExecuteListAsync<Game>("select * from [Game] ", da.Args());
        }

        public static async Task<Game> FindGame(this DataSvc da, string id)
        {

            return await da.ExecuteObjectAsync<Game>("select top 1 * from [Game] where id=@id", da.Args("@id", id));
        }


        public static async Task<Game> FindNextGame(this DataSvc da)
        {

            //Game nextGame = await _context.Games.Include("Prize").OrderBy(l => l.StartDateTime).Where(item => item.IsActive == false).FirstOrDefaultAsync();
            Game nextGame = await da.ExecuteObjectAsync<Game>("select top 1 * from [Game] where isActive=0 order by startdatetime asc", da.Args());
            if (nextGame != null)
            {
                nextGame.Prize = await da.ExecuteObjectAsync<Prize>("select top 1 * from [Prize] where id=@id", da.Args("@id", nextGame.PrizeID));
            }
            return nextGame;
        }

        public static async Task<Game> FindActiveGame(this DataSvc da, bool? includeQuestions = null, bool? includePrize = null)
        {
            Game activeGame = null;

            activeGame = await da.ExecuteObjectAsync<Game>("select top 1 * from [Game] where isActive=1 and IsCompleted=0", da.Args());
            if (activeGame != null)
            {
                if (includeQuestions.GetValueOrDefault() && includePrize.GetValueOrDefault())
                {
                    activeGame.Prize = await da.ExecuteObjectAsync<Prize>("select top 1 * from [Prize] where id=@id", da.Args("@id", activeGame.PrizeID));
                    activeGame.Questions = await da.ExecuteListAsync<Question>("select * from [Question] where gameid=@id order by sorting", da.Args("@id", activeGame.ID));
                }
                else if (includeQuestions.GetValueOrDefault())
                {
                    activeGame.Questions = await da.ExecuteListAsync<Question>("select * from [Question] where gameid=@id order by sorting", da.Args("@id", activeGame.ID));
                }
                else
                {
                    activeGame.Prize = await da.ExecuteObjectAsync<Prize>("select top 1 * from [Prize] where id=@id", da.Args("@id", activeGame.PrizeID));
                }
                return activeGame;
            }
            return null;
        }

        public static async Task<IList<Question>> FindGameQuestions(this DataSvc da, string gameID)
        {
            return await da.ExecuteListAsync<Question>("select * from [Question] where gameid=@id order by sorting", da.Args("@id", gameID));
        }

        public static async Task<Prize> FindGamePrize(this DataSvc da, string prizeID)
        {
            return await da.ExecuteObjectAsync<Prize>("select * from [Prize] where id=@id", da.Args("@id", prizeID));
        }


        public static async Task<Game> FindRunningGame(this DataSvc da, bool? includeQuestions = null, bool? includePrize = null)
        {
            Game activeGame = null;
            activeGame = await da.ExecuteObjectAsync<Game>("select top 1 * from [Game] where isActive=1 and IsCompleted=0 and hasStarted=0", da.Args());

            if (activeGame != null)
            {
                if (includeQuestions.GetValueOrDefault() && includePrize.GetValueOrDefault())
                {
                    activeGame.Prize = await da.ExecuteObjectAsync<Prize>("select top 1 * from [Prize] where id=@id", da.Args("@id", activeGame.PrizeID));
                    activeGame.Questions = await da.ExecuteListAsync<Question>("select * from [Question] where gameid=@id", da.Args("@id", activeGame.ID));
                }
                else if (includeQuestions.GetValueOrDefault())
                {
                    activeGame.Questions = await da.ExecuteListAsync<Question>("select * from [Question] where gameid=@id", da.Args("@id", activeGame.ID));
                }
                else
                {
                    activeGame.Prize = await da.ExecuteObjectAsync<Prize>("select top 1 * from [Prize] where id=@id", da.Args("@id", activeGame.PrizeID));
                }
                return activeGame;
            }
            return null;
        }

        public static async Task<Game> FindStartedgGame(this DataSvc da)
        {
            Game activeGame = null;
            activeGame = await da.ExecuteObjectAsync<Game>("select top 1 * from [Game] where isActive=1 and hasStarted=1", da.Args());

            if (activeGame != null)
            {
                activeGame.Prize = await da.ExecuteObjectAsync<Prize>("Select top 1 * from [Prize] where id=@id", da.Args("@id", activeGame.PrizeID));
                activeGame.Questions = await da.ExecuteListAsync<Question>("select * from [Question] where gameid=@id", da.Args("@id", activeGame.ID));
            }
            return activeGame;
        }

        #endregion

        public static async Task<User_PrizeWon> FindUser_PrizeWon(this DataSvc da, string id)
        {
            return await da.ExecuteObjectAsync<User_PrizeWon>("select top 1 * from [User_PrizeWon] where id=@id", da.Args("@id", id));
        }

        public static async Task<Donation> FindDonation(this DataSvc da, string id = null, bool? isActive = null)
        {
            if (!id.IsNull())
                return await da.ExecuteObjectAsync<Donation>("select * from [Donation] Where ID=@id", da.Args("@id", id));
            else if (!id.IsNull() && !isActive.IsNull())
                return await da.ExecuteObjectAsync<Donation>("select * from [Donation] Where isactive=@isactive and ID=@id", da.Args("@isactive", (isActive == true ? 1 : 0), "@id", id));
            else if (!isActive.IsNull())
                return await da.ExecuteObjectAsync<Donation>("select * from [Donation] Where isactive=@isactive", da.Args("@isactive", (isActive == true ? 1 : 0)));
            return null;
        }

        public static async Task UpdateDonation(this DataSvc da, Donation item)
        {
            poUpdateQuery q = new poUpdateQuery("Donation");
            q.AddUpdateTerms(item);
            q.WhereEquals("id", item.ID);
            await da.ExecuteNonQueryAsync(q);
        }

        public static async Task UpdateInvitation(this DataSvc da, Invitation item)
        {

            poUpdateQuery q = new poUpdateQuery("Invitation");
            q.AddUpdateTerms(item);
            q.WhereEquals("id", item.ID);
            await da.ExecuteNonQueryAsync(q);
        }

        public static async Task UpdateInvitationRedeemed(this DataSvc da, Invitation item)
        {
            poUpdateQuery q = new poUpdateQuery("Invitation");
            q.AddTerm("HasBeenRedeemed", item.HasBeenRedeemed);
            q.WhereEquals("id", item.ID);
            await da.ExecuteNonQueryAsync(q);
        }

        #region < Cashout >

        public static async Task<CashOut> FindCashOut(this DataSvc da, string id)
        {
            return await da.ExecuteObjectAsync<CashOut>("select top 1 * from [CashOut] where id=@id", da.Args("@id", id));
        }

        public static async Task<CashOut> FindPendingCashOut(this DataSvc da, string userId = null)
        {
            return await da.ExecuteObjectAsync<CashOut>("select top 1 * from [CashOut] where isApproved=0 and UserID=@userid", da.Args("@userid", userId));
        }

        public static async Task UpdateUserCashedOut(this DataSvc da, User item)
        {
            poUpdateQuery q = new poUpdateQuery("User");
            q.AddTerm("Money_CashedOut", item.Money_CashedOut);
            q.WhereEquals("id", item.ID);
            await da.ExecuteNonQueryAsync(q);
        }

        public static async Task<Invitation> FindPendingInvitation(this DataSvc da, string invitedUserID, bool hasBeenRedeemed)
        {
            return await da.ExecuteObjectAsync<Invitation>("select top 1 * from [Invitation] where InvitedUserID=@invitedUserID and HasBeenRedeemed=@hasBeenRedeemed",
                            da.Args("@invitedUserID", invitedUserID, "@hasBeenRedeemed", (hasBeenRedeemed == true ? 1 : 0)));
        }


        public static async Task InsertCashOut(this DataSvc da, CashOut item)
        {
            poInsertQuery q = new poInsertQuery("CashOut");
            if (item.ID.IsNull()) item.ID = da.NEWID();
            q.AddTerm("ID", item.ID);
            q.AddInsertTerms(item);
            await da.ExecuteNonQueryAsync(q);
        }

        public static async Task InsertGameRating(this DataSvc da, GameRating item)
        {
            poInsertQuery q = new poInsertQuery("GameRating");
            if (item.ID.IsNull()) item.ID = da.NEWID();
            q.AddTerm("ID", item.ID);
            q.AddInsertTerms(item);
            await da.ExecuteNonQueryAsync(q);
        }


        public static async Task InsertUser_QuestionAsync(this DataSvc da, User_Question item)
        {
            poInsertQuery q = new poInsertQuery("User_Question");
            if (item.ID.IsNull()) item.ID = da.NEWID();
            q.AddTerm("ID", item.ID);
            q.AddInsertTerms(item);
            int n = await da.ExecuteNonQueryAsync(q);
        }

        public static void InsertUser_Question(this DataSvc da, User_Question item)
        {
            poInsertQuery q = new poInsertQuery("User_Question");
            if (item.ID.IsNull()) item.ID = da.NEWID();
            q.AddTerm("ID", item.ID);
            q.AddInsertTerms(item);
            da.ExecuteNonQuery(q);
        }


        public static async Task<int> Insert<T>(this DataSvc da, T item)
        {
            Type typ = typeof(T);
            poInsertQuery q = new poInsertQuery(typ.Name);
            object id = typ.GetProperty("ID").GetValue(item);
            if (id.IsNull()) typ.GetProperty("ID").SetValue(item, da.NEWID());
            q.AddTerm("ID", typ.GetProperty("ID").GetValue(item));
            q.AddInsertTerms(item);
            return await da.ExecuteNonQueryAsync(q);
        }


        public static async Task<int> Update<T>(this DataSvc da, T item)
        {
            Type typ = typeof(T);
            poUpdateQuery q = new poUpdateQuery(typ.Name);
            q.AddUpdateTerms(item);
            q.WhereEquals("id", item.GetType().GetProperty("ID").GetValue(item));
            return await da.ExecuteNonQueryAsync(q);
        }

        #endregion



        //public static int ReactivateUser(this DataSvc da, string GameID, string UserID, int livesNeeded, string lastUserQuestionID, string activeQuestionID)
        //{
        //    try
        //    {
        //        DateTime dateCreated = DateTime.Now;
        //        string newID = DataSvc.NEWSEQUENTIALID();
        //        try
        //        {
        //            var args = da.Args(
        //                                "@GameID", GameID,
        //                                "@UserID", UserID,
        //                                "@livesNeeded", livesNeeded,
        //                                "@lastUserQuestionID", lastUserQuestionID,
        //                                "@activeQuestionID", activeQuestionID,
        //                                "@dateCreated", dateCreated,
        //                                "@newID", newID
        //                    );
        //            int n= da.ExecuteNonQuery("ReactivateUser", args, true);
        //            return n;
        //        }
        //        catch (Exception ex)
        //        {
        //            string text = ex.Message;
        //            return 0;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}


        public static async Task<int> ReactivateUser(this DataSvc da, string GameID, string UserID, int livesNeeded, string lastUserQuestionID, string activeQuestionID)
        {
            try
            {
                DateTime dateCreated = DateTime.Now;
                string newID = DataSvc.NEWSEQUENTIALID();
                try
                {

                    var args = da.Args(
                    "@GameID", GameID,
                    "@UserID", UserID,
                    "@livesNeeded", livesNeeded,
                    "@lastUserQuestionID", lastUserQuestionID,
                    "@activeQuestionID", activeQuestionID,
                    "@dateCreated", dateCreated,
                    "@newID", newID);

                    StringBuilder sql = new StringBuilder();

                    int n = 0;

                    sql.Clear();
                    sql.Append(@"INSERT INTO dbo.LivesUsed( [ID], [Count], [DateCreated], [UserID], [QuestionID], [User_QuestionID]) VALUES(@newID, @livesNeeded, @dateCreated, @userid, @activeQuestionID, @lastUserQuestionID);");
                    sql.Append(@"UPDATE[User_Game] SET HasBeenEliminated = 0, HasUsedExtraLive = 1 WHERE UserID = @UserID and GameID = @GameID;");
                    sql.Append(@"UPDATE[User] SET ExtraLives = ExtraLives - @livesNeeded WHERE ID = @UserID;");
                    sql.Append(@"UPDATE[User_Question] SET IsCorrect = 1, HasUsedExtraLive = 1 WHERE QuestionID = @activeQuestionID;");
                    n += await da.ExecuteNonQueryAsync(sql.ToString(), args);
                    return n;
                }
                catch (Exception ex)
                {
                    string text = ex.Message;
                    //da.RollbackTransaction();
                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public static async Task ReactivateUserAsync(this DataSvc da, User u, User_Question uq, User_Game ug, LivesUsed lu)
        {
            try
            {
                DateTime dateCreated = DateTime.Now;
                string newID = DataSvc.NEWSEQUENTIALID();
                try
                {
                    da.BeginTransaction();
                    poUpdateQuery q = new poUpdateQuery("User");
                    q.AddTerm("ExtraLives", u.ExtraLives);
                    q.Where( "ID", u.ID, Reeb.SqlOM.CompareOperator.Equal);
                    await da.ExecuteNonQueryAsync(q);

                    q = new poUpdateQuery("User_Question");
                    q.AddTerm("IsCorrect", uq.IsCorrect);
                    q.AddTerm("HasUsedExtraLive", uq.HasUsedExtraLive);
                    q.Where("ID", uq.ID, Reeb.SqlOM.CompareOperator.Equal);
                    await da.ExecuteNonQueryAsync(q);

                    q = new poUpdateQuery("User_Game");
                    q.AddTerm("HasUsedExtraLive", ug.HasUsedExtraLive);
                    q.AddTerm("HasBeenEliminated", ug.HasBeenEliminated);
                    q.Where("ID", ug.ID, Reeb.SqlOM.CompareOperator.Equal);
                    await da.ExecuteNonQueryAsync(q);

                    poInsertQuery qi = new poInsertQuery("LivesUsed");
                    qi.AddInsertTerms(lu);
                    await da.ExecuteNonQueryAsync(qi);

                    da.CommitTransaction();
                    return;
                }
                catch (Exception)
                {
                    da.RollbackTransaction();
                    return;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }


}
