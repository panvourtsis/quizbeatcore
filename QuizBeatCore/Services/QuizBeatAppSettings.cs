﻿using Core.Common;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
namespace QuizBeatBusiness
{
    public interface IQuizBeatAppSettings : IAppSettings
    {
        decimal AppVersion { get; set; }
        string ServerURL { get; set; }
        string storage_account_name { get; set; }
        string storage_account_key { get; set; }
    }

    public class QuizBeatAppSettings : AppSettings, IQuizBeatAppSettings
    {
        public decimal AppVersion { get; set; }
        public string ServerURL { get; set; }
        public string storage_account_name { get; set; }
        public string storage_account_key { get; set; }
    }


    public static class QuizBeatConfigHelper
    {
        private static QuizBeatAppSettings _config;
        private static IServiceProvider _services;
        private static ILogger _logger;

        public static void Configure(IOptions<QuizBeatAppSettings> appSettings)
        {
            _config = appSettings.Value;
        }

        public static void Configure(QuizBeatAppSettings appSettings)
        {
            _config = appSettings;
        }


        public static void Configure(IServiceProvider Services)
        {
            _services = Services;
        }

        public static void Configure(ILogger logger)
        {
            _logger = logger;
        }

        public static IQuizBeatAppSettings Current => _config;
        public static ILogger Logger => _logger;
        public static IServiceProvider Services => _services;

    }

}
