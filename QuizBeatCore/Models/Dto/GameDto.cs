﻿using Newtonsoft.Json;
using QuizBeat.Models.Database;
using System;

namespace QuizBeat.Models.Dto
{
    [Serializable]
    public class GameDto
    {
        public GameDto(Game game)
        {
            this.ID = game.ID;
            this.StartDateTime = game.StartDateTime;
            this.IsCompleted = game.IsCompleted;
            this.HasStarted = game.HasStarted;
            this.PrizeID = game.PrizeID;
            this.IsActive = game.IsActive;
            this.TimeStarted = game.TimeStarted;
            this.NumberOfQuestions = game.NumberOfQuestions;

            if (game.Prize != null)
            {
                this.Prize = new PrizeDto(game.Prize);
            }
        }

        [JsonProperty(PropertyName = "ID")]
        public string ID { get; set; }
        [JsonProperty(PropertyName = "StartDateTime")]
        public Nullable<System.DateTime> StartDateTime { get; set; }
        [JsonProperty(PropertyName = "TimeStarted")]
        public Nullable<System.DateTime> TimeStarted { get; set; }
        [JsonProperty(PropertyName = "IsCompleted")]
        public Nullable<bool> IsCompleted { get; set; }
        [JsonProperty(PropertyName = "HasStarted")]
        public Nullable<bool> HasStarted { get; set; }

        [JsonProperty(PropertyName = "PrizeID")]
        public string PrizeID { get; set; }
        [JsonProperty(PropertyName = "StreamingURLString")]
        public string StreamingURLString { get; set; }

        [JsonProperty(PropertyName = "NumberOfQuestions")]
        public int? NumberOfQuestions { get; set; }
        [JsonProperty(PropertyName = "IsActive")]
        public Nullable<bool> IsActive { get; set; }
        [JsonProperty(PropertyName = "Prize")]
        public PrizeDto Prize { get; set; }

        public override string ToString()
        {
            return $"StartDateTime: {StartDateTime}|TimeStarted: {TimeStarted}|HasStarted: {HasStarted}|IsCompleted: {IsCompleted}|IsActive: {IsActive}";
        }

    }
}