﻿using Newtonsoft.Json;
using System;

namespace QuizBeat.Models.Dto
{
    [Serializable]
    public class MessageDto
    {
        [JsonProperty(PropertyName = "Username")]
        public string Username { get; set; }
        [JsonProperty(PropertyName = "UserImageURL")]
        public String UserImageURL { get; set; }
        [JsonProperty(PropertyName = "DateSent")]
        public Nullable<System.DateTime> DateSent { get; set; }
        [JsonProperty(PropertyName = "Text")]
        public String Text { get; set; }
    }
}