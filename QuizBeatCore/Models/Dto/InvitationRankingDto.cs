﻿using Newtonsoft.Json;
using System;

namespace QuizBeat.Models.Dto
{
    [Serializable]
    public class InvitationRankingDto
    {
        [JsonProperty(PropertyName = "UserID")]
        public string UserID { get; set; }
        [JsonProperty(PropertyName = "Month")]
        public string Month { get; set; }
        [JsonProperty(PropertyName = "Ranking")]
        public int Ranking { get; set; }
        [JsonProperty(PropertyName = "NumberOfInvitationSent")]
        public int NumberOfInvitationSent { get; set; }
    }
}