﻿using Newtonsoft.Json;
using QuizBeat.Models.Database;
using System;
using System.Collections.Generic;

namespace QuizBeat.Models.Dto
{
    [Serializable]
    public class QuestionDto
    {
        public QuestionDto(Question question)
        {
            this.ID = question.ID;
            this.Text = question.Text;
            this.Answer_1 = question.Answer_1;
            this.Answer_2 = question.Answer_2;
            this.Answer_3 = question.Answer_3;
            this.GameID = question.GameID;
            this.Sorting = question.Sorting;
        }

        public static List<QuestionDto> ToQuestionsDto(List<Question> questions)
        {
            List<QuestionDto> list = new List<QuestionDto>();
            foreach (Question question in questions)
            {
                list.Add(new QuestionDto(question));
            }
            return list;
        }

        [JsonProperty(PropertyName = "ID")]
        public string ID { get; set; }
        [JsonProperty(PropertyName = "Text")]
        public string Text { get; set; }
        [JsonProperty(PropertyName = "Answer_1")]
        public string Answer_1 { get; set; }
        [JsonProperty(PropertyName = "Answer_2")]
        public string Answer_2 { get; set; }
        [JsonProperty(PropertyName = "Answer_3")]
        public string Answer_3 { get; set; }
        [JsonProperty(PropertyName = "GameID")]
        public string GameID { get; set; }
        [JsonProperty(PropertyName = "Sorting")]
        public Nullable<int> Sorting { get; set; }

        public override string ToString()
        {
            return $"Text: {Text}|Sorting: {Sorting}|GameID: {GameID}";
        }
    }
}