﻿using Newtonsoft.Json;
using System;

namespace QuizBeat.Models.Dto
{
    [Serializable]
    public class InfoMessageDto
    {
        [JsonProperty(PropertyName = "ID")]
        public string ID { get; set; }
        [JsonProperty(PropertyName = "Title")]
        public string Title { get; set; }
        [JsonProperty(PropertyName = "Subtitle")]
        public string Subtitle { get; set; }
        [JsonProperty(PropertyName = "ButtonText")]
        public string ButtonText { get; set; }
        [JsonProperty(PropertyName = "Show")]
        public bool Show { get; set; }

    }
}