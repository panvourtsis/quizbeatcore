﻿using Newtonsoft.Json;
using QuizBeat.Models.Database;
using System;

namespace QuizBeat.Models.Dto
{
    [Serializable]
    public class DonationDto
    {
        [JsonProperty(PropertyName = "ID")]
        public string ID { get; set; }
        [JsonProperty(PropertyName = "Text")]
        public string Text { get; set; }
        [JsonProperty(PropertyName = "MoneyTarget")]
        public float? MoneyTarget { get; set; }
        [JsonProperty(PropertyName = "MoneyReached")]
        public float? MoneyReached { get; set; }
        [JsonProperty(PropertyName = "Company")]
        public string Company { get; set; }
        [JsonProperty(PropertyName = "IsActive")]
        public bool? IsActive { get; set; }

        public DonationDto(Donation donation)
        {
            this.ID = donation.ID;
            this.Text = donation.Text;
            this.MoneyTarget = (float?)donation.MoneyTarget;
            this.MoneyReached = (float?)donation.MoneyReached;
            this.Company = donation.Company;
            this.IsActive = donation.IsActive;
        }

        public override string ToString()
        {
            return $"{Text}|MoneyTarget: {MoneyTarget}|MoneyReached: {MoneyReached}|IsActive: {IsActive}";
        }
    }
}