﻿using Newtonsoft.Json;
using QuizBeat.Models.Database;
using System;

namespace QuizBeat.Models.Dto
{
    [Serializable]
    public class PrizeDto
    {
        [JsonProperty(PropertyName = "ID")]
        public string ID { get; set; }
        [JsonProperty(PropertyName = "Amount")]
        public double? Amount { get; set; }
        [JsonProperty(PropertyName = "Name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "Type")]
        public string Type { get; set; }

        public PrizeDto(Prize prize)
        {
            this.ID = prize.ID;
            this.Amount = prize.Amount;
            this.Name = prize.Name;
            this.Type = prize.Type;
        }
        public override string ToString()
        {
            return $"Name: {Name}|Amount: {Amount}|Type: {Type}";
        }
    }
}