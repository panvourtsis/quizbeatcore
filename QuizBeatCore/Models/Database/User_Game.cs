using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace QuizBeat.Models.Database
{

    [Serializable]
    public partial class User_Game
    {
        public User_Game()
        {
            User_Question = new List<User_Question>();
        }

        [StringLength(40)]
        public string UserID { get; set; }

        [StringLength(40)]
        public string GameID { get; set; }

        [StringLength(40)]
        public string ID { get; set; }

        public bool HasUsedExtraLive { get; set; }

        public bool HasBeenEliminated { get; set; }

        public DateTime? DateJoined { get; set; }

        public bool HasBeenBanned { get; set; }

        public virtual Game Game { get; set; }

        public virtual User User { get; set; }

        public virtual IList<User_Question> User_Question { get; set; } = new List<User_Question>();


    }
}
