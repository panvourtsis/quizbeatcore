using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace QuizBeat.Models.Database
{

    [Table("Question")]
    [Serializable]
    public partial class Question
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Question()
        {
            User_Question = new List<User_Question>();
        }

        public string Text { get; set; }

        public int? CorrectAnswer { get; set; }

        public string Answer_1 { get; set; }

        public string Answer_2 { get; set; }

        public string Answer_3 { get; set; }

        [Required]
        [StringLength(40)]
        public string GameID { get; set; }

        public int? Sorting { get; set; }

        public DateTime? TimeWentLive { get; set; }

        [StringLength(40)]
        public string ID { get; set; }

        public bool HasAudio { get; set; }

        [StringLength(250)]
        public string ImageUrlString { get; set; }

        public virtual Game Game { get; set; }

        public virtual IList<User_Question> User_Question { get; set; }

        public override string ToString()
        {
            return $"{ID}-{Sorting.ToString().PadLeft(2,'0')}-{Text}-{CorrectAnswer}-{TimeWentLive}";
        }
    }
}
