using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace QuizBeat.Models.Database
{

    [Table("Donation")]
    [Serializable]
    public partial class Donation
    {
        [StringLength(255)]
        public string ID { get; set; }

        public string Text { get; set; }

        public double? MoneyTarget { get; set; }

        public double? MoneyReached { get; set; }

        [StringLength(255)]
        public string Company { get; set; }

        public bool? IsActive { get; set; }
    }
}
