using Core.Common.Model;
using System;
using System.ComponentModel.DataAnnotations;
namespace QuizBeat.Models.Database
{

    [Serializable]
    public partial class User_Question
    {
        [StringLength(40)]
        [KeyField]
        public string ID { get; set; }

        [StringLength(40)]
        public string UserID { get; set; }

        [StringLength(40)]
        public string QuestionID { get; set; }

        public int? AnswerGiven { get; set; }

        public bool? IsCorrect { get; set; }

        [StringLength(40)]
        public string User_GameID { get; set; }

        [StringLength(40)]
        public string GameID { get; set; }


        public bool? HasUsedExtraLive { get; set; }

        public DateTime? DateCreated { get; set; }

        public virtual Question Question { get; set; }

        public virtual User User { get; set; }

        public virtual User_Game User_Game { get; set; }
    }
}
