using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace QuizBeat.Models.Database
{

    [Table("SocketConnection")]
    public partial class SocketConnection
    {
        public string UserID { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Date { get; set; }

        public string SocketID { get; set; }

        public Guid ID { get; set; }
    }
}
