using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QuizBeat.Models.Database
{

    [Table("User")]
    [Serializable]
    public partial class User
    {
        public User()
        {
            Invitations = new List<Invitation>();
            User_Game = new List<User_Game>();
            User_Question = new List<User_Question>();
        }

        [Required]
        [StringLength(40)]
        public string Email { get; set; }

        [Required]
        [StringLength(250)]
        public string Password { get; set; }

        [Required]
        [StringLength(40)]
        public string Username { get; set; }

        public double Money_Won { get; set; } = 0;

        public int ExtraLives { get; set; }

        public Guid? AuthToken { get; set; }

        [StringLength(250)]
        public string PushToken { get; set; }

        [StringLength(40)]
        public string ID { get; set; }

        [StringLength(250)]
        public string ResetPasswordToken { get; set; }

        public DateTime? ResetPasswordRequestedDate { get; set; }

        public double Money_CashedOut { get; set; } = 0;

        public DateTime? Created_At { get; set; }

        public bool IsApproved { get; set; }

        public int? DeviceType { get; set; }

        [StringLength(250)]
        public string OneSignalID { get; set; }

        public DateTime? LastActive_At { get; set; }

        public int? Ranking { get; set; }

        public bool? HasSharedAppOnFacebook { get; set; }

        public bool? HasSharedAppOnTwitter { get; set; }

        public virtual IList<Invitation> Invitations { get; set; }

        public virtual IList<User_Game> User_Game { get; set; }

        public virtual IList<User_Question> User_Question { get; set; }


        public override string ToString()
        {
            return $"Username: {Username}|Email: {Email}|Money_Won: {Money_Won}|Money_CashedOut: {Money_CashedOut}|ExtraLives: {ExtraLives}|Ranking: {Ranking}";
        }
    }
}
