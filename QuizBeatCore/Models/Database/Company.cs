using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace QuizBeat.Models.Database
{


    [Table("Company")]
    [Serializable]
    public partial class Company
    {
        public Company()
        {
            Prizes = new List<Prize>();
        }

        public Guid ID { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        public virtual IList<Prize> Prizes { get; set; }
    }
}
