using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace QuizBeat.Models.Database
{

    [Table("Prize")]
    [Serializable]
    public partial class Prize
    {
        public Prize()
        {
            Games = new List<Game>();
            User_PrizeWon = new List<User_PrizeWon>();
        }

        public double Amount { get; set; }

        [Required]
        [StringLength(36)]
        public string Name { get; set; }

        [StringLength(40)]
        public string ID { get; set; }

        [StringLength(10)]
        public string Type { get; set; }

        public virtual IList<Game> Games { get; set; }

        public virtual IList<User_PrizeWon> User_PrizeWon { get; set; }
    }
}
