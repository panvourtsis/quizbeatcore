using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace QuizBeat.Models.Database
{

    [Table("Invitation")]
    [Serializable]
    public partial class Invitation
    {
        [StringLength(40)]
        public string UserID { get; set; }

        [StringLength(40)]
        public string InvitedUserID { get; set; }

        [StringLength(40)]
        public string ID { get; set; }

        public bool? HasBeenRedeemed { get; set; }

        public DateTime? Date { get; set; }

        public virtual User User { get; set; }
    }
}
