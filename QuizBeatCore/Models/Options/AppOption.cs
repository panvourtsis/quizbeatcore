﻿using Newtonsoft.Json;

namespace QuizBeat.Models.Options
{
    public class AppOption
    {
        [JsonProperty(PropertyName = "Version")]
        public string Version { get; set; }

        [JsonProperty(PropertyName = "Build")]
        public string Build { get; set; }
    }
}