﻿using Core.Common;
using Microsoft.EntityFrameworkCore;
using QuizBeat.Models.Database;
using QuizBeat.Models.Dto;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace QuizBeatAPI.Repositories
{
    public interface IDataBaseRepository
    {
        #region < SocketConnection >
        Task<IEnumerable<SocketConnection>> AllSocketConnections();  // { get; }
        Task<SocketConnection> FindSocketConnection(string id);
        Task InsertSocketConnection(SocketConnection item);
        Task UpdateSocketConnection(SocketConnection item);
        Task DeleteSocketConnection(string id);
        #endregion

        #region < PrizeWon >
        Task<IEnumerable<User_PrizeWon>> AllUser_PrizeWon();  // { get; }
        Task<User_PrizeWon> FindUser_PrizeWon(string id);
        Task InsertUser_PrizeWon(User_PrizeWon item);
        Task UpdateUser_PrizeWon(User_PrizeWon item);
        Task DeleteUser_PrizeWon(string id);
        #endregion


        #region < User_Question >
        Task<IEnumerable<User_Question>> AllUser_Questions();  // { get; }
        Task<User_Question> FindUser_Question(string id);
        Task<IEnumerable<User_Question>> FindAnswers(string questionID = null);
        Task<IEnumerable<User_Question>> FindWinners(string questionID, bool isCorrect);
        Task InsertUser_Question(User_Question item);
        Task UpdateUser_Question(User_Question item);
        Task DeleteUser_Question(string id);
        Task<User_Question> FindUser_Question(string userID, string gameID, string questionID);
        #endregion

        #region < User >

        Task<IEnumerable<User>> AllUsers(bool hasPushNotification);
        Task<IEnumerable<User>> Top100AllTimeUsers(); //{ get; }
        Task<IEnumerable<User>> Top100WeeklyUsers();  // { get; }
        Task<int> FindRankIndexForUserID(string id);
        Task<bool> DoesUserExist(string id, string username, string email);
        Task<User> FindUserWithResetPassword(string resetPasswordToken = null);
        Task<User> FindUser(string id = null, string email = null, string username = null);
        Task<User> FindUser(Guid authToken);
        Task<User> FindUserAsync(Guid authToken);
        Task InsertUser(User item);
        Task UpdateUser(User item);
        Task DeleteUser(string id);
        #endregion

        #region < Question >
        Task<IEnumerable<Question>> AllQuestions();
        Task<Question> FindQuestion(string id);
        Task InsertQuestion(Question item);
        Task UpdateQuestion(Question item);
        Task DeleteQuestion(string id);
        #endregion

        #region <LivesUsed >
        Task<IEnumerable<LivesUsed>> AllLivesUseds();
        Task<LivesUsed> FindLivesUsed(string id);
        Task InsertLivesUsed(LivesUsed item);
        Task UpdateLivesUsed(LivesUsed item);
        Task DeleteLivesUsed(string id);
        #endregion


        Task<IEnumerable<LivesBought>> AllLivesBoughts();
        Task<LivesBought> FindLivesBought(string id);
        Task InsertLivesBought(LivesBought item);
        Task UpdateLivesBought(LivesBought item);
        Task DeleteLivesBought(string id);


        #region < Invitation >
        Task<IEnumerable<InvitationRankingDto>> AllInvitationRankingsDtos();
        Task<IEnumerable<InvitationRankingDto>> AllInvitationRankingsDtos(string userID);
        IEnumerable<Invitation> AllInvitation { get; }
        Task<Invitation> FindInvitation(string ID);
        Task<Invitation> FindPendingInvitation(string invitedUserID, bool hasBeenRedeemed);
        Task<IEnumerable<Invitation>> FindInvitations(string UserID);

        Task InsertInvitation(Invitation item);
        Task UpdateInvitation(Invitation item);
        Task DeleteInvitation(string id);
        #endregion


        #region < Game >
        Task<IEnumerable<Game>> AllGames();  // { get; }
        Task<bool> DoesGameExist(Guid id);
        Task<Game> FindGame(string id);
        Task<Game> FindNextGame();
        Task<Game> FindActiveGame(bool? includeQuestions = null, bool? includePrize = null);
        Task<Game> FindRunningGame(bool? includeQuestions = null, bool? includePrize = null);
        Task<Game> FindStartedgGame();
        Task InsertGame(Game item);
        Task UpdateGame(Game item);
        Task DeleteGame(string id);
        #endregion

        Task<IEnumerable<Donation>> AllDonations();
        Task<Donation> FindDonation(string id = null, bool? isActive = null);
        Task InsertDonation(Donation item);
        Task UpdateDonation(Donation item);
        Task DeleteDonation(string id);

        Task<IEnumerable<CashOut>> AllCashOuts();
        Task<CashOut> FindCashOut(string id);
        Task<CashOut> FindPendingCashOut(string userId = null);
        Task InsertCashOut(CashOut item);
        Task UpdateCashOut(CashOut item);
        Task DeleteCashOut(string id);

        Task<User_Game> FindUser_Game(string id = null);
        Task<User_Game> FindUser_GameByUserID(string userID);
        Task<User_Game> FindUser_Game(string userID, string gameID);

        Task<IEnumerable<User_Game>> AllUser_Games(string gameID);
        Task InsertUser_Game(User_Game item);
        Task UpdateUser_Game(User_Game item);
        Task DeleteUser_Game(string id);


        Task InsertGameRating(GameRating item);

        Task SaveChanges();

    }
}

namespace QuizBeatAPI.Repositories
{
    public class DataBaseRepository : IDataBaseRepository
    {
        private QuizBeatContext _context;

        public DataBaseRepository()
        {
            InitializeData();
        }
        public DataBaseRepository(QuizBeatContext ctx)
        {
            _context = ctx;
        }

        private void InitializeData()
        {
            _context = new QuizBeatContext();
        }

        public async Task SaveChanges()
        {
            await _context.SaveChangesAsync();
        }


        #region < SocketConnection >

        public async Task<IEnumerable<SocketConnection>> AllSocketConnections() => await _context.SocketConnections.ToListAsync();

        public async Task<SocketConnection> FindSocketConnection(string id)
        {
            return await _context.SocketConnections.Where(item => item.ID.ToString() == id.ToString()).FirstOrDefaultAsync();
        }

        public async Task InsertSocketConnection(SocketConnection item)
        {
            await _context.SocketConnections.AddAsync(item);
        }

        public async Task UpdateSocketConnection(SocketConnection item)
        {
            await Task.Yield();
            _context.Update<SocketConnection>(item);
            //var todoItem = this.FindSocketConnection(item.ID.ToString());
            //var index = _context.SocketConnections.ToList().IndexOf(todoItem);
            //_context.SocketConnections.ToList().RemoveAt(index);
            //_context.SocketConnections.ToList().Insert(index, item);
        }

        public async Task DeleteSocketConnection(string id)
        {
            _context.SocketConnections.Remove(await this.FindSocketConnection(id));
        }
        #endregion


        #region < PrizeWon >
        public async Task<IEnumerable<User_PrizeWon>> AllUser_PrizeWon() => await _context.User_PrizeWon.ToListAsync();

        public async Task<User_PrizeWon> FindUser_PrizeWon(string id)
        {
            return await _context.User_PrizeWon.Where(item => item.ID.ToString() == id.ToString()).FirstOrDefaultAsync();
        }

        public async Task InsertUser_PrizeWon(User_PrizeWon item)
        {
            await _context.User_PrizeWon.AddAsync(item);
        }

        public async Task UpdateUser_PrizeWon(User_PrizeWon item)
        {
            await Task.Yield();
            _context.Update<User_PrizeWon>(item);
            //var todoItem = this.FindUser_PrizeWon(item.ID);
            //var index = _context.User_PrizeWon.ToList().IndexOf(todoItem);
            //_context.User_PrizeWon.ToList().RemoveAt(index);
            //_context.User_PrizeWon.ToList().Insert(index, item);
        }

        public async Task DeleteUser_PrizeWon(string id)
        {
            _context.User_PrizeWon.Remove(await this.FindUser_PrizeWon(id));
        }

        #endregion


        #region < Questions >
        public async Task<IEnumerable<User_Question>> AllUser_Questions() => await _context.User_Question.ToListAsync();

        public async Task<User_Question> FindUser_Question(string id)
        {
            return await _context.User_Question.Where(item => item.ID.ToString() == id.ToString()).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<User_Question>> FindAnswers(string questionID = null)
        {
            return await _context.User_Question.Include("User").Where(item => item.QuestionID == questionID).ToListAsync();
        }

        public async Task<IEnumerable<User_Question>> FindWinners(string questionID, bool isCorrect)
        {
            return await _context.User_Question.Include("User").Where(item => item.QuestionID == questionID && item.IsCorrect == isCorrect).ToListAsync();
        }

        public async Task InsertUser_Question(User_Question item)
        {
            await _context.User_Question.AddAsync(item);
        }

        public async Task UpdateUser_Question(User_Question item)
        {
            await Task.Yield();
            _context.Update<User_Question>(item);
            //var todoItem = this.FindUser_Question(item.ID);
            //var index = _context.User_Question.ToList().IndexOf(todoItem);
            //_context.User_Question.ToList().RemoveAt(index);
            //_context.User_Question.ToList().Insert(index, item);
        }

        public async Task DeleteUser_Question(string id)
        {
            _context.User_Question.Remove(await this.FindUser_Question(id));
        }

        #endregion


        #region < User >
        public async Task<IEnumerable<User>> AllUsers(bool hasPushNotification)
        {
            if (hasPushNotification == true)
            {
                return await _context.Users.Where(l => l.PushToken != null && l.PushToken.Length > 0).ToListAsync();
            }
            else
            {
                return await _context.Users.ToListAsync();
            }
        }

        public async Task<IEnumerable<User>> Top100AllTimeUsers() => await _context.Users.Where(l => l.Ranking.Value > 0).OrderBy(l => l.Ranking).Take(100).ToListAsync();

        public async Task<IEnumerable<User>> Top100WeeklyUsers() => await _context.Users.ToListAsync();

        public async Task<int> FindRankIndexForUserID(string id)
        {
            IEnumerable<User> users = await _context.Users.OrderByDescending(l => l.Money_Won).ToListAsync();
            User user = users.FirstOrDefault(l => l.ID == id);
            int index = users.ToList().IndexOf(user);
            return index;
        }

        public async Task<bool> DoesUserExist(string id, string username, string email)
        {
            if (!id.IsNullOrEmpty())
            {
                return await _context.Users.Where(item => item.ID == id).AnyAsync<User>();

            }
            else if (!username.IsNullOrEmpty())
            {
                return await _context.Users.Where(item => item.Username == username).AnyAsync<User>();

            }
            else if (!email.IsNullOrEmpty())
            {
                return await _context.Users.Where(item => item.Email == email).AnyAsync<User>();
            }
            return false;
        }
        public async Task<User> FindUserWithResetPassword(string resetPasswordToken = null)
        {
            var todayMinus7 = DateTime.UtcNow.AddDays(-7);
            IEnumerable<User> users = await _context.Users.Where(item => item.ResetPasswordToken == resetPasswordToken && todayMinus7 < item.ResetPasswordRequestedDate.AsDateOnly()).ToListAsync();
            return users.FirstOrDefault();
        }
        public async Task<User> FindUser(string id = null, string email = null, string username = null)
        {
            return await _context.Users.Where(item => item.ID == id || item.Email == email || item.Username == username).FirstOrDefaultAsync<User>();
        }

        public async Task<User> FindUser(Guid authToken)
        {
            return await _context.Users.Where(item => item.AuthToken == authToken).FirstOrDefaultAsync();
        }

        public async Task<User> FindUserAsync(Guid authToken)
        {
            return await _context.Users.Where(item => item.AuthToken == authToken).FirstOrDefaultAsync();
        }

        public async Task InsertUser(User item)
        {
            await _context.Users.AddAsync(item);
        }

        public async Task UpdateUser(User item)
        {
            await Task.Yield();
            _context.Users.Update(item);
            //var todoItem = await this.FindUser(item.ID);
            //var index = _context.Users.ToList().IndexOf(todoItem);
            //_context.Users.ToList().RemoveAt(index);
            //_context.Users.ToList().Insert(index, item);
        }

        public async Task DeleteUser(string id)
        {
            _context.Users.Remove(await this.FindUser(id));
        }

        #endregion


        #region < Question >
        public async Task<IEnumerable<Question>> AllQuestions() => await _context.Questions.ToListAsync();

        public async Task<Question> FindQuestion(string id)
        {
            return await _context.Questions.Where(item => item.ID.ToString() == id.ToString()).FirstOrDefaultAsync();
        }

        public async Task InsertQuestion(Question item)
        {
            await _context.Questions.AddAsync(item);
        }

        public async Task UpdateQuestion(Question item)
        {
            await Task.Yield();
            _context.Update<Question>(item);
            //var todoItem = this.FindQuestion(item.ID);
            //var index = _context.Questions.ToList().IndexOf(todoItem);
            //_context.Questions.ToList().RemoveAt(index);
            //_context.Questions.ToList().Insert(index, item);
        }

        public async Task DeleteQuestion(string id)
        {
            _context.Questions.Remove(await this.FindQuestion(id));
        }

        #endregion


        #region < LivesUsed >
        public async Task<IEnumerable<LivesUsed>> AllLivesUseds() => await _context.LivesUseds.ToListAsync();

        public async Task<LivesUsed> FindLivesUsed(string id)
        {
            return await _context.LivesUseds.Where(item => item.ID.ToString() == id.ToString()).FirstOrDefaultAsync();
        }

        public async Task InsertLivesUsed(LivesUsed item)
        {
            await _context.LivesUseds.AddAsync(item);
        }

        public async Task UpdateLivesUsed(LivesUsed item)
        {
            await Task.Yield();

            _context.Update<LivesUsed>(item);

            //var todoItem = this.FindLivesUsed(item.ID);
            //var index = _context.LivesUseds.ToList().IndexOf(todoItem);
            //_context.LivesUseds.ToList().RemoveAt(index);
            //_context.LivesUseds.ToList().Insert(index, item);
        }

        public async Task DeleteLivesUsed(string id)
        {
            _context.LivesUseds.Remove(await this.FindLivesUsed(id));
        }

        #endregion

        #region < AllLivesBought >
        public async Task<IEnumerable<LivesBought>> AllLivesBoughts() => await _context.LivesBoughts.ToListAsync();

        public async Task<LivesBought> FindLivesBought(string id)
        {
            return await _context.LivesBoughts.Where(item => item.ID.ToString() == id.ToString()).FirstOrDefaultAsync();
        }

        public async Task InsertLivesBought(LivesBought item)
        {
            await _context.LivesBoughts.AddAsync(item);
        }

        public async Task UpdateLivesBought(LivesBought item)
        {
            await Task.Yield();
            _context.Update<LivesBought>(item);
            //var todoItem = this.FindLivesBought(item.ID);
            //var index = _context.LivesBoughts.ToList().IndexOf(todoItem);
            //_context.LivesBoughts.ToList().RemoveAt(index);
            //_context.LivesBoughts.ToList().Insert(index, item);
        }

        public async Task DeleteLivesBought(string id)
        {
            _context.LivesBoughts.Remove(await this.FindLivesBought(id));
        }

        #endregion


        public async Task<IEnumerable<InvitationRankingDto>> AllInvitationRankingsDtos()
        {
            int month = DateTime.Now.Month;
            string monthName = DateTime.UtcNow.ToString("MMMM", CultureInfo.CreateSpecificCulture("el"));
            int year = DateTime.Now.Year;

            IEnumerable<InvitationRankingDto> invitationRankingDtos = await _context.Invitations
                   .Where(inv => inv.Date.Value.Month == month && inv.Date.Value.Year == year)
                   .GroupBy(info => info.UserID)
                   .Select(group => new InvitationRankingDto()
                   {
                       UserID = group.Key,
                       Month = monthName,
                       NumberOfInvitationSent = group.Count()
                   })
                   .OrderByDescending(x => x.NumberOfInvitationSent).ToListAsync();

            return invitationRankingDtos;
        }
        public async Task<IEnumerable<InvitationRankingDto>> AllInvitationRankingsDtos(string userID)
        {
            int month = DateTime.Now.Month;
            string monthName = DateTime.UtcNow.ToString("MMMM", CultureInfo.CreateSpecificCulture("el"));
            int year = DateTime.Now.Year;

            IEnumerable<InvitationRankingDto> invitationRankingDtos = await _context.Invitations
                   .Where(inv => inv.Date.Value.Month == month && inv.Date.Value.Year == year && inv.UserID == userID)
                   .GroupBy(info => info.UserID)
                   .Select(group => new InvitationRankingDto()
                   {
                       UserID = group.Key,
                       Month = monthName,
                       NumberOfInvitationSent = group.Count()
                   })
                   .OrderByDescending(x => x.NumberOfInvitationSent).ToListAsync();

            return invitationRankingDtos;
        }

        #region < Invitation >

        public IEnumerable<Invitation> AllInvitation => _context.Invitations.ToList();

        public async Task<Invitation> FindInvitation(string ID)
        {
            return await _context.Invitations.Where(item => item.ID.ToString() == ID).FirstOrDefaultAsync();
        }

        public async Task<Invitation> FindPendingInvitation(string invitedUserID, bool hasBeenRedeemed)
        {
            return await _context.Invitations.Where(item => item.InvitedUserID.ToString() == invitedUserID && item.HasBeenRedeemed == hasBeenRedeemed).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Invitation>> FindInvitations(string UserID)
        {
            return await _context.Invitations.Include("User").Where(item => item.UserID == UserID).ToListAsync();
        }

        public async Task InsertInvitation(Invitation item)
        {
            await _context.Invitations.AddAsync(item);
        }

        public async Task UpdateInvitation(Invitation item)
        {
            await Task.Yield();
            _context.Update<Invitation>(item);
            //var todoItem = this.FindInvitation(item.ID);
            //var index = _context.Invitations.ToList().IndexOf(todoItem);
            //_context.Invitations.ToList().RemoveAt(index);
            //_context.Invitations.ToList().Insert(index, item);
        }

        public async Task DeleteInvitation(string id)
        {
            _context.Invitations.Remove(await this.FindInvitation(id));
        }

        #endregion


        #region < Game >
        public async Task<IEnumerable<Game>> AllGames() => await _context.Games.Include("Questions").ToListAsync();

        public async Task<bool> DoesGameExist(Guid id)
        {
            return await _context.Games.AnyAsync(item => item.ID.Equals(id));
        }

        public async Task<Game> FindGame(string id)
        {
            return await _context.Games.Where(item => item.ID.Equals(id)).FirstOrDefaultAsync();
        }

        public async Task<Game> FindNextGame()
        {
            Game nextGame = await _context.Games.Include("Prize").OrderBy(l => l.StartDateTime).Where(item => item.IsActive == false).FirstOrDefaultAsync();
            return nextGame;
        }

        public async Task<Game> FindActiveGame(bool? includeQuestions = null, bool? includePrize = null)
        {
            Game activeGame = null;

            if (includeQuestions.GetValueOrDefault() && includePrize.GetValueOrDefault())
            {
                activeGame = await _context.Games.Include("Questions").Include("Prize").Where(item => item.IsActive == true && item.IsCompleted == false).FirstOrDefaultAsync();
            }
            else if (includeQuestions.GetValueOrDefault())
            {
                activeGame = await _context.Games.Include("Questions").Where(item => item.IsActive == true && item.IsCompleted == false).FirstOrDefaultAsync();
            }
            else
            {
                activeGame = await _context.Games.Include("Prize").Where(item => item.IsActive == true && item.IsCompleted == false).FirstOrDefaultAsync();
            }
            if (activeGame != null)
            {
                return activeGame;
            }
            return null;
        }


        public async Task<Game> FindRunningGame(bool? includeQuestions = null, bool? includePrize = null)
        {
            Game activeGame = null;

            if (includeQuestions.GetValueOrDefault() && includePrize.GetValueOrDefault())
            {
                activeGame = await _context.Games.Include("Questions").Include("Prize").Where(item => item.IsActive == true && item.IsCompleted == false && item.HasStarted == false).FirstOrDefaultAsync();
            }
            else if (includeQuestions.GetValueOrDefault())
            {
                activeGame = await _context.Games.Include("Questions").Where(item => item.IsActive == true && item.IsCompleted == false && item.HasStarted == false).FirstOrDefaultAsync();
            }
            else
            {
                activeGame = await _context.Games.Include("Prize").Where(item => item.IsActive == true && item.IsCompleted == false && item.HasStarted == false).FirstOrDefaultAsync();
            }
            if (activeGame != null)
            {
                return activeGame;
            }
            return null;
        }


        public async Task<Game> FindStartedgGame()
        {
            Game activeGame = null;

            activeGame = await _context.Games.Include(x => x.Questions).Include(x => x.Prize).Where(item => item.IsActive == true && item.HasStarted == true).FirstOrDefaultAsync();
            if (activeGame != null)
            {
                return activeGame;
            }
            return null;
        }


        public async Task InsertGame(Game item)
        {
            await _context.Games.AddAsync(item);
        }

        public async Task UpdateGame(Game item)
        {
            await Task.Yield();
            _context.Update<Game>(item);
            //var todoItem = this.FindGame(item.ID);
            //var index = _context.Games.ToList().IndexOf(todoItem);
            //_context.Games.ToList().RemoveAt(index);
            //_context.Games.ToList().Insert(index, item);
        }

        public async Task DeleteGame(string id)
        {
            _context.Games.Remove(await this.FindGame(id));
        }

        #endregion


        #region < Donation >

        public async Task<IEnumerable<Donation>> AllDonations() => await _context.Donations.ToListAsync();

        public async Task<Donation> FindDonation(string id = null, bool? isActive = null)
        {
            if (!id.IsNull())
                return await _context.Donations.Where(item => item.ID.ToString() == id.ToString()).FirstOrDefaultAsync();
            else if (!isActive.IsNull())
                return await _context.Donations.Where(item => item.IsActive == isActive).FirstOrDefaultAsync();
            else if (!id.IsNull() && !isActive.IsNull())
                return await _context.Donations.Where(item => item.ID.ToString() == id.ToString() && item.IsActive == isActive).FirstOrDefaultAsync();
            return null;
        }

        public async Task InsertDonation(Donation item)
        {
            await _context.Donations.AddAsync(item);
        }

        public async Task UpdateDonation(Donation item)
        {

            await Task.Yield();
            _context.Update<Donation>(item);
            //var todoItem = this.FindDonation(item.ID);
            //var index = _context.Donations.ToList().IndexOf(todoItem);
            //_context.Donations.ToList().RemoveAt(index);
            //_context.Donations.ToList().Insert(index, item);
        }

        public async Task DeleteDonation(string id)
        {
            _context.Donations.Remove(await this.FindDonation(id));
        }
        #endregion


        #region < Cashout >
        public async Task<IEnumerable<CashOut>> AllCashOuts() => await _context.CashOuts.ToListAsync();

        public async Task<CashOut> FindCashOut(string id)
        {
            return await _context.CashOuts.Where(item => item.ID.ToString() == id.ToString()).FirstOrDefaultAsync();
        }

        public async Task<CashOut> FindPendingCashOut(string userId = null)
        {
            return await _context.CashOuts.Where(item => item.IsApproved == false && item.UserID == userId).FirstOrDefaultAsync();
        }

        public async Task InsertCashOut(CashOut item)
        {
            await _context.CashOuts.AddAsync(item);
        }

        public async Task UpdateCashOut(CashOut item)
        {
            await Task.Yield();
            _context.Update(item);
            //var todoItem = this.FindCashOut(item.ID);
            //var index = _context.CashOuts.ToList().IndexOf(todoItem);
            //_context.CashOuts.ToList().RemoveAt(index);
            //_context.CashOuts.ToList().Insert(index, item);
        }

        public async Task DeleteCashOut(string id)
        {
            _context.CashOuts.Remove(await this.FindCashOut(id));
        }

        #endregion


        #region < User_Game >

        public async Task<User_Game> FindUser_Game(string id = null)
        {
            return await _context.User_Game.Where(item => item.ID == id.ToString()).FirstOrDefaultAsync();
        }

        public async Task<User_Game> FindUser_GameByUserID(string userID)
        {
            return await _context.User_Game.FirstOrDefaultAsync(item => item.UserID == userID.ToString());
        }

        public async Task<User_Game> FindUser_Game(string userID, string gameID)
        {
            return await _context.User_Game.Include("User_Question").Where(item => item.UserID == userID && item.GameID == gameID).FirstOrDefaultAsync();

            //User_Game ug = await _context.User_Game.Include("User_Question").Where(item => item.UserID == userID && item.GameID == gameID).FirstOrDefaultAsync();
            //if (ug != null)
            //{
            //    foreach (var q in ug.User_Question)
            //    {
            //        q.Question = await FindQuestion(q.ID);
            //    }
            //}
            //return ug;
        }

        public async Task<User_Question> FindUser_Question(string userID, string gameID, string questionID)
        {
            return await _context.User_Question.Include("User_Game").Where(l => l.User_Game.UserID == userID && l.User_Game.GameID == gameID).FirstOrDefaultAsync(item => item.QuestionID == questionID);

        }

        public async Task<IEnumerable<User_Game>> AllUser_Games(string gameID)
        {
            return await _context.User_Game.Where(item => item.GameID == gameID).ToListAsync();
        }

        public async Task InsertUser_Game(User_Game item)
        {
            await _context.User_Game.AddAsync(item);
        }

        public async Task UpdateUser_Game(User_Game item)
        {
            await Task.Yield();
            _context.Update<User_Game>(item);

            //var todoItem = this.FindUser_Game(item.ID);
            //var index = _context.User_Game.ToList().IndexOf(todoItem);
            //_context.User_Game.ToList().RemoveAt(index);
            //_context.User_Game.ToList().Insert(index, item);
        }

        public async Task DeleteUser_Game(string id)
        {
            _context.User_Game.Remove(await this.FindUser_Game(id));
        }

        #endregion

        public async Task InsertGameRating(GameRating item)
        {
            await _context.GameRatings.AddAsync(item);
        }

    }

}