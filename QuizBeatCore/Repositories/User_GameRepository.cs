﻿using System.Collections.Generic;
using System.Linq;
using QuizBeat.Models.Database;
using System;

namespace QuizBeatAPI.Repositories
{
    public interface IUser_GameRepository
    {
        IEnumerable<User_Game> All(string gameID);
        User_Game Find(string id = null);
        User_Game FindByUserID(string userID);
        User_Game Find(string userID = null, string gameID = null);
        void Insert(User_Game item);
        void Update(User_Game item);
        void Delete(string id);
        void SaveChanges();
    }
}

namespace QuizBeatAPI.Repositories
{
    public class User_GameRepository : IUser_GameRepository
    {
        private QuizBeatContext _context;

        public User_GameRepository()
        {
            InitializeData();
        }
        public User_GameRepository(QuizBeatContext ctx)
        {
            _context = ctx;
        }

        public User_Game Find(string id = null)
        {
            return _context.User_Game.FirstOrDefault(item => item.ID == id.ToString());
        }

        public User_Game FindByUserID(string userID) {
            return _context.User_Game.FirstOrDefault(item => item.UserID == userID.ToString());
        }

        public User_Game Find(string userID, string gameID)
        {
            return _context.User_Game.FirstOrDefault(item =>item.UserID == userID && item.GameID == gameID);
        }

        public IEnumerable<User_Game> All(string gameID)
        {
            return _context.User_Game.Where(item => item.GameID == gameID);
        }

        public void Insert(User_Game item)
        {
            _context.User_Game.Add(item);
        }

        public void Update(User_Game item)
        {
            var todoItem = this.Find(item.ID);
            var index = _context.User_Game.ToList().IndexOf(todoItem);
            _context.User_Game.ToList().RemoveAt(index);
            _context.User_Game.ToList().Insert(index, item);
        }

        public void Delete(string id)
        {
            _context.User_Game.Remove(this.Find(id));
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        private void InitializeData()
        {
            _context = new QuizBeatContext();
        }
    }
}