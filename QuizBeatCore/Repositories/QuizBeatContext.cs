using Core.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;

namespace QuizBeat.Models.Database
{

    public partial class QuizBeatContext : DbContext
    {
        public QuizBeatContext()
            : base()  //"name=QuizBeatModel"
        {

        }

        public QuizBeatContext(DbContextOptions<QuizBeatContext> options)
            : base(options)  //"name=QuizBeatModel"
        {
        }

        public virtual DbSet<CashOut> CashOuts { get; set; }
        public virtual DbSet<Donation> Donations { get; set; }
        public virtual DbSet<Game> Games { get; set; }
        public virtual DbSet<GameRating> GameRatings { get; set; }
        public virtual DbSet<Invitation> Invitations { get; set; }
        public virtual DbSet<LivesBought> LivesBoughts { get; set; }
        public virtual DbSet<LivesUsed> LivesUseds { get; set; }
        public virtual DbSet<Prize> Prizes { get; set; }
        public virtual DbSet<Question> Questions { get; set; }
        public virtual DbSet<SocketConnection> SocketConnections { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<User_Game> User_Game { get; set; }
        public virtual DbSet<User_PrizeWon> User_PrizeWon { get; set; }
        public virtual DbSet<User_Question> User_Question { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            string activeConnStr = ConfigHelper.Configuration["AppSettings:ActiveConnStr"];
            string cn = ConfigHelper.Configuration[$"AppSettings:{activeConnStr}"];
            Dictionary<string,string> cnstr = ConfigHelper.Configuration.AsDictionary($"AppSettings:ConnectionStrings");
            optionsBuilder.UseSqlServer(cn);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<CashOut>().Property(e => e.UserID).IsUnicode(false);
            modelBuilder.Entity<CashOut>().Property(e => e.IBAN).IsUnicode(false);
            modelBuilder.Entity<CashOut>().Property(e => e.FullName).IsUnicode(false);
            modelBuilder.Entity<CashOut>().Property(e => e.Type).IsUnicode(false);

            modelBuilder.Entity<Game>().HasMany(e => e.Questions);
            //modelBuilder.Entity<Game>().HasMany(e => e.Questions).WithRequired(e => e.Game).WillCascadeOnDelete(false);

            modelBuilder.Entity<Prize>().Property(e => e.Type).IsUnicode(false);
            modelBuilder.Entity<Prize>().HasMany(e => e.User_PrizeWon);
            //modelBuilder.Entity<Prize>().HasMany(e => e.User_PrizeWon).WithRequired(e => e.Prize).WillCascadeOnDelete(false);

            modelBuilder.Entity<Question>().Property(e => e.ImageUrlString).IsUnicode(false);

            modelBuilder.Entity<User>().Property(e => e.Email).IsUnicode(false);
            modelBuilder.Entity<User>().Property(e => e.Password).IsUnicode(false);
            modelBuilder.Entity<User>().Property(e => e.PushToken).IsUnicode(false);
            modelBuilder.Entity<User>().Property(e => e.OneSignalID).IsUnicode(false);
            modelBuilder.Entity<User>().HasMany(e => e.Invitations);
            //modelBuilder.Entity<User>().HasMany(e => e.Invitations).WithOptional(e => e.User).HasForeignKey(e => e.InvitedUserID);

            //modelBuilder.Entity<User_Question>().HasOne(e => e.Question);


        }
    }
}
